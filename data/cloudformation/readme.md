

deploy `db.yml` with `deploy_db.sh`


this will make a new cloudformation stack, get the endpoint output from there

run
`sqlcmd -S ed1kb6a1d66qnsk.ck630tcb41d7.us-east-2.rds.amazonaws.com -U adminuser -P password` to connect to the database

run
```
nick::~$ sqlcmd -S ed1kb6a1d66qnsk.ck630tcb41d7.us-east-2.rds.amazonaws.com -U adminuser -P password
1> USE master;
2> CREATE DATABASE testdata;
3> GO
Changed database context to 'master'.
1> USE testdata
2> ;
3> GO
Changed database context to 'testdata'.
```
to create `testdata` database