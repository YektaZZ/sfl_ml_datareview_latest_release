#!/bin/bash
set -ex

DB_STACK_NAME="ecs-data-review-test-data-db"
PATH_TO_DB_TEMPLATE="db.yml"

function create_cluster(){
	aws cloudformation create-stack \
    --stack-name $DB_STACK_NAME \
    --template-body file://$PATH_TO_DB_TEMPLATE \
    --parameters \
    --capabilities CAPABILITY_NAMED_IAM \
    --tags Key=Project,Value="eCS AI Architecture"

}

create_cluster


aws cloudformation wait stack-create-complete --stack-name $DB_STACK_NAME