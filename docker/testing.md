# Unit Testing


Once the container is built (by running the command `./build_container.sh` from this `docker` directory), the tests can be ran by running the command
`docker run -it --rm -v $(cd .. && pwd):/app/ ecsdatareview pytest test/src` to run all tests.

To run specific tests, run the command `docker run -it --rm -v $(cd .. && pwd):/app/ ecsdatareview bash` to get a shell in the container. From there you can run a specifiec test by running `pytest test/src/classes/test_name.py`

# E2E testing


## Deployment on AWS
This step requires aws_cli set up for SFL's AWS account. It also assumes standard linux tools like `awk` and `grep` are available to the scripts.


Follow the instructions [here](README.md) to deploy using the documentation.
When the instructions reference the AI Architecture repo, that can be found [here](https://github.com/SFLScientific/eCS_AIArchitecture)


## Testing endpoint on AWS

First visit the cloudformation console on AWS. There will be some cloudformation stacks (if they do not show up, check that the region that your aws_cli is set to and the region selected in the webconsole match). In the stack that says cluster, note down one of the public subnets as well as the VPC that it is deployed into (this should be already filled out in the templates). Next deploy a new ec2 instance into that save vpc, in the public subnet (the "auto asign public IP" setting will be default to "enable"), and then make sure it is in the security group that has "OVPN" in its name. Don't forget to tag the ec2 instance with the project and your user name tags.

Next once that is online, it can be SSH'd into to run tests against the load balancer. To find the load balancer endpoint, go to the web ec2 console and on the sidebar pick "load balancers". There will be a list of load balancers, the one that has "internal" in the name and is in the same VPC as the cluster was deployed into, is the load balancer for the service/cluster. When it is selected, it will show a panel at the bottom of the page, select "DNS Name" (there will be a "click to copy this" button next to it). That is the endpoint to make requests to for testing.


## Deployment on local system (for debugging / errors on AWS)

Since it is much easier to debug locally than the logs in AWS, there are scripts that make that process easier. 
This process will build the docker image again and then run it with your local aws credentials.
All following steps are ran from inside this `docker` folder.
Run `./build_container.sh` to build the container image.
Next make sure that you have the following variables set in your ternimal that is running the image
```
AWS_SECRET_ACCESS_KEY
AWS_ACCESS_KEY_ID
AWS_SESSION_TOKEN
```
Once that is confirmed via `printenv`, run `./run_container.sh`. That will run an interactive session with the docker image where you can see all logs live. To make requests to it, use the same commands as AWS, but instead of the load balancer endpoint, use `localhost` instead. 
It will run on port `8050` and can be closed with `crtl-c`.


# Tests that must pass E2E


### Deploys on AWS

The deployment steps in the documentation can be followed and the service is deployed sucessfully.

### S3 bucket is created

Check than an S3 bucket called "ecs-datareview-test-sfl-bucket" is created/exists.

### S3 bucket can be shared

See that there are no errors that mention S3 from any of the tasks running for the service on AWS ECS.



### Domain-Study Compatibility
Test the data ingestion across multiple domains and studies.
No errors are raised.


### Test POST requests to predict route


Test that the following call to predict will run the model inference
```
curl -H "Content-Type: application/json" -d '{"metadata_table":"fake_metadata","query_table": "fake_query","audit_table": "fake_audit","endpoint": "ed1kb6a1d66qnsk.ck630tcb41d7.us-east-2.rds.amazonaws.com","database": "testdata","user": "adminuser","password": "password"}' -X POST localhost:8050/datareview/predict
```


### Test POST requests to train route

Test that the following call to train will run the model training
```
curl -H "Content-Type: application/json" -d '{"metadata_table":"fake_metadata","query_table": "fake_query","audit_table": "fake_audit","endpoint": "ed1kb6a1d66qnsk.ck630tcb41d7.us-east-2.rds.amazonaws.com","database": "testdata","user": "adminuser","password": "password"}' -X POST localhost:8050/datareview/train
```

