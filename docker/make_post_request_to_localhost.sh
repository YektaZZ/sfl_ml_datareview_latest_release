#!/bin/bash

# predict
#curl -H "Content-Type: application/json" -d '{"metadata_table":"fake_metadata","query_table": "fake_query","audit_table": "fake_audit","endpoint": "ed1kb6a1d66qnsk.ck630tcb41d7.us-east-2.rds.amazonaws.com","database": "testdata","user": "adminuser","password": "password"}' -X POST localhost:8050/datareview/predict
# train
curl -H "Content-Type: application/json" -d '{"metadata_table":"fake_metadata","query_table": "fake_query","audit_table": "fake_audit","endpoint": "ed1kb6a1d66qnsk.ck630tcb41d7.us-east-2.rds.amazonaws.com","database": "testdata","user": "adminuser","password": "password"}' -X POST localhost:8050/datareview/train
