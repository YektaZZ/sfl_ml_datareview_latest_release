# Data Review Service Deployment and Docker Image building/testing
## 1.0 Image Building and ECR Deployment

The Data Review service consists of the containerization of application code and an S3 bucket for data cache. That application code must be packaged in a Dockerfile and be available on a repository on AWS or dockerhub. In order to push to ECR you'll need to do the following steps.

### 1.1 Creating a repo in ECR

From the command line create a repo on AWS. The format is below:

`aws ecr create-repository  --repository-name {repositoryName} --region {region}`


This example below would have the following output. 

`aws ecr create-repository  --repository-name datareview --region us-east-2`

```
{
    "repository": {
        "repositoryArn": "arn:aws:ecr:us-east-2:003440510767:repository/rbm",
        "registryId": "003440510767",
        "repositoryName": "datareview",
        "repositoryUri": "003440510767.dkr.ecr.us-east-2.amazonaws.com/rbm",
        "createdAt": 1601922723.0,
        "imageTagMutability": "MUTABLE"
    }
}
```

### 1.2 Setting the S3 Bucket

Before building the image, there is a variable to change in the python code to set the name of the S3 bucket that is used as a data cache.
By default it is `"ecs-datareview-test-sfl-bucket"`. It is located on line 36 of `server.py` in this same directory.


### 1.2 Building your image

From the root of this code repository, run

`docker build -t {repositoryUri} -f {path_to_Dockerfile}`  where `repositoryUri` is the `repositoryURI` that was printed to the terminal when you created the ECR Repo. To build the image you need to be in the root directory of this code repository (one level up from this `/docker` directory where the `src`, `data` and `docker` directories are all subdirectories). This will build your image locally. Once it is built you can push it to ECR. 

The structure of the command will look like this:

`docker build -t {repositoryUri}:latest -f docker/dockerfile .`

For example:

`docker build -t 003440510767.dkr.ecr.us-east-2.amazonaws.com/datareview -f docker/dockerfile .`

If you have not built this image locally on your machine it will take a few minutes because docker needs to create the layers for the image. 

### 1.3 Pushing your image to ECR 

Before you can push you must login to ECR via docker. You should alredy be logged in but just in case if you just created a repo. 

This is the command to log into the AWS ECR repo:

`aws ecr get-login-password --region {region} | docker login --username AWS --password-stdin {AWS ACCOUNT ID}.dkr.ecr.{region}.amazonaws.com`

For example:

`aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 003440510767.dkr.ecr.us-east-2.amazonaws.com`

Pushing with docker to AWS looks like this. Make sure your tag is `latest`:

`docker push {your-repositoryUri}:latest` 

An example would look like this. This too may take about 5-10 minutes to complete. 

`docker push 003440510767.dkr.ecr.us-east-2.amazonaws.com/datareview:latest`

### 1.4 Confirm

* `repositoryName`: this is the value you gave to the repo when you created the repository. 

`aws ecr list-images --repository-name {repositoryName}`

For example if your `repositoryName` is `datareview` then do:

`aws ecr list-images --repository-name datareview` 


```
{
    "imageIds": [
        {
            "imageDigest": "sha256:182e354e0790ad42da7ab43f4f3cda4d67662fe93c476e9605af20079b6d86ea"
        },
        {
            "imageDigest": "sha256:44723cfc25c7403636a366cc21075eec5df5eaa57d0109ecd7bf66f8ace6569e",
            "imageTag": "latest"
        }
    ]
}
```

As you push updates to this repo, the list of images will grow. The last image at the bottom will have the `imageTag` `latest` indicating this is the most recently added. 


## 2.0 Service Deployment

### 2.1 Creating a deploy.ini file

Now that the ECR repo is made, the next step is to update the missing settings from the `deploy_datareview.ini` file

The fields that are left blank in the service section are
 - `ECRRepo` This is where to put the URI from earlier steps
 - `ExecutionRoleARN` This will be covered later in section 2.2.2
 - `ContainerCpu` The ammount of CPU allocated to the docker instance
 - `ContainerMemory` The ammount of memory allocated to the docker instance

The fields that are left blank in the cluster section are
 - `ClusterStackName`
 - `LogBucketName`
 - `VPCID`
 - `IGID`

All fields in the cluster section should be the same as any previously deployed services, in order to deploy into the same cluster.

For contianer CPU and memory, 1024 and 2048 for each respectively will work for calls to predict, however larger values are recommended for any calls to train.

### 2.2.0 Checking if the Cluster must be Deployed

If the cluster has not yet been deployed and this is the first service, follow steps 2.2.1 and 2.2.2.
If the cluster has already been deployed, make sure that all fields in the cluster section match the deployed cluster and skip to step 2.2.2
### 2.2.1 Deploying the Cluster
If a cluster has not yet been deployed, follow the steps in the AI Architecture Repo `deployment/milestone_1_private/README.md` up to and including step two, to populate the `deploy_datareview.ini` file.
Next once the cluster section of the `deploy_datareview.ini` file is populated, copy it to the `AI Architecture Repo/deployment/milestone_1_private/` directory. Next follow step three in the readme, to deploy the cluster

### 2.2.2 Deploying the Service

The Data Review service requires an execution role that allows it to access S3 buckets. To create that role there is the cloudformation template `S3AccessRole.yaml`. To deploy that template, use the command:
```
aws cloudformation deploy  \
	--stack-name ecs-ecstask-s3accessrole \
	--template-file=S3AccessRole.yaml \
    --parameter-overrides \
		RoleName=ECSTaskS3AccessRole \
	--capabilities CAPABILITY_NAMED_IAM \
	--tags Project="eCS AI Architecture" \
&& \
aws cloudformation describe-stacks --stack-name ecs-ecstask-s3accessrole | grep OutputValue

```

This will output a role ARN to the console that will look like
```
Waiting for changeset to be created..
Waiting for stack create/update to complete
Successfully created/updated stack - ecs-ecstask-s3accessrole
                    "OutputValue": "arn:aws:iam::003440510767:role/ECSTaskS3AccessRole",
```
The value inside the `"`'s after `"OutputValue":` is what is used to populate the `ExecutionRoleARN` field of the `deploy_datareview.ini` file.


Copy the fully populated `deploy_datareview.ini` file to the `AI Architecture Repo/deployment/milestone_1_private/` directory if you have not done so already.
Next follow step four in the `deployment/milestone_1_private/README.md` readme, to deploy the service.



## 3.0 Data Cache


### 3.1 Data Cache Layout

Data is cached in an S3 bucket

Each database that is connected to by any instance of the Data Review Service has its own directory in the S3 bucket.
Each directory is named the same as the endpoint for the database thats data is cached.
Inside each directory has the records cache file `records.pkl`

There is also a folder called model, this must be made manually once the Data Review service is deployed. See section 3.2 for details



```
S3 Bucket
   ├── model
   │   ├── encoder.pkl
   │   └── model.h5
   ├── endpoint_1
   │   └── records.pkl
   ├── endpoint_2
   │   └── records.pkl
   ├── endpoint_3
   │   └── records.pkl
```


### Data Cache Initial Setup

Once the Data Review service has been deployed. The model.h5 file and the encoder.pkl file must be uploaded to the S3 cache bucket in the `model` folder (it will have to be made).
The names of the files must be the same as the values set to `model_file_name` and `encoder_file_name` in `server.py`
```
# model and encoder folder
model_folder = "model"

# model file name
model_file_name = "fake_model.h5"
# encoder file name
encoder_file_name = "fake_encoder.pkl"
# name of the cache file to check
pickel_file_name = "fake_records.pkl"
```
If the file names are different, then they either must be renamed in the S3 bucket, or the values must be changed in `server.py` and the service must be rebuilt and redeployed.


## 4.0 Code Structure

When starting up a new copy of the Data Review service, an S3 bucket for data cache is created if it does not already exist, if it does exist, multiple instances of the Data Review service will use the same cache.

### Request Handling Code Path

When a POST request is recieved at either the `train` or `predict` routes, the following happens:
1. A check that a folder in the cache that corrisponds to the database endpoint in the request is done
    - If there is a folder already in the cache for that database endpoint, that is used
    - If the folder does not already exist, one is created and a variable set to ensure that when preprocessing the data, that it uses all the data and saves it to the cache
1. There are two paths that can happen now, 
    1. There was cached data to retrieve
        1. Cached data is saved localy
        1. The database is queried for all data newer than the cached data.
        1. If there is newer data, it is used to update the preprocessed data
        1. The updated preprocessed data is saved to the cache
    1. There was not cached data to retrieve
        1. All data in the query and audit tables is used to create preprocessed data files
        1. The preprocessed data is saved to the cache
1. The local preprocessed data is used in the `predict` or `train` call and a response is returned

## Testing

See the docs [here](testing.md) for testing
