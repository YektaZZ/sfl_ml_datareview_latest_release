#!/bin/bash


#docker run -it --network host -v $(cd .. && pwd):/app ecsdatareview
#docker run -it --network host -v $(cd .. && pwd):/app ecsdatareview bash

#docker run -it -p 8050:8050 -v $(cd .. && pwd):/app -v $HOME/.aws:/root/.aws:ro ecsdatareview bash


# run with aws access
AWS_SECRET_ACCESS_KEY=$(printenv | grep AWS_SECRET_ACCESS_KEY | cut -d '=' -f2)
AWS_ACCESS_KEY_ID=$(printenv | grep AWS_ACCESS_KEY_ID | cut -d '=' -f2)
AWS_SESSION_TOKEN=$(printenv | grep AWS_SESSION_TOKEN | cut -d '=' -f2)

# bash mode
#docker run -it --rm \
#   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
#   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
#   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
#   -v $(pwd)/server.py:/app/server.py \
#   -v $(cd ../src && pwd):/app/ \
#   -v $(cd ../ && pwd):/code/ \
#   -p 8050:8050 ecsdatareview bash
#
#exit 0

# running mode
docker run -it --rm \
   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
   -v $(pwd)/server.py:/app/server.py \
   -v $(cd ../src && pwd):/app/ \
   -p 8050:8050 ecsdatareview 


# orig
#docker run -it --rm \
#   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
#   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
#   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
#   -p 8050:8050 ecsdatareview


   #-v $(cd .. && pwd):/app \
   # ecsdatareview bash