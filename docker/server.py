import logging

import boto3

import configparser
import sys
from typing import Optional, Set, Any, Dict


import pyodbc

import os
import time

from fastapi import FastAPI

import socket
import numpy as np
from datetime import datetime

import preprocesses as preprocessor
import infer as model_predict
import train as model_train


logging.basicConfig(level=logging.INFO)
logging.info("Loading Model DataReview")

app = FastAPI(
    title="eCS Data Review docs",
    description="API docs for the updated eCS Architecture",
    openapi_url="/datareview/openapi.json",
    redoc_url="/datareview/redoc",
    docs_url="/datareview/docs",
    version="0.0.1",
)

# Change this to change the S3 bucket that is used as cache
bucket = "ecs-datareview-test-sfl-bucket"


# static assumed variables

# model and encoder folder
model_folder = "model"

# model file name
model_file_name = "fake_model.h5"
# encoder file name
encoder_file_name = "fake_encoder.pkl"
# name of the cache file to check
pickel_file_name = "fake_records.pkl"


def get_database_connection(endpoint, db_user, db_pass, database):
    """
    Returns a database connection

    Args:
        endpoint (string): database endpoint to connect to
        db_user (string): database user
        db_pass (string): database password
        database (string): name of database to use

    Returns:
        [database connection]: [the current connection the the database]
    """
    try:
        conn = pyodbc.connect(
            "DRIVER={ODBC Driver 17 for SQL Server};SERVER="
            + endpoint
            + ";DATABASE="
            + database
            + ";UID="
            + db_user
            + ";PWD="
            + db_pass
        )
    except Exception as e:
        print(e)
        return None

    return conn


def sql_to_csv(sql_conn, sql_query, header_line, csv_file):
    """
    writes the output of a sql query to a csv file with an optional header line
    overwrites the csv_file if exists

    Args:
        sql_conn (database connection): database connection object
        sql_query (string): sql query to run to get output
        header_line (string): header line of csv file, for column names, optional
        csv_file (string): path to csv file to write output to
    """
    # get data from sql db
    cursor = sql_conn.cursor()
    cursor.execute(sql_query)

    # write output of query to csv file
    with open(csv_file, "w") as csv_:
        # add header
        if header_line:
            csv_.write(header_line)

        i = 0
        for row in cursor:
            # add row num
            csv_.write("\n" + str(i))
            # write each entry
            for entry in list(row):
                csv_.write("," + str(entry).replace(",", "").replace("\n"," "))
            i += 1


def generate_csv_header_line_from_table_columns(sql_conn, table):
    """
    makes a string out of all the column names of a sql table to be a header line for csv file

    Args:
        sql_conn (database connection): database connection object
        table (string): sql table to get column names for

    Returns:
        string: returns a string of column names with ',' before each
    """

    # base sql query
    parts = table.split(".")

    a = parts[0].replace('[','').replace(']','')
    b = parts[1].replace('[','').replace(']','')

    sql_query = (
        "SELECT * FROM INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = '{}' AND TABLE_NAME = '{}';".format(a,b)
    )



    col_names = []

    # connect to db and get col names for table
    cursor = sql_conn.cursor()
    cursor.execute(sql_query)
    for row in cursor:
        col_names.append(list(row)[3])

    # combine col names into header line
    return_string = ""
    for col in col_names:
        return_string += "," + col

    return return_string


def get_csv_from_sql_query(sql_conn, query, table, output_file_name):
    """
    Makes a csv file from sql query with header string

    Args:
        sql_conn (database connection): database connection object
        query (string): query to make in the database to get the csv values
        table (string): which table is being queried, for header generation
        output_file_name (string): output file name
    """

    # header line for the csv file
    header_line = generate_csv_header_line_from_table_columns(sql_conn, table)

    sql_to_csv(sql_conn, query, header_line, output_file_name)


def make_timestamp_query(sql_conn, table, timestamp_since):
    # format date string for easy auto-convert to datetime in sql
    date_string = timestamp_since.isoformat()
    date_string = str(date_string).replace("T", " ")
    date_string = date_string[: date_string.rfind("+")] + ".000"

    # get table headers from database
    col_names = generate_csv_header_line_from_table_columns(sql_conn, table)[1:]

    if "query" in table.lower():
        # query query
        # assumes 126	yyyy-mm-ddThh:mi:ss.mmm	ISO8601 datetime
        # 2018-02-15 13:45:24.000 is example in audit table
        query = "SELECT {} FROM {} WHERE OpenDate  > CONVERT(DATETIME,'{}');".format(
            col_names, table, date_string
        )
    else:
        # audit query
        query = (
            "SELECT {} FROM {} WHERE DateTimeStamp  > CONVERT(DATETIME,'{}');".format(
                col_names, table, date_string
            )
        )

    return query


def get_modified_timestamp_of_file_in_S3(key):
    """[summary]
    Gets datetime stamp of last modified time of a file in S3
    Args:
        file_to_get_timestamp (string): S3 key to get timestamp of

    Returns:
        string: datetime timestamp string of last modified time None if not found
    """
    s3 = boto3.client("s3", region_name="us-east-2")
    # get objects that match a search for the key
    objects = s3.list_objects(Bucket=bucket, Prefix=key)
    # if object is not found, return none
    if "Contents" not in objects.keys():
        return None
    for o in objects["Contents"]:
        if o["Key"] == key:
            # return this keys last modified value as a datetime string
            return o["LastModified"]
    return None


# S3 management


def get_files_from_s3(folder, list_of_files):
    """[summary]
    List of files from the S3 folder


    Args:
        folder (string): folder key in S3 bucket
        list_of_files ([string]): list of files in the folder to get

    Returns:
        [boolean]: returns true if sucess to get all files
    """
    s3 = boto3.resource("s3")
    try:
        for f in list_of_files:
            s3_file_path = folder + "/" + f
            s3.Bucket(bucket).download_file(s3_file_path, f)
    except BaseException:
        return False
    return True


def put_files_in_s3(folder, list_of_files):
    """[summary]
    Upload all files in list from local to the S3 folder

    Args:
        folder (string): folder key in S3 bucket
    """
    s3 = boto3.resource("s3")
    for f in list_of_files:
        s3_file_path = folder + "/" + f
        s3.Bucket(bucket).upload_file(f, s3_file_path)


def ensure_s3_bucket_exists():
    """
    Checks that the S3 bucket thats name is set in the global "bucket" variable, exists
    If it does not exist, it will create it with all public access blocked
    """
    # check if bucket exists
    s3 = boto3.resource("s3")
    bucket_exists_and_can_access = s3.Bucket(bucket) in s3.buckets.all()

    if not bucket_exists_and_can_access:
        # create S3 client and get current region
        s3_client = boto3.client("s3")
        region = s3_client.meta.region_name
        # create the bucket
        if region == "us-east-1":
            # if the region is us-east-1 then location must be missing
            s3_bucket = s3_client.create_bucket(Bucket=bucket)
        else:
            # if location is not us-east-1 then specify it to the bucket
            s3_bucket = s3_client.create_bucket(
                Bucket=bucket,
                CreateBucketConfiguration={"LocationConstraint": region},
            )
        # set its access config to not public
        # set its access config to not public
        response_public = s3_client.put_public_access_block(
            Bucket=bucket,
            PublicAccessBlockConfiguration={
                "BlockPublicAcls": True,
                "IgnorePublicAcls": True,
                "BlockPublicPolicy": True,
                "RestrictPublicBuckets": True,
            },
        )


def ensure_s3_folder_exists(folder):
    """
    Creates folder in S3 if it didnt exist
    Returns whether it had to create folder or not

    Args:
        folder (string): folder key in S3
    Returns:
        boolean: returns false if had to create folder, true if it alreay existed
    """

    client = boto3.client("s3")
    # look at all objects in bucket
    response = client.list_objects_v2(Bucket=bucket, Prefix=folder)
    # if folder name is part of any object key, return true
    if response:
        if "Contents" in response.keys():
            for obj in response["Contents"]:
                if folder in obj["Key"]:
                    return True
    # if didnt return true, make the folder and return false
    client.put_object(Bucket=bucket, Key=(folder + "/"))
    return False


# preprocessing
def update_local_pkl_files(request, files_exist_in_S3):
    """[summary]
    Updates local pki files so predict/train can assume they are there

    Args:
        request (dict): main request dict with data
        files_exist_in_S3 (boolean): whether the files are already in S3 or must be made new
    """

    # make database connection
    conn = get_database_connection(
        request["endpoint"], request["user"], request["password"], request["database"]
    )

    if conn is None:
        print("no con")
        return

    if files_exist_in_S3:
        # get files from S3 to update or use
        get_files_from_s3(request["endpoint"], [pickel_file_name])

        # get the timestamps for both files from S3
        pickel_timestamp = get_modified_timestamp_of_file_in_S3(
            request["endpoint"] + "/" + pickel_file_name
        )

        # if the pickle file is not found, call this again but with not found
        if not pickel_timestamp:  # or True:
            update_local_pkl_files(request, False)
            return

        # generate timestamp queries
        audit_query = make_timestamp_query(
            conn, request["audit_table"], pickel_timestamp
        )
        query_query = make_timestamp_query(
            conn, request["query_table"], pickel_timestamp
        )

        # get csv files from queries
        get_csv_from_sql_query(conn, audit_query, request["audit_table"], "audit.csv")
        get_csv_from_sql_query(conn, query_query, request["query_table"], "query.csv")
        # check if the csv files have content
        need_to_update_local_cache = False
        with open("query.csv", "r") as query_csv, open("audit.csv", "r") as audit_csv:
            audit_lines = audit_csv.readlines()
            query_lines = query_csv.readlines()
            # check that they have more than the header line
            need_to_update_local_cache = len(audit_lines) > 1 or len(query_lines) > 1

        if need_to_update_local_cache:

            # do update pkl files with preprocessor part
            use_preprocessor_to_update_local_pkl_files()

            # push files back to S3
            put_files_in_s3(request["endpoint"], [pickel_file_name])

    else:
        # generate queries
        audit_query = "SELECT * FROM {};".format(request["audit_table"])
        query_query = "SELECT * FROM {};".format(request["query_table"])

        # get csv files from queries
        get_csv_from_sql_query(conn, audit_query, request["audit_table"], "audit.csv")
        get_csv_from_sql_query(conn, query_query, request["query_table"], "query.csv")

        # do update pkl files with preprocessor part
        use_preprocessor_to_update_local_pkl_files()

        # push files back to S3
        put_files_in_s3(request["endpoint"], [pickel_file_name])

    # also update metadata each time
    metadata_query = "SELECT * FROM {};".format(request["metadata_table"])

    # get csv files from queries
    get_csv_from_sql_query(
        conn, metadata_query, request["metadata_table"], "metadata.csv"
    )

    return


def use_preprocessor_to_update_local_pkl_files():
    """[summary]
    Calls the preprocessor to update the pickle file using the data csvs
    """

    preprocessor.make_record_collection("audit.csv", "query.csv", pickel_file_name)

    return


# App routes


@app.get("/datareview/")
def home():
    """
    Perform regular status checks to make sure the container is up and responding

    Returns
    =======
    str
    """

    return {"output": "Hello World I am the DataReview Model"}


@app.post("/datareview/train")
def train(request: Dict[Any, Any]):

    # to get a model and encoder files from S3
    # overwrites any existing in case of updates
    got_model_and_encoder = get_files_from_s3(
        model_folder, [model_file_name, encoder_file_name]
    )
    # if fails to get model, output an error
    if not got_model_and_encoder:
        error_message = (
            "Make sure {} and {} exist in the bucket {} in the folder {}".format(
                model_file_name, encoder_file_name, bucket, model_folder
            )
        )
        return {"output": error_message}

    # ensure that the folder for the cache exists (using endpoint as folder
    # name)
    folder_existed_prior = ensure_s3_folder_exists(request["endpoint"])

    # update local pkl files so predict can assume they are present to load
    update_local_pkl_files(request, folder_existed_prior)

    # run training
    model_train.train_model(
        pickel_file_name, "metadata.csv", model_file_name, encoder_file_name
    )

    # save model file to S3
    put_files_in_s3(model_folder, [model_file_name, encoder_file_name])

    output = {"status": "training completed"}

    return output


@app.post("/datareview/predict")
def predict(request: Dict[Any, Any]):

    # to get a model and encoder files from S3
    # overwrites any existing in case of updates
    got_model_and_encoder = get_files_from_s3(
        model_folder, [model_file_name, encoder_file_name]
    )
    # if fails to get model, output an error
    if not got_model_and_encoder:
        error_message = (
            "Make sure {} and {} exist in the bucket {} in the folder {}".format(
                model_file_name, encoder_file_name, bucket, model_folder
            )
        )
        return {"output": error_message}

    # ensure that the folder for the cache exists (using endpoint as folder
    # name)
    folder_existed_prior = ensure_s3_folder_exists(request["endpoint"])

    # update local pkl files so predict can assume they are present to load
    update_local_pkl_files(request, folder_existed_prior)

    # run inference
    output = model_predict.identify_queries(
        pickel_file_name, "metadata.csv", model_file_name, encoder_file_name, date=None
    )

    return output


# check that the S3 bucket exists on creation
ensure_s3_bucket_exists()
