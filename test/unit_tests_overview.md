# Unit Tests Overview

Based on the first pass of unit tests, these were the fail criteria I found. This document is separated by scripts (e.g. Dataset) and test functions (e.g. test_dataset_query_required_fields).

## Dataset
### test_dataset_empty_init
QueryDataset cannot be initialized with empty params

#### test_dataset_query_required_fields
Code fails with the empty init params, need to load the dataset before processing for this test.

### test_dataset_domain_study_compatibility
QueryDataset cannot be initialized with empty params



## DataWindow
### test_datawindow_input_output_types
np (numpy) is not defined



## Event
### test_event_empty_init
EditEvent fails because it does not inherit from Event class

### test_event_class_inheritance
The EditEvent class does not inherit from the Event class

### test_event_query_is_open
Expected is_open() to return True when close date is None

### test_event_query_is_manual
Expected is_manual() to return False when query recipient is NULL or None

### test_event_from_dict_type
Should raise an error if the from_dict method receives an empty dict



## EventHistory
### test_eventhistory_edit_required_date	
`to_edit_dict` is not defined



## OrdinalEncoder
### test_ordinalencoder_required_string_value_len
Currently generates a ZeroDivisionError, should generate ValueError (or QuantityError?)

### test_ordinalencoder_fit_input
The function does not raise a TypeError for these inputs



## OrdinalEncoderCollection
### test_ordinalencodercollection_items_to_dtype_errors
Currently generates no error, should generate ValueError

### test_ordinalencodercollection_init_input
The function does not raise a TypeError for non-dict inputs

### test_ordinalencodercollection_fit_input
The function does not raise a TypeError for non-dict inputs



## Record
### test_record_add_edit_history
`to_edit_dict` is not defined	

### test_record_add_query_history
The event_record is never added to the events

### test_record_state_type 
The state method does not return dict

### test_record_study_table_compatibility
`to_edit_dict` is not defined



## RecordCollection
### test_recordcollection_make_records
Not sure why the populated flag is False

### test_recordcollection_study_table_compatibility
`to_edit_dict` is not defined











