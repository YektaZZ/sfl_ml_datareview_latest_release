import os
import pytest
import pickle
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from dataset import Dataset, QueryDataset, AuditDataset
from record import Record
from recordcollection import RecordCollection


## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))

PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)

DICT_AUDIT_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_audit_df.p"),
        "rb",
    )
)
DICT_QUERY_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_query_df.p"),
        "rb",
    )
)


"""
-----------------------
--recordcollection.py--
-----------------------

1. Check initialization state
2. Check for required fields
3. Check record value can be properly added/removed
4. Check types of inputs/outputs
"""


@pytest.fixture
def recordcollection():
    audit_dataset = AuditDataset(path=PATH_AUDIT_DATASET)
    query_dataset = QueryDataset(path=PATH_QUERY_DATASET)
    recordcollection = RecordCollection()
    recordcollection.make_manifest(audit_dataset)
    recordcollection.initialize_records()
    recordcollection.make_records(audit_dataset, query_dataset)
    return recordcollection


@pytest.fixture
def event_record_query():
    return pd.DataFrame(
        {
            "OpenDate": ["2200/01/01", "2200/02/01", "2200/03/01"],
            "CloseDate": ["2200/01/01", "2200/02/01", "2200/03/01"],
            "QueryOpenUserOID": ["XYZ", "XYZ", "XYZ"],
            "QueryRecipient": ["ABC", "ABC", "ABC"],
            "ItemOID": ["123.0", "123.0", "123.0"],
            "ItemValue": ["75.4", "72.9", "81.0"],
        }
    )


# 1. Check initialization state


@pytest.mark.parametrize(
    "attribute, expected_value",
    [
        ("records", {}),
        ("manifest", None),
        ("populated", False),
    ],
)
def test_recordcollection_empty_init(attribute, expected_value):
    """ Test the default values for an empty initialized RecordCollection class. """
    recordcollection = RecordCollection()
    assert (
        getattr(recordcollection, attribute) == expected_value
    ), f"Empty initialized Record does not have the default {attribute} = {expected_value}"


# 2. Check for required fields


@pytest.mark.parametrize(
    "missing_field, expected_error",
    [
        ("ItemGroupRecordId", KeyError),
        ("DateTimeStamp", KeyError),
        ("CreationDate", KeyError),
        ("SubjectName", KeyError),
        ("FormOID", KeyError),
    ],
)
def test_recordcollection_make_manifest_required_fields(missing_field, expected_error):
    """ Test that a missing_field raises the proper expected_error in the RecordCollection method make_manifest. """
    with pytest.raises(expected_error):
        audit_dataset = AuditDataset()
        audit_dataset.load_csv(path="data/raw/test_dataset_audit_df.csv")
        audit_dataset.dataset = audit_dataset.dataset.drop(columns=missing_field)
        recordcollection = RecordCollection()
        recordcollection.make_manifest(audit_dataset)


# NOTE: This test is already covered by the test above, since
# initialize_records depends on make_manifest


@pytest.mark.parametrize(
    "missing_field, expected_error",
    [
        ("ItemGroupRecordId", KeyError),
        ("SubjectName", KeyError),
        ("FormOID", KeyError),
    ],
)
def test_recordcollection_initialize_records_required_fields(
    missing_field, expected_error
):
    """ Test that a missing_field raises the proper expected_error in the RecordCollection method initialize_records. """
    with pytest.raises(expected_error):
        audit_dataset = AuditDataset(path="data/raw/test_dataset_audit_df.csv")
        audit_dataset.dataset = audit_dataset.dataset.drop(columns=missing_field)
        recordcollection = RecordCollection()
        recordcollection.make_manifest(audit_dataset)
        recordcollection.initialize_records()


# 3. Check records can be properly added


def test_recordcollection_make_records():
    """ Test that the RecordCollection method make_records are properly added. """
    audit_dataset = AuditDataset(path=PATH_AUDIT_DATASET)
    query_dataset = QueryDataset(path=PATH_QUERY_DATASET)
    recordcollection = RecordCollection()
    recordcollection.make_manifest(audit_dataset)
    recordcollection.initialize_records()
    recordcollection.make_records(audit_dataset, query_dataset)
    assert recordcollection.populated, "The RecordCollection was not populated."


# 4. Check types of inputs/outputs


@pytest.mark.parametrize(
    "method, params, expected_type",
    [
        ("get_items", "", set),
        ("count_snapshots", [["ItemOID.1"]], int),
    ],
)
def test_recordcollection_method_types(recordcollection, params, method, expected_type):
    """ Test the return types for methods in the RecordCollection class. """
    assert isinstance(
        getattr(recordcollection, method)(*params), expected_type
    ), f"The method {method} did not return the expected type {expected_type}."


# 5. Check for compatibility across studies and tables


@pytest.mark.parametrize(
    "method, study_table_name, study_table_dataset",
    [("add_edit_history", name, data) for name, data in DICT_AUDIT_DATASET.items()]
    + [("add_query_history", name, data) for name, data in DICT_QUERY_DATASET.items()],
)
def test_recordcollection_study_table_compatibility(
    method, study_table_name, study_table_dataset
):
    """
    Test that no errors are raised when adding
    records across multiple studies and tables.

    NOTE: study_table_name is included for debugging test failures,
    but is never used in the test
    """
    record = Record()
    # record.add_query_history(event_record_query)
    getattr(record, method)(study_table_dataset)
