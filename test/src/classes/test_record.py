import os
import pytest
import pickle
import numpy as np
import pandas as pd

import sys

sys.path.append("src/classes")
from record import Record


## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))

PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)

DICT_AUDIT_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_audit_df.p"),
        "rb",
    )
)
DICT_QUERY_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_query_df.p"),
        "rb",
    )
)


"""
-------------
--record.py--
-------------

1. Check initialization state
2. Check history states (additions, rollbacks)
3. Check types of inputs/outputs
4. Check for compatibility across studies and tables
"""


@pytest.fixture
def event_record_edit():
    return pd.DataFrame(
        {
            "DateTimeStamp": ["2200/01/01", "2200/02/01", "2200/03/01"],
            "ItemOID": ["123.0", "123.0", "123.0"],
            "ItemValue": ["75.4", "72.9", "81.0"],
            "UserOID": ["0", "1", "2"],
            "AuditSubCategoryName": ["a", "b", "c"],
        }
    )


@pytest.fixture
def event_record_query():
    return pd.DataFrame(
        {
            "OpenDate": ["2200/01/01", "2200/02/01", "2200/03/01"],
            "CloseDate": ["2200/01/01", "2200/02/01", "2200/03/01"],
            "QueryOpenUserOID": ["XYZ", "XYZ", "XYZ"],
            "QueryRecipient": ["ABC", "ABC", "ABC"],
            "ItemOID": ["123.0", "123.0", "123.0"],
            "ItemValue": ["75.4", "72.9", "81.0"],
        }
    )


# 1. Check initialization state


@pytest.mark.parametrize(
    "attribute, expected_value",
    [
        ("record_id", 0),
        ("subject_id", 0),
        ("form", ""),
    ],
)
def test_record_empty_init(attribute, expected_value):
    """ Test the default values for an empty initialized event class. """
    record = Record()
    assert (
        getattr(record, attribute) == expected_value
    ), f"Empty initialized Record does not have the default {attribute} = {expected_value}"


# 2. Check history states (additions, rollbacks)


def test_record_add_edit_history(event_record_edit):
    """ Test that the add_edit_history function updates the edit_history attribute. """
    record = Record()
    record.add_edit_history(event_record_edit)
    assert (
        record.edit_history.events != []
    ), "The event_record (edit) was not added to the Record object."


def test_record_add_query_history(event_record_query):
    """ Test that the add_query_history function updates the edit_history attribute. """
    record = Record()
    record.add_query_history(event_record_query)
    assert (
        record.query_history.events != []
    ), "The event_record (query) was not added to the Record object."


# 3. Check types of inputs/outputs


def test_record_get_ItemOIDS_type(event_record_query):
    """ Test that the get_items() method returns an object of type "set". """
    record = Record()
    record.add_query_history(event_record_query)
    assert isinstance(
        record.get_items(), set
    ), 'The method get_items() did not return the expected type "set".'


def test_record_state_type(event_record_query):
    """ Test that the state() method returns an object of type "dict". """
    record = Record()
    record.add_query_history(event_record_query)
    output = record.edit_history.state(-1)
    assert isinstance(
        output, dict
    ), 'The method state() did not return the expected type "dict".'


# 4. Check for compatibility across studies and tables


@pytest.mark.parametrize(
    "method, study_table_name, study_table_dataset",
    [("add_edit_history", name, data) for name, data in DICT_AUDIT_DATASET.items()]
    + [("add_query_history", name, data) for name, data in DICT_QUERY_DATASET.items()],
)
def test_record_study_table_compatibility(
    method, study_table_name, study_table_dataset
):
    """
    Test that no errors are raised when adding
    records across multiple studies and tables.

    NOTE: study_table_name is included for debugging test failures,
    but is never used in the test
    """
    record = Record()
    getattr(record, method)(study_table_dataset)
