import os
import pytest
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from ordinalencoder import (
    OrdinalEncoder,
    NumericOrdinalEncoder,
    CategoricalOrdinalEncoder,
    DateTimeOrdinalEncoder,
)
from ordinalencodercollection import OrdinalEncoderCollection

## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))
PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)


"""
-------------------------------
--ordinalencodercollection.py--
-------------------------------

1. Check initialization state
2. Check for required fields (error handling)
3. Check function return values
4. Check types of inputs/outputs
"""

# 1. Check initialization state


@pytest.mark.parametrize(
    "OrdinalEncoderCollectionClass, attribute, expected_value",
    [
        (OrdinalEncoderCollection, "items_to_dtype", {}),
        (OrdinalEncoderCollection, "encoders", {}),
    ],
)
def test_ordinalencodercollection_empty_init(
    OrdinalEncoderCollectionClass, attribute, expected_value
):
    """ Test the default values for an empty initialized OrdinalEncoderCollectionClass class. """
    ordinalencodercollection = OrdinalEncoderCollectionClass()
    assert (
        getattr(ordinalencodercollection, attribute) == expected_value
    ), f"Empty initialized OrdinalEncoderCollectionClass does not have the expected {attribute} = {expected_value}"


# 2. Check for required fields (error handling)


@pytest.mark.parametrize(
    "input_value, expected_error",
    [
        ({"Item1": "Other"}, ValueError),
    ],
)
def test_ordinalencodercollection_items_to_dtype_errors(input_value, expected_error):
    """
    items_to_dtype looks like:
            {"Item1":"Character", "Item2":"Numeric", "Item3":"DateTime"}

    The items_to_dtype must raise an error if the value is not one of the following:
            - "Character"
            - "Numeric"
            - "DateTime"
    """
    with pytest.raises(expected_error):
        ordinalencodercollection = OrdinalEncoderCollection(items_to_dtype=input_value)


# 3. Check function return values


@pytest.mark.parametrize(
    "items_to_dtype, item_to_string_value_set, expected_value",
    [
        (
            {"item": "Character"},
            {"item": ["asteroid", "planet", "star"]},
            {"asteroid": 2, "planet": 3, "star": 4},
        ),
        (
            {"item": "Numeric"},
            {"item": [31, 53342, 8965, 74893, 99999]},
            {31: 2, 8965: 2, 53342: 2, 74893: 3, 99999: 3},
        ),
        (
            {"item": "DateTime"},
            {"item": ["2200/01/01", "1200/01/01", "3200/01/01"]},
            {"1200/01/01": 2, "2200/01/01": 2, "3200/01/01": 2},
        ),
    ],
)
def test_ordinalencodercollection_fit_return(
    items_to_dtype, item_to_string_value_set, expected_value
):
    """ Test the fit method returns the expected encoding. """
    ordinalencodercollection = OrdinalEncoderCollection(items_to_dtype=items_to_dtype)
    ordinalencodercollection.fit(item_to_string_value_set)
    output = ordinalencodercollection.encoders["item"].string_to_ordinal
    assert (
        output == expected_value
    ), f"The fit encoding {output} does not match the expected value: {expected_value}"


@pytest.mark.parametrize(
    "items_to_dtype, item_to_string_value_set, expected_value",
    [
        (
            {"item": "Character"},
            {"item": ["asteroid", "planet", "star"]},
            {"asteroid": 2, "planet": 3, "star": 4},
        ),
        (
            {"item": "Numeric"},
            {"item": [31, 53342, 8965, 74893, 99999]},
            {31: 2, 8965: 2, 53342: 2, 74893: 3, 99999: 3},
        ),
        (
            {"item": "DateTime"},
            {"item": ["2200/01/01", "1200/01/01", "3200/01/01"]},
            {"1200/01/01": 2, "2200/01/01": 2, "3200/01/01": 2},
        ),
    ],
)
def test_ordinalencodercollection_transform_return(
    items_to_dtype, item_to_string_value_set, expected_value
):
    """ Test the transform method returns the expected encoded value. """
    ordinalencodercollection = OrdinalEncoderCollection(items_to_dtype=items_to_dtype)
    ordinalencodercollection.fit(item_to_string_value_set)
    for value in item_to_string_value_set["item"]:
        transformed_value = ordinalencodercollection.transform(item="item", value=value)
        assert (
            transformed_value == expected_value[value]
        ), f"The transformed value {transformed_value} does not match the expected value: {expected_value[value]}"


# 4. Check types of inputs/outputs


@pytest.mark.parametrize(
    "input_value, expected_error",
    [
        (["item"], AttributeError),
    ],
)
def test_ordinalencodercollection_init_input(input_value, expected_error):
    """ Test that the initialization params only accepts dicts """
    with pytest.raises(expected_error):
        ordinalencodercollection = OrdinalEncoderCollection(input_value)


@pytest.mark.parametrize(
    "input_value, expected_error",
    [
        (["item"], AttributeError),
    ],
)
def test_ordinalencodercollection_fit_input(input_value, expected_error):
    """ Test that the fit function only accepts dicts of sets/lists. """
    with pytest.raises(expected_error):
        ordinalencodercollection = OrdinalEncoderCollection({"item": "Character"})
        ordinalencodercollection.fit(input_value)


@pytest.mark.parametrize(
    "input_value, expected_error",
    [
        ({"item": [], "value": []}, TypeError),
    ],
)
def test_ordinalencodercollection_transform_input(input_value, expected_error):
    """ Test that the transform function only accepts ints/strs for the item and value """
    with pytest.raises(expected_error):
        ordinalencodercollection = OrdinalEncoderCollection({"item": "Character"})
        ordinalencodercollection.fit({"item": ["x", "y", "z"]})
        ordinalencodercollection.transform(**input_value)
