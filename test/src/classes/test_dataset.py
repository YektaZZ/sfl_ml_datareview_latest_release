import os
import pytest
import pickle
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from dataset import Dataset, QueryDataset, AuditDataset

## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))

PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)

DICT_AUDIT_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_audit_df.p"),
        "rb",
    )
)
DICT_QUERY_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_query_df.p"),
        "rb",
    )
)

"""
--------------
--dataset.py--
--------------

1. Check the initialization of object
2. Check for required fields
3. Check for types
4. Check for compatability across studies and tables
"""


# 1. Check the initialization of object


@pytest.mark.parametrize(
    "DatasetClass, attribute, expected_value",
    [
        (Dataset, "dataset", None),
        (QueryDataset, "dataset", None),
        (AuditDataset, "dataset", None),
    ],
)
def test_dataset_empty_init(DatasetClass, attribute, expected_value):
    dataset = DatasetClass()
    assert (
        getattr(dataset, attribute) == expected_value
    ), f"Empty initialized Dataset does not have the default {attribute} = {expected_value}"


# 2. Check for errors based on missing fields


@pytest.mark.parametrize(
    "missing_field, expected_error",
    [
        ("AuditSubCategoryName", KeyError),
        ("ItemGroupRecordId", KeyError),
        ("DateTimeStamp", KeyError),
        ("ItemOID", KeyError),
        ("ItemValue", KeyError),
    ],
)
def test_dataset_audit_required_fields(missing_field, expected_error):
    """ Checks that the missing_field raises the proper error in the AuditDataset. """
    with pytest.raises(expected_error):
        dataset = AuditDataset()
        dataset.load_csv(path=PATH_AUDIT_DATASET)
        dataset.dataset = dataset.dataset.drop(columns=missing_field)
        dataset.process_dataset()


@pytest.mark.parametrize(
    "missing_field, expected_error",
    [
        ("OpenDate", KeyError),
        ("RecordId", KeyError),
        ("ItemOID", KeyError),
    ],
)
def test_dataset_query_required_fields(missing_field, expected_error):
    """ Checks that the missing_field raises the proper error in the QueryDataset. """
    with pytest.raises(expected_error):
        dataset = QueryDataset()
        dataset.load_csv(path=PATH_QUERY_DATASET)
        dataset.dataset = dataset.dataset.drop(columns=missing_field)
        dataset.process_dataset()


# 3. Check for types


@pytest.mark.parametrize(
    "child_class, parent_class",
    [
        (AuditDataset, Dataset),
        (QueryDataset, Dataset),
    ],
)
def test_dataset_class_inheritance(child_class, parent_class):
    """ Check that each child event class inherits from the parent class. """
    assert issubclass(
        child_class, parent_class
    ), f"The {child_class} object must inherit from the parent class {parent_class}"


# 4. Check for compatibility across studies and tables


@pytest.mark.parametrize(
    "DatasetClass, domain_study_name, domain_study_dataset",
    [(AuditDataset, name, data) for name, data in DICT_AUDIT_DATASET.items()]
    + [(QueryDataset, name, data) for name, data in DICT_QUERY_DATASET.items()],
)
def test_dataset_domain_study_compatibility(
    DatasetClass, domain_study_name, domain_study_dataset
):
    """
    Test that no errors are raised when processing
    datasets across multiple studies and domains.

    NOTE: domain_study_name is included for debugging test failures,
    but is never used in the test
    """
    dataset = DatasetClass()
    dataset.dataset = domain_study_dataset
    dataset.process_dataset()
