import os
import pytest
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from dataset import Dataset, QueryDataset, AuditDataset
from datawindow import DataWindow

## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))
PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)


"""
-----------------
--datawindow.py--
-----------------

1. Test size/values of initialized window (OrderedDict)
2. Test that DataWindow values are properly added
3. Test types of input/output
"""


@pytest.fixture
def datawindow():
    return DataWindow(window_size=5, fill_value=0)


# 1. Test types of input/output


@pytest.mark.parametrize("window_size", [(1.1), ("237"), ("large")])
def test_datawindow_window_size_type(window_size):
    """ Test that the window_size raises a TypeError for non integer types. """
    with pytest.raises(TypeError):
        DataWindow(window_size=window_size)
