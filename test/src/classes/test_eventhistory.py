import os
import pytest
import pickle
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from event import Event, EditEvent, QueryEvent
from eventhistory import EventHistory, EditEventHistory, QueryEventHistory


## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))

PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)

DICT_AUDIT_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_audit_df.p"),
        "rb",
    )
)
DICT_QUERY_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_query_df.p"),
        "rb",
    )
)


"""
-------------------
--eventhistory.py--
-------------------

1. Check for initialization of object
2. Check for required fields
3. Check event value can be properly added/removed
4. Check event methods
5. Check types of inputs/outputs
6. Check for compatibility across studies and tables
"""


@pytest.fixture
def editevent_dict():
    return {
        "0": {
            "ItemOID": "0.0",
            "ItemValue": "0",
            "DateTimeStamp": "0",
            "UserOID": "0",
            "AuditSubCategoryName": "0",
        }
    }


@pytest.fixture
def queryevent_dict():
    return {
        "OpenDate": "0",
        "CloseDate": "0",
        "QueryOpenUserOID": "0",
        "QueryRecipient": "0",
        "ItemOID": "0.0",
        "ItemValue": "0",
    }


@pytest.fixture
def editevent(editevent_dict):
    event = EditEvent()
    event.from_dict(editevent_dict)
    return event


@pytest.fixture
def queryevent(queryevent_dict):
    event = QueryEvent()
    event.from_dict(queryevent_dict)
    return event


@pytest.fixture
def event_record():
    return pd.DataFrame(
        {
            "DateTimeStamp": ["2200/01/01", "2200/02/01", "2200/03/01"],
            "ItemOID": ["123", "123", "123"],
            "ItemValue": ["75.4", "72.9", "81.0"],
        }
    )


# 1. Check for initialization of object


@pytest.mark.parametrize(
    "EventHistoryClass", [(EventHistory), (EditEventHistory), (QueryEventHistory)]
)
def test_eventhistory_empty_init(EventHistoryClass):
    """ Test the default values for an empty initialized event class. """
    eventhistory = EventHistoryClass()
    assert (
        eventhistory.events == []
    ), "Empty initialized EventHistory does not have the default events []"


# 2. Check for required fields


def test_eventhistory_edit_required_date(event_record):
    """ Test that the missing_field "DateTimeStamp" raises the proper KeyError in the EditEventHistory. """
    with pytest.raises(KeyError):
        missing_field = "DateTimeStamp"
        event_record_no_date = event_record.drop(columns=missing_field)
        editeventhistory = EditEventHistory()
        editeventhistory.from_event_record(event_record)


# 3. Check event value can be properly added/removed


def check_events_equal(event_x, event_y, attributes):
    """ Checks that two events are equal by comparing their attributes."""
    for attribute in attributes:
        if getattr(event_x, attribute) != getattr(event_y, attribute):
            return False
    return True


def test_eventhistory_edit_add_value(editevent_dict, editevent):
    """ Test that a new event can be added to the EditEventHistory. """
    attributes = ["values", "start_time", "user", "subtype"]
    eventhistory = EditEventHistory()
    eventhistory.add_event(editevent_dict)
    assert check_events_equal(
        eventhistory.events[0], editevent, attributes
    ), "New event was not added to EditEventHistory"


def test_eventhistory_query_add_value(queryevent_dict, queryevent):
    """ Test that a new event can be added to the QueryEventHistory. """
    attributes = ["start_time", "user", "subtype", "item"]
    eventhistory = QueryEventHistory()
    eventhistory.add_event(queryevent_dict)
    assert check_events_equal(
        eventhistory.events[0], queryevent, attributes
    ), "New event was not added to QueryEventHistory"


# 4. Check remaining event methods


def test_eventhistory_edit_update_state():
    """ Tests that the state() method correctly updates the EditEventHistory. """
    eventhistory = EditEventHistory()
    # Add multiple events
    event_list = []
    for value in range(5):
        # Used to create a new event with an updated ItemValue
        event_dict = {
            "0": {
                "ItemOID": "0.0",
                "ItemValue": str(value),
                "DateTimeStamp": "2200/01/0" + str(value),
                "UserOID": "0",
                "AuditSubCategoryName": "0",
            }
        }
        eventhistory.add_event(event_dict)
        event = EditEvent()
        event.from_dict(event_dict)
        event_list += [event]

    # Check the values at each state
    for idx in range(5):
        assert eventhistory.state(idx) == dict(
            event_list[idx]
        ), f"The state at index {idx} does not match the EditEventHistory."


@pytest.mark.parametrize(
    "init_value, final_value, expected_bool",
    [(1, 1, False), (1, 0, True), (1.6, 2, True)],
)
def test_eventhistory_edit_is_edited(init_value, final_value, expected_bool):
    """ Tests that the is_edited() method correctly updates the EditEventHistory. """
    eventhistory = EditEventHistory()
    eventhistory.add_event(
        {
            "0": {
                "ItemOID": "0.0",
                "ItemValue": init_value,
                "DateTimeStamp": "0",
                "UserOID": "0",
                "AuditSubCategoryName": "0",
            }
        }
    )
    eventhistory.add_event(
        {
            "0": {
                "ItemOID": "0.0",
                "ItemValue": final_value,
                "DateTimeStamp": "0",
                "UserOID": "0",
                "AuditSubCategoryName": "0",
            }
        }
    )

    assert (
        eventhistory.is_edited() == expected_bool
    ), f"The EditEventHistory edit boolean was expected to be {expected_bool}"


# 5. Check types of inputs/outputs


@pytest.mark.parametrize(
    "child_class, parent_class",
    [
        (EditEventHistory, EventHistory),
        (QueryEventHistory, EventHistory),
    ],
)
def test_eventhistory_class_inheritance(child_class, parent_class):
    """ Test that each child event class inherits from the parent class. """
    assert issubclass(
        child_class, parent_class
    ), f"The {child_class} object must inherit from the parent class {parent_class}"


# 6. Check for compatibility across studies and tables


@pytest.mark.parametrize(
    "EventHistoryClass, study_table_name, study_table_dataset",
    [(EditEventHistory, name, data) for name, data in DICT_AUDIT_DATASET.items()]
    + [(QueryEventHistory, name, data) for name, data in DICT_QUERY_DATASET.items()],
)
def test_eventhistory_study_table_compatibility(
    EventHistoryClass, study_table_name, study_table_dataset
):
    """
    Test that no errors are raised when processing
    datasets across multiple studies and tables.

    NOTE: study_table_name is included for debugging test failures,
    but is never used in the test
    """
    row = study_table_dataset.values[0]
    if EventHistoryClass == EditEventHistory:
        event_dict = {0: dict(zip(study_table_dataset.columns, row))}
    elif EventHistoryClass == QueryEventHistory:
        event_dict = dict(zip(study_table_dataset.columns, row))
    else:
        raise ValueError("EventHistoryClass must be either EditEvent or QueryEvent")

    eventhistory = EventHistoryClass()
    eventhistory.add_event(event_dict)
