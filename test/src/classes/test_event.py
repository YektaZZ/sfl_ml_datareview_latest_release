import os
import pytest
import pickle
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from event import Event, EditEvent, QueryEvent

## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))

PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)

DICT_AUDIT_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_audit_df.p"),
        "rb",
    )
)
DICT_QUERY_DATASET = pickle.load(
    open(
        os.path.join(abs_path, "..", "..", "..", "data", "raw", "test_dict_query_df.p"),
        "rb",
    )
)


"""
------------
--event.py--
------------

1. Check for initialization of object
2. Check for required fields
3. Check event states (open, manual)
4. Check types of inputs/outputs
5. Check for compatibility across studies and tables
"""


@pytest.fixture
def editevent_dict():
    return {
        "0": {
            "ItemOID": "0.0",
            "ItemValue": "0",
            "DateTimeStamp": "0",
            "UserOID": "0",
            "AuditSubCategoryName": "0",
        }
    }


@pytest.fixture
def queryevent_dict():
    return {
        "OpenDate": "0",
        "CloseDate": "0",
        "QueryOpenUserOID": "0",
        "QueryRecipient": "0",
        "ItemOID": "0.0",
        "ItemValue": "0",
    }


# 1. Check for initialization of object


@pytest.mark.parametrize(
    "EventClass, attribute, expected_value",
    [
        (Event, "start_time", ""),
        (Event, "user", ""),
        (Event, "subtype", ""),
        (EditEvent, "start_time", ""),
        (EditEvent, "user", ""),
        (EditEvent, "subtype", ""),
        (EditEvent, "values", {}),
        (QueryEvent, "start_time", ""),
        (QueryEvent, "user", ""),
        (QueryEvent, "subtype", ""),
    ],
)
def test_event_empty_init(EventClass, attribute, expected_value):
    """ Test the default values for an empty initialized event class. """
    event = EventClass()
    assert (
        getattr(event, attribute) == expected_value
    ), 'Empty initialized Event does not have the default {attribute} = {expected_value} ""'


# 2. Check for required fields


@pytest.mark.parametrize(
    "missing_field, expected_error",
    [
        ("ItemOID", KeyError),
        ("ItemValue", KeyError),
        ("DateTimeStamp", KeyError),
        ("UserOID", KeyError),
        ("AuditSubCategoryName", KeyError),
    ],
)
def test_event_edit_required_fields(editevent_dict, missing_field, expected_error):
    """ Test that the missing_field raises the proper error in the EditEvent. """
    with pytest.raises(expected_error):
        editevent = EditEvent()
        editevent_dict.pop(missing_field)
        editevent.from_dict(editevent_dict)


@pytest.mark.parametrize(
    "missing_field, expected_error",
    [
        ("OpenDate", KeyError),
        ("QueryOpenUserOID", KeyError),
        ("QueryRecipient", KeyError),
        ("ItemOID", KeyError),
    ],
)
def test_event_query_required_fields(queryevent_dict, missing_field, expected_error):
    """ Test that the missing_field raises the proper error in the QueryEvent. """
    with pytest.raises(expected_error):
        queryevent = QueryEvent()
        queryevent_dict.pop(missing_field)
        queryevent.from_dict(queryevent_dict)


# 3. Check event states (manual)


@pytest.mark.parametrize(
    "query_recipient, expected_bool",
    [
        ("sys_ALPHA", False),
        ("man_OMEGA", True),
    ],
)
def test_event_query_is_manual(queryevent_dict, query_recipient, expected_bool):
    """ Test the various expected states of the is_manual() method. """
    queryevent_dict["QueryRecipient"] = query_recipient
    queryevent = QueryEvent()
    queryevent.from_dict(queryevent_dict)
    assert (
        queryevent.is_manual() == expected_bool
    ), f"For query recipient {query_recipient}, the method is_manual() was expected to be {expected_bool}."


# 4. Check types of inputs/outputs


@pytest.mark.parametrize(
    "child_class, parent_class",
    [
        (EditEvent, Event),
        (QueryEvent, Event),
    ],
)
def test_event_class_inheritance(child_class, parent_class):
    """ Check that each child event class inherits from the parent class. """
    assert issubclass(
        child_class, parent_class
    ), f"The {child_class} object must inherit from the parent class {parent_class}"


@pytest.mark.parametrize(
    "event, event_dict, expected_error",
    [
        (EditEvent, [], AttributeError),
        (QueryEvent, [], TypeError),
    ],
)
def test_event_from_dict_type(event, event_dict, expected_error):
    """ Test the response of from_dict method if it does not receive a dictionary object """
    with pytest.raises(expected_error):
        event_obj = event()
        event_obj.from_dict(event_dict)


# 5. Check for compatibility across studies and tables


@pytest.mark.parametrize(
    "EventClass, study_table_name, study_table_dataset",
    [(EditEvent, name, data) for name, data in DICT_AUDIT_DATASET.items()]
    + [(QueryEvent, name, data) for name, data in DICT_QUERY_DATASET.items()],
)
def test_event_study_table_compatibility(
    EventClass, study_table_name, study_table_dataset
):
    """
    Test that no errors are raised when processing
    datasets across multiple studies and tables.

    NOTE: study_table_name is included for debugging test failures,
    but is never used in the test
    """
    row = study_table_dataset.values[0]
    if EventClass == EditEvent:
        event_dict = {0: dict(zip(study_table_dataset.columns, row))}
    elif EventClass == QueryEvent:
        event_dict = dict(zip(study_table_dataset.columns, row))
    else:
        raise ValueError("EventClass must be either EditEvent or QueryEvent")

    event = EventClass()
    event.from_dict(event_dict)
