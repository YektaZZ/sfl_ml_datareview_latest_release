import os
import pytest
import numpy as np
import pandas as pd
import sys

sys.path.append("src/classes")
from ordinalencoder import (
    OrdinalEncoder,
    NumericOrdinalEncoder,
    CategoricalOrdinalEncoder,
    DateTimeOrdinalEncoder,
)


## DATA PATHS ##

# Tests are located within the `test` folder, followed by the subfolders that
# match the scripts being tested, e.g. `src/classes` for the classes.
abs_path = os.path.dirname(os.path.abspath(__file__))
PATH_AUDIT_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_audit_df.csv"
)
PATH_QUERY_DATASET = os.path.join(
    abs_path, "..", "..", "..", "data", "raw", "test_dataset_query_df.csv"
)


"""
---------------------
--ordinalencoder.py--
---------------------

1. Check initialization state
2. Check for required fields (error handling)
3. Check function return values
4. Check types of inputs/outputs
"""

# 1. Check initialization state


@pytest.mark.parametrize(
    "OrdinalEncoderClass, attribute, expected_value",
    [
        (OrdinalEncoder, "string_to_ordinal", {}),
        (NumericOrdinalEncoder, "string_to_ordinal", {}),
        (CategoricalOrdinalEncoder, "string_to_ordinal", {}),
        (DateTimeOrdinalEncoder, "string_to_ordinal", {}),
    ],
)
def test_ordinalencoder_empty_init(OrdinalEncoderClass, attribute, expected_value):
    """ Test the default values for an empty initialized OrdinalEncoder class. """
    ordinalencoder = OrdinalEncoderClass()
    assert (
        getattr(ordinalencoder, attribute) == expected_value
    ), f"Empty initialized OrdinalEncoderClass does not have the expected {attribute} = {expected_value}"


# 2. Check for required fields (error handling)


@pytest.mark.parametrize(
    "OrdinalEncoderClass, expected_error",
    [
        (NumericOrdinalEncoder, ValueError),
        (CategoricalOrdinalEncoder, ValueError),
        (DateTimeOrdinalEncoder, ValueError),
    ],
)
def test_ordinalencoder_required_string_value_len(OrdinalEncoderClass, expected_error):
    """ The string_value_set must have a minimum length when running on "auto" """
    with pytest.raises(expected_error):
        string_value_set = []
        ordinalencoder = OrdinalEncoderClass()
        ordinalencoder.fit(string_value_set)


# 3. Check function return values


@pytest.mark.parametrize(
    "OrdinalEncoderClass, string_value_set, expected_value",
    [
        (NumericOrdinalEncoder, [25, 2200, 9001], {25: 2, 2200: 2, 9001: 2}),
        (
            NumericOrdinalEncoder,
            [25, 78, 1093, 2200, 5384, 9001],
            {25: 2, 78: 2, 1093: 2, 2200: 2, 5384: 3, 9001: 3},
        ),
        (CategoricalOrdinalEncoder, ["alpha", "omega"], {"alpha": 2, "omega": 3}),
        (
            CategoricalOrdinalEncoder,
            [
                "alpha",
                "epsilon",
                "omega",
                "zeta",
            ],
            {
                "alpha": 2,
                "epsilon": 3,
                "omega": 4,
                "zeta": 5,
            },
        ),
        (
            DateTimeOrdinalEncoder,
            ["2200/01/01", "2200/03/03"],
            {"2200/01/01": 2, "2200/03/03": 2},
        ),
        (
            DateTimeOrdinalEncoder,
            ["2200/01/01", "2200/03/03", "3200/01/01", "4200/01/01"],
            {"2200/01/01": 2, "2200/03/03": 2, "3200/01/01": 2, "4200/01/01": 3},
        ),
    ],
)
def test_ordinalencoder_fit_return(
    OrdinalEncoderClass, string_value_set, expected_value
):
    """ Test the fit method returns the expected encoding. """
    ordinalencoder = OrdinalEncoderClass()
    ordinalencoder.fit(string_value_set)
    assert (
        ordinalencoder.string_to_ordinal == expected_value
    ), f"The fit encoding {ordinalencoder.string_to_ordinal} does not match the expected value: {expected_value}"


# 4. Check types of inputs/outputs


@pytest.mark.parametrize(
    "child_class, parent_class",
    [
        (NumericOrdinalEncoder, OrdinalEncoder),
        (CategoricalOrdinalEncoder, OrdinalEncoder),
        (DateTimeOrdinalEncoder, OrdinalEncoder),
    ],
)
def test_ordinalencoder_class_inheritance(child_class, parent_class):
    """ Test that each child OrdinalEncoder class inherits from the parent class. """
    assert issubclass(
        child_class, parent_class
    ), f"The {child_class} object must inherit from the parent class {parent_class}"


@pytest.mark.parametrize(
    "OrdinalEncoderClass, input_value, expected_error",
    [
        (NumericOrdinalEncoder, 0, TypeError),
        (CategoricalOrdinalEncoder, "alpha", TypeError),
        (DateTimeOrdinalEncoder, "2200/01/01", TypeError),
    ],
)
def test_ordinalencoder_fit_input(OrdinalEncoderClass, input_value, expected_error):
    """ Test that the fit function only accepts sets/lists. """
    with pytest.raises(expected_error):
        ordinalencoder = OrdinalEncoderClass()
        ordinalencoder.fit(input_value)
