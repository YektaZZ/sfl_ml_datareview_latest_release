import os
import sys

sys.path.append("src/classes")


# PATHS
abs_path = os.path.dirname(os.path.abspath(__file__))

RECORD_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_records.pkl"
)
ENCODER_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_encoder.pkl"
)
AUDIT_CSV_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_audit.csv"
)
QUERY_CSV_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_query.csv"
)
METADATA_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_metadata.csv"
)


def istype(ObjectToCheck, ObjectType):
    return isinstance(ObjectToCheck, ObjectType)


# Load Record Collection:


def test_load_recordcollection():
    from recordcollection import RecordCollection

    path_to_saved_record = RECORD_PATH
    record_collection = RecordCollection(path=path_to_saved_record)
    assert istype(record_collection, RecordCollection)


# Load Encoder Collection:
### CODE ###
def test_load_encoder():
    from ordinalencodercollection import OrdinalEncoderCollection

    path_to_saved_encoder = ENCODER_PATH
    encoder_collection = OrdinalEncoderCollection(path=path_to_saved_encoder)

    assert istype(encoder_collection, OrdinalEncoderCollection)


# Make Record Collection
def test_recordcollection_manifest():
    from recordcollection import RecordCollection
    from dataset import AuditDataset, QueryDataset

    path_to_audit_csv = AUDIT_CSV_PATH
    path_to_query_csv = QUERY_CSV_PATH
    path_to_reference_record = RECORD_PATH

    audit = AuditDataset(path=path_to_audit_csv)
    query = QueryDataset(path=path_to_query_csv)

    record_collection = RecordCollection()
    record_collection.make_manifest(audit)
    record_collection.initialize_records()
    record_collection.make_records(audit, query)

    ref_record_collection = RecordCollection(path=path_to_reference_record)

    assert (record_collection.manifest == ref_record_collection.manifest).any().any()


# Make Encoder Collection
def test_encoder_collection():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection

    path_to_metadata = METADATA_PATH
    path_to_reference_record = RECORD_PATH

    record_collection = RecordCollection(path=path_to_reference_record)
    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)
    encoder_collection = OrdinalEncoderCollection(
        items_to_dtype=sorted_itemoids_to_dtype
    )
    encoder_collection.fit(itemoid_values)


# Subject Snapshot
def test_snapshot():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from datagenerator import DataGenerator

    path_to_metadata = METADATA_PATH
    path_to_reference_record = RECORD_PATH
    path_to_reference_encoder = ENCODER_PATH
    record_collection = RecordCollection(path=path_to_reference_record)
    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)
    encoder_collection = OrdinalEncoderCollection(path=path_to_reference_record)
    fit_generator = DataGenerator(
        record_collection, encoder_collection, sorted_itemoids
    )
