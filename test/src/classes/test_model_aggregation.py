import os
import sys

sys.path.append("src/classes")


# PATHS
abs_path = os.path.dirname(os.path.abspath(__file__))

RECORD_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_records.pkl"
)
ENCODER_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_encoder.pkl"
)
AUDIT_CSV_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_audit.csv"
)
QUERY_CSV_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_query.csv"
)
METADATA_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_metadata.csv"
)
MODEL_PATH = os.path.join(abs_path, "..", "..", "..", "data", "test", "test_model.h5")

date = None

# Aggregation Level


def test_aggregation_level():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from model import SubjectModel
    from datagenerator import DataGenerator

    path_to_record_collection = RECORD_PATH
    path_to_metadata = METADATA_PATH
    path_to_model = MODEL_PATH
    path_to_encoder = ENCODER_PATH

    record_collection = RecordCollection(path=path_to_record_collection)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)

    encoder_collection = OrdinalEncoderCollection(path=path_to_encoder)
    fit_generator = DataGenerator(
        record_collection,
        encoder_collection,
        sorted_itemoids,
        augmentation=0,
        subject_split=0,
        split_date=date,
    )
    source_snapshots, _, record_snapshots = fit_generator.get_nontraining_data()

    model = SubjectModel(path=path_to_model)
    pred_target_snapshots = model.infer(source_snapshots)
    record_scores = model.assess_batch(pred_target_snapshots, record_snapshots)

    assert isinstance(record_scores, dict)


#  Output Range


def test_output_range():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from model import SubjectModel
    from datagenerator import DataGenerator

    path_to_record_collection = RECORD_PATH
    path_to_metadata = METADATA_PATH
    path_to_model = MODEL_PATH
    path_to_encoder = ENCODER_PATH

    record_collection = RecordCollection(path=path_to_record_collection)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)

    encoder_collection = OrdinalEncoderCollection(path=path_to_encoder)
    fit_generator = DataGenerator(
        record_collection,
        encoder_collection,
        sorted_itemoids,
        augmentation=0,
        subject_split=0,
        split_date=date,
    )
    source_snapshots, _, record_snapshots = fit_generator.get_nontraining_data()

    model = SubjectModel(path=path_to_model)

    pred_target_snapshots = model.infer(source_snapshots)
    record_scores = model.assess_batch(pred_target_snapshots, record_snapshots)

    assert (max(record_scores.values()) - 1) ** 2 + (
        min(record_scores.values()) - 0
    ) ** 2 < 1


# Double Query Check


def test_double_query():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from model import SubjectModel
    from datagenerator import DataGenerator

    path_to_record_collection = RECORD_PATH
    path_to_metadata = METADATA_PATH
    path_to_model = MODEL_PATH
    path_to_encoder = ENCODER_PATH

    record_collection = RecordCollection(path=path_to_record_collection)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)

    encoder_collection = OrdinalEncoderCollection(path=path_to_encoder)
    fit_generator = DataGenerator(
        record_collection,
        encoder_collection,
        sorted_itemoids,
        augmentation=0,
        subject_split=0,
        split_date=date,
    )
    source_snapshots, _, record_snapshots = fit_generator.get_nontraining_data()

    model = SubjectModel(path=path_to_model)

    pred_target_snapshots = model.infer(source_snapshots)
    record_scores = model.assess_batch(pred_target_snapshots, record_snapshots)
    filtered_record_scores = record_collection.filter_record_scores(record_scores)
    twice_filtered_record_scores = record_collection.filter_record_scores(
        filtered_record_scores
    )

    assert set(filtered_record_scores.keys()) == set(
        twice_filtered_record_scores.keys()
    )
