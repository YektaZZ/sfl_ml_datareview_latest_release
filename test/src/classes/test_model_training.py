import os
import sys

sys.path.append("src/classes")


# PATHS
abs_path = os.path.dirname(os.path.abspath(__file__))

RECORD_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_records.pkl"
)
ENCODER_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_encoder.pkl"
)
AUDIT_CSV_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_audit.csv"
)
QUERY_CSV_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_query.csv"
)
METADATA_PATH = os.path.join(
    abs_path, "..", "..", "..", "data", "test", "test_metadata.csv"
)

# Empty Initialization:


def test_empty_initialization():
    from model import SubjectModel
    from tensorflow.keras import Model

    expected_values = {"trained": False, "histories": [], "parameters": {}}

    subject_model = SubjectModel()

    for attr, value in expected_values.items():
        assert getattr(subject_model, attr) == value


# Run Subject Model


def run_subject_model():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from datagenerator import DataGenerator
    from model import SubjectModel

    record_collection = RecordCollection(path=RECORD_PATH)
    encoder_collection = OrdinalEncoderCollection(path=ENCODER_PATH)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(METADATA_PATH)

    fit_generator = DataGenerator(
        record_collection, encoder_collection, sorted_itemoids
    )
    feature_dict = fit_generator.count_feature_values()

    model = SubjectModel()
    model.build_model(feature_dict)

    model.train(fit_generator, epochs=1)

    assert model.trained


# Zero Knowledge Model


def test_zero_knowledge_model():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from model import ZeroKnowledgeModel
    from datagenerator import DataGenerator
    import numpy as np

    record_collection = RecordCollection(path=RECORD_PATH)
    encoder_collection = OrdinalEncoderCollection(path=ENCODER_PATH)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(METADATA_PATH)

    fit_generator = DataGenerator(
        record_collection, encoder_collection, sorted_itemoids
    )

    model = ZeroKnowledgeModel(num_itemoids=len(sorted_itemoids))
    model.fit(fit_generator)

    test_mat = np.zeros((model.num_itemoids, model.window_size))

    for idx in range(1000):
        test_mat += model.infer()

    test_mat = test_mat / 1000
    assert np.linalg.norm(test_mat.mean(axis=0) - model.rates.mean(axis=0)) < 0.01


# Subject Leakage


def test_subject_leakage():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from datagenerator import DataGenerator

    record_collection = RecordCollection(path=RECORD_PATH)
    encoder_collection = OrdinalEncoderCollection(path=ENCODER_PATH)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(METADATA_PATH)

    fit_generator = DataGenerator(
        record_collection, encoder_collection, sorted_itemoids
    )
    assert (
        len(
            fit_generator.subjects["train"].intersection(
                fit_generator.subjects["validation"]
            )
        )
        == 0
    )


# Snapshot Leakage


def test_snapshot_leakage():
    from recordcollection import RecordCollection
    from ordinalencodercollection import OrdinalEncoderCollection
    from datagenerator import DataGenerator

    record_collection = RecordCollection(path=RECORD_PATH)
    encoder_collection = OrdinalEncoderCollection(path=ENCODER_PATH)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(METADATA_PATH)

    fit_generator = DataGenerator(
        record_collection, encoder_collection, sorted_itemoids
    )
    assert (
        len(
            fit_generator.partition["train"].intersection(
                fit_generator.partition["validation"]
            )
        )
        == 0
    )
