##########################################################################
#   Copyright by SFL Scientific
#   Description: Domain-Centric modeling pipeline and helper functions
#                for data review workstream
##########################################################################
import os
import sys
import time
import re
import glob
import logging
from importlib import reload
import numpy as np
import pandas as pd
import json
import random
from tqdm import tqdm
import warnings
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier
import category_encoders
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_curve, precision_score, recall_score
import shap

# local libraries
ROOT_DIR = os.path.abspath("../")
sys.path.append(ROOT_DIR)

# settings
warnings.filterwarnings("ignore")
reload(logging)
logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)
##########################################################################
#   Preprocess
##########################################################################


def GT_preprocess(df_GT):
    """
    preprocess and return ground truth dataframe for a given Clintek
    +df_GT: ground truth dataframe, consists of the following columns:
            -ID: (int) Record ID
            -Label: (str) the query/edit status
            -Initial: (dict) dictionary of initial query values
            -Final: (dict) dictionary of final query values
            -Target: (bool) ground truth about query
    """
    # fill NAN Initial/Final values with empty dictionary
    df_GT["Initial"] = df_GT["Initial"].apply(lambda x: {} if pd.isnull(x) else x)
    df_GT["Final"] = df_GT["Final"].apply(lambda x: {} if pd.isnull(x) else x)

    # extract FormOID
    df_GT["FormOID_Initial"] = df_GT["Initial"].apply(lambda x: get_FormOID(x))
    df_GT["FormOID_Final"] = df_GT["Final"].apply(lambda x: get_FormOID(x))

    # remove ground truth without any FormOID
    df_GT = df_GT[
        (df_GT["FormOID_Initial"] != "NULL") | (df_GT["FormOID_Final"] != "NULL")
    ]

    return df_GT


def get_FormOID(dict_initial_final_values):
    """
    extract FormOID (str) from dictionaries of initial/final values
    +dict_initial_final_values: dict of initial/final values
                         in the format {"AE.XXX": "value1", "LB.YYY": "value2"}
    """
    if dict_initial_final_values:
        # extract FormOID from first key of dict
        formoid = list(dict_initial_final_values)[0]
        formoid = formoid.split(".")[0]
        return formoid
    else:
        # return NULL for empty dict
        return "NULL"


def prepare_domain_table(df_all, formoid):
    """
    extract and return the model input dataframe for a given domain FormOID
    +df_all: preprocessed ground truth dataframe for all domain tables, with columns
            -ID: (int) Record ID
            -FormOID: (str) FormOID
            -ItemValues: (dict) dictionary of query values for model input
            -Target: (bool) ground truth about query
    +formoid: (str) FormOID of a given domain (e.g. AE)
    """
    # filter the given domain
    df_domain = df_all[df_all["FormOID"] == formoid].reset_index(drop=True)

    # create bare domain table with target for modeling
    df_itemvalues = pd.json_normalize(df_domain["ItemValues"])
    df_domain = pd.concat([df_domain[["ID", "Target"]], df_itemvalues], axis=1)

    return df_domain


##########################################################################
#   Feature Engineering
##########################################################################
def generate_length_feat(df, col):
    """
    generate feature using the length of string object
    +df: dataframe of train/valid/test sets
    +col: domain table column name
    """
    df[col + "_len"] = df[col].str.len()

    return df


def generate_null_feat(df, col):
    """
    generate feature to check if the string is NULL
    +df: dataframe of train/valid/test sets
    +col: domain table column name
    """
    df[col + "_null"] = df[col] == "NULL"

    return df


##########################################################################
#   ML Pipeline
##########################################################################
class DomainModeling:
    """
    base domain-centric modeling pipeline class using XGBoost model
    """

    def __init__(
        self,
        df=None,
        formoid="",
        valid_size=0.15,
        test_size=0.15,
        log_flag=True,
    ):
        """
        +df: bare domain table with ground truth column "Target"
        +formoid: FormOID, i.e. domain table name
        +valid_size: valid dataset fraction
        +test_size: test dataset fraction
        +log_flag: flag for logger
        """
        self.df = df
        self.formoid = formoid
        self.valid_size = valid_size
        self.test_size = test_size
        self.log_flag = log_flag

        # initialization
        self.df_train = pd.DataFrame()
        self.df_valid = pd.DataFrame()
        self.df_test = pd.DataFrame()
        self.list_feats = []
        self.feat_encoder = None
        self.model = None

    def run(self):
        """
        domain-centric model pipeline execution including data splitting,
        feature engineering, and model training
        """
        # data split
        if self.log_flag:
            logger.info("Performing data split ...")
        self.data_split()

        # feature engineering
        if self.log_flag:
            logger.info("Generating and encoding features ...")
        self.clean_feats()
        self.generate_feats()
        self.clean_feats()
        self.encode_feats()
        self.get_feats()

        # model training
        if self.log_flag:
            logger.info("Training model ...")
        self.model_train()

        if self.log_flag:
            logger.info("Run completed.")

    def data_split(self):
        """
        split dataframe into train/valid/test sets
        """
        # random split
        self.df_train, self.df_test = train_test_split(
            self.df,
            test_size=self.test_size,
            shuffle=True,
            stratify=self.df["Target"],
        )
        self.df_train, self.df_valid = train_test_split(
            self.df_train,
            test_size=self.valid_size / (1 - self.test_size),
            shuffle=True,
            stratify=self.df_train["Target"],
        )

    def generate_feats(self):
        """
        domain-independent feature engineering for train/valid/test data sets
        """
        # collect domain-specific columns
        list_domain_cols = [
            col for col in self.df.columns if self.df[col].dtypes == object
        ]

        # generate feats for each domain cols in train/valid/test sets
        for df in [self.df_train, self.df_valid, self.df_test]:
            for col in list_domain_cols:
                df = generate_length_feat(df=df, col=col)
                df = generate_null_feat(df=df, col=col)

    def clean_feats(self):
        """
        fill NAN values for train/valid/test data sets
        """
        for df in [self.df_train, self.df_valid, self.df_test]:
            for col in df.columns:
                if df[col].dtype in [int, float, bool]:
                    df[col] = df[col].fillna(0)
                else:
                    df[col] = df[col].fillna("NULL")

    def encode_feats(self):
        """
        encode categorical features with ordinal encoding
        """
        # get all categorical columns
        list_cat_feats = [
            col for col in self.df.columns if self.df[col].dtypes == object
        ]

        # remove columns with too many categories
        list_drop_cat_feats = []
        for col in list_cat_feats:
            if self.df_train[col].nunique() > self.df_train.shape[0] * 0.2:
                list_drop_cat_feats.append(col)
        self.df_train = self.df_train.drop(columns=list_drop_cat_feats)
        list_cat_feats = [
            col for col in list_cat_feats if col not in list_drop_cat_feats
        ]

        # encoding
        self.feat_encoder = category_encoders.OrdinalEncoder(
            cols=list_cat_feats,
            return_df=True,
            handle_missing="value",
            handle_unknown="value",
        )
        self.feat_encoder.fit(self.df_train[list_cat_feats])

        self.df_train[list_cat_feats] = self.feat_encoder.transform(
            self.df_train[list_cat_feats]
        )
        self.df_valid[list_cat_feats] = self.feat_encoder.transform(
            self.df_valid[list_cat_feats]
        )
        self.df_test[list_cat_feats] = self.feat_encoder.transform(
            self.df_test[list_cat_feats]
        )

    def get_feats(self):
        """
        get model feature list
        """
        list_drop_feats = [
            "ID",
            "Target",
        ]
        self.list_feats = [
            feat
            for feat in self.df_train.columns.tolist()
            if (feat not in list_drop_feats)
        ]

    def model_train(self):
        """
        model fitting and validation
        """
        # prepare model data input
        x_train = self.df_train[self.list_feats]
        y_train = self.df_train["Target"]
        x_valid = self.df_valid[self.list_feats]
        y_valid = self.df_valid["Target"]

        # train XGB classifier
        self.model = XGBClassifier(
            n_estimators=1000,
            max_depth=6,
            learning_rate=0.1,
            verbosity=0,
            objective="binary:logistic",
            n_jobs=-1,
            gamma=0,
            min_child_weight=1,
            max_delta_step=1,
            subsample=0.7,
            colsample_bytree=1,
            reg_alpha=1,
            reg_lambda=1,
            random_state=0,
            base_score=0.7,
        )
        self.model = self.model.fit(
            x_train,
            y_train,
            eval_set=[(x_train, y_train), (x_valid, y_valid)],
            eval_metric="logloss",
            early_stopping_rounds=50,
            verbose=False,
        )

    def model_evaluate(self):
        """
        evaluate model using test set

        return a dictionary of evaluation metrics including
        accuracy, precision, recall and F1
        """
        y_pred = self.model.predict(self.df_test[self.list_feats])
        y_true = self.df_test["Target"]
        dict_report = classification_report(y_true, y_pred, output_dict=True)

        return dict_report

    def generate_pr_values(self):
        """
        return arrays of precision, recall and model prediction probability
        thresholds
        """
        # get mapping index when model predicts True class
        true_ind = 0
        for ind, cls in enumerate(self.model.classes_):
            if cls:
                true_ind = ind
                break

        # calculate precision/recall pairs
        y_scores = self.model.predict_proba(self.df_test[self.list_feats])[:, true_ind]
        y_true = self.df_test["Target"]
        precision, recall, thresholds = precision_recall_curve(y_true, y_scores)

        return precision, recall, thresholds

    def generate_feat_importance(self):
        """
        return dataframe of model feature importance columns of feature names
        and importances calculated from SHAP values
        """
        # compute SHAP values
        explainer = shap.TreeExplainer(self.model)
        shap_values = explainer.shap_values(self.df_train[self.list_feats])

        # set up dataframe sorted by feature importance
        df_importance = pd.DataFrame()
        df_importance["feature"] = self.model.get_booster().feature_names
        df_importance["importance"] = np.abs(shap_values).mean(axis=0)
        df_importance = df_importance.sort_values(by="importance", ascending=False)

        return df_importance


##########################################################################
#   Utils
##########################################################################
def ZKM_performance(df):
    """
    estimate zero knowledge model performance
    +df: bare domain table with ground truth column "Target"

    return a dictionary of evaluation metrics
    """
    data_size = df.shape[0]
    true_frac = df["Target"].sum() / data_size

    # a zero knowledge model prediction based on overall fraction of true
    # target
    y_true = df["Target"]
    y_baseline = np.random.choice(
        [False, True], size=data_size, p=[1 - true_frac, true_frac]
    )
    dict_report = classification_report(y_true, y_baseline, output_dict=True)

    return dict_report


def calculate_performance_statistics(df_results):
    """
    return a dataframe of statistics of model performances with the columns of
    FormOID, accuracy, precision, recall, F1, and support

    +df_results: dataframe of model performances from multiple model runs,
                 with the columns of FormOID, accuracy, precision, recall, F1,
                 and support
    """
    # performance average grouped by domain tables
    df_mean = (
        df_results.groupby("FormOID", sort=False)[
            [
                "accuracy",
                "precision",
                "recall",
                "f1-score",
                "support",
            ]
        ]
        .mean()
        .reset_index()
    )
    df_mean = df_mean.round(decimals=3)
    df_mean["support"] = df_mean["support"].astype(int)
    df_mean = df_mean.astype(str)

    # performance standard deviation
    df_std = (
        df_results.groupby("FormOID", sort=False)[
            [
                "accuracy",
                "precision",
                "recall",
                "f1-score",
                "support",
            ]
        ]
        .std()
        .reset_index()
    )
    df_std = df_std.round(decimals=3)
    df_std["support"] = df_std["support"].astype(int)
    df_std = df_std.astype(str)

    # collect performance statistics
    df_results_stat = pd.DataFrame()
    df_results_stat["FormOID"] = df_mean["FormOID"]
    for col in ["accuracy", "precision", "recall", "f1-score", "support"]:
        df_results_stat[col] = df_mean[col] + r" (+/-" + df_std[col] + ")"

    return df_results_stat
