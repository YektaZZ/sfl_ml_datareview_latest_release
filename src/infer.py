import sys

sys.path.append("../src/classes")
from datageneratorfactory import DataGeneratorFactory
from model import SubjectModel
from ordinalencodercollection import OrdinalEncoderCollection
from recordcollection import RecordCollection



def identify_queries(
    path_to_record_collection,
    path_to_metadata,
    path_to_model,
    path_to_encoder,
    date=None,
):
    """
    Load a preprocessed record colection, encoder collection, and subject model to score the data risk of records.

    Args:
        path_to_record_collection: (str) path to a preprocessed record collection
        path_to_metadata: (str) path to the appropriate metadata
        path_to_encoder_collection: (str) path to a preprocessed encoder collection
        path_to_model: (str) path to a trained model
    Returns:
        a dictionary whose keys are record IDs and whose values are data risk scores (0 is no risk, 1 is maximal risk)
    """
    record_collection = RecordCollection(path=path_to_record_collection)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)

    encoder_collection = OrdinalEncoderCollection(path=path_to_encoder)

    generator_factory = DataGeneratorFactory(
        record_collection,
        encoder_collection,
        sorted_itemoids,
        augmentation=0,
        subject_split=0,
        split_date=date,
    )
    _, infer_generator = generator_factory.make_data_generators()

    model = SubjectModel(path=path_to_model)

    pred_target_snapshots = model.infer(infer_generator)
    record_scores = model.assess_batch(infer_generator)
    filtered_record_scores = record_collection.filter_record_scores(record_scores)

    return filtered_record_scores
