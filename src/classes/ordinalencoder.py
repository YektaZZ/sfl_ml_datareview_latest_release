import numpy as np
from dateutil.parser import parse, ParserError
from collections import Counter
from nltk.corpus import stopwords
import string


class OrdinalEncoder:
    """
    An object for mapping string, float, or datetime data to integer valued representations.

    Class Args:
        string_to_ordinal: (dict) a dictionary of data, integer pairs
    """

    def __init__(self):
        """
        Initialize a new object.

        Args:
            None
        Returns:
            None
        """

        self.string_to_ordinal = dict()

    def fit(self, string_value_set):
        """
        An abstract method placeholder for a fit statement.

        Args:
            None
        Returns:
            None
        """
        pass

    def transform(self, value):
        """
        Use the string_to_ordinal dictionary to encode a string value.

        Args:
            value: (str) the raw value to be encoded, stored as a string regardless of underlying datatype
        Returns
            an integer representation of the raw value
        """
        if value in self.string_to_ordinal:
            return self.string_to_ordinal[value]
        else:
            self.transform_outlier(value)

    def transform_outlier(self, value):
        """
        An abstract method for transforming an out of range value, overwritten by child classes.

        Args:
            value: (str) the raw value to be encoded, stored as a string regardless of underlying datatype
        Returns:
            None
        """
        pass

    @staticmethod
    def to_ordinal(string_to_object, num_bins="auto"):
        """
        A helper method for binning float and datetime objects

        Args:
            string_to_object: (dict) a dictionary of string, value pairs
        Kwargs:
            num_bins: (str, int) the number of bins into which to place data, passing "auto" will divide the data into int(sqrt(num_samples)) bins
        Returns:
            a dictionary of string, bin number pairs
        """
        string_to_ordinal = dict()
        bin_cutoffs = dict()
        num_samples = len(string_to_object)

        if num_bins == "auto":
            num_bins = np.sqrt(num_samples)
            num_bins = max(1, int(num_bins))

        num_samples_per_bin = num_samples // num_bins

        string_to_object = string_to_object.items()
        sorted_string_to_object = sorted(string_to_object, key=lambda x: x[1])

        counter = 0
        bin_number = 2
        bin_values = []

        for string_value, object_value in sorted_string_to_object:
            string_to_ordinal[string_value] = bin_number
            bin_values.append(object_value)

            if counter == num_samples_per_bin:
                bin_cutoffs[bin_number] = min(bin_values)
                bin_number += 1
                bin_values = []
                counter = 0
            else:
                counter += 1

        return string_to_ordinal, bin_cutoffs


class NumericOrdinalEncoder(OrdinalEncoder):
    def __init__(self):
        """
        Initialize a new object.

        Args:
            None
        Returns:
            None
        """
        super().__init__()
        self.bin_cutoffs = dict()

    def fit(self, string_value_set, num_bins="auto"):
        """
        Fit the encoder to given item values.

        Args:
            string_value_set: (set) a set containing all values which occur in the target ItemOID
        Kwargs:
            num_bins: (str, int) the number of bins into which to place data, passing "auto" will divide the data into int(sqrt(num_samples)) bins
        Returns:
            None
        """
        if not isinstance(string_value_set, set) and not isinstance(
            string_value_set, list
        ):
            raise TypeError("string_value_set must be a list or a set")
        if len(string_value_set) == 0:
            raise ValueError("string_value_set is emptry")

        string_to_float = dict()

        for string_value in string_value_set:
            try:
                float_value = float(string_value)
                string_to_float[string_value] = float_value
            except ValueError:
                self.string_to_ordinal[string_value] = 0

        string_to_ordinal, bin_cutoffs = self.to_ordinal(
            string_to_float, num_bins=num_bins
        )
        self.string_to_ordinal.update(string_to_ordinal)
        self.bin_cutoffs = bin_cutoffs

    def transform_outlier(self, value):
        """
        Send all previously unseen values to a withheld token.

        Args:
            value: (str) the raw value to be encoded, stored as a string regardless of underlying datatype
        Returns:
            an integer
        """
        try:
            float_value = float(value)
        except BaseException:
            return 1

        for bin_num in range(2, len(self.bin_cutoffs) + 2):
            if float_value < self.bin_cutoffs[bin_num]:
                return bin_num - 1

            return len(self.bin_cutoffs) + 2


class CategoricalOrdinalEncoder(OrdinalEncoder):
    def __init__(self):
        """
        Initialize a new object.

        Args:
            None
        Returns:
            None
        """
        super().__init__()

    def fit(self, string_value_set):
        """
        Fit the encoder to given item values.

        Args:
            string_value_set: (set) a set containing all values which occur in the target ItemOID
        Returns:
            None
        """

        if not isinstance(string_value_set, set) and not isinstance(
            string_value_set, list
        ):
            raise TypeError("string_value_set must be a list or a set")
        if len(string_value_set) == 0:
            raise ValueError("string_value_set is emptry")

        ordinal_count = 2
        clean_string_to_ordinal = dict()
        for string_value in string_value_set:
            clean_string_value = string_value.translate(
                str.maketrans("", "", string.punctuation)
            )
            clean_string_value = clean_string_value.lower()
            clean_string_value = " ".join(
                [
                    word
                    for word in clean_string_value.split(" ")
                    if word not in stopwords.words("english")
                ]
            )
            clean_string_value = clean_string_value.strip()
            clean_string_value = clean_string_value.replace("  ", " ")

            if clean_string_value in clean_string_to_ordinal:
                ordinal_value = clean_string_to_ordinal[clean_string_value]
                self.string_to_ordinal[string_value] = ordinal_value
            else:
                clean_string_to_ordinal[clean_string_value] = ordinal_count
                self.string_to_ordinal[string_value] = ordinal_count
                ordinal_count += 1

    def transform_outlier(self, value):
        """
        Send all previously unseen values to a withheld token.

        Args:
            value: (str) the raw value to be encoded, stored as a string regardless of underlying datatype
        Returns:
            an integer
        """
        self.string_to_ordinal[value] = 1
        return 1


class DateTimeOrdinalEncoder(OrdinalEncoder):
    def __init__(self, date_fomat="auto"):
        """
        Initialize a new object.

        Args:
            None
        Returns:
            None
        """
        super().__init__()
        self.bin_cutoffs = dict()

    def fit(self, string_value_set, num_bins="auto", date_format="auto"):
        """
        Fit the encoder to given item values.

        Args:
            string_value_set: (set) a set containing all values which occur in the target ItemOID
        Kwargs:
            num_bins: (str, int) the number of bins into which to place data, passing "auto" will divide the data into int(sqrt(num_samples)) bins
            date_format: (str, int) the number of spaces in datetime strings, passing "auto" will determine the dominant date format
        Returns:
            None
        """

        if not isinstance(string_value_set, set) and not isinstance(
            string_value_set, list
        ):
            raise TypeError("string_value_set must be a list or a set")
        if len(string_value_set) == 0:
            raise ValueError("string_value_set is emptry")

        if date_format == "auto":
            date_component_distribution = [
                string_value.count(" ") + 1 for string_value in string_value_set
            ]
            date_component_counter = Counter(date_component_distribution)
            date_format = date_component_counter.most_common()[0][0]
        string_to_datetime = dict()

        for string_value in string_value_set:
            num_date_components = string_value.count(" ") + 1
            if num_date_components != date_format:
                self.string_to_ordinal[string_value] = 1
            else:
                try:
                    datetime_value = parse(string_value)
                    string_to_datetime[string_value] = datetime_value
                except:
                    self.string_to_ordinal[string_value] = 1
        string_to_ordinal, bin_cutoffs = self.to_ordinal(
            string_to_datetime, num_bins=num_bins
        )
        self.string_to_ordinal.update(string_to_ordinal)
        self.bin_cutoffs = bin_cutoffs

    def transform_outlier(self, value):
        """
        Send all previously unseen values to a withheld token.

        Args:
            value: (str) the raw value to be encoded, stored as a string regardless of underlying datatype
        Returns:
            an integer
        """
        try:
            datetime_value = parse(value)
        except BaseException:
            return 1

        for bin_num in range(2, len(self.bin_cutoffs) + 2):
            if datetime_value < self.bin_cutoffs[bin_num]:
                return bin_num - 1

            return len(self.bin_cutoffs) + 2
