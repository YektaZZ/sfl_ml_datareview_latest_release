from collections import OrderedDict
import numpy as np


class DataWindow:
    """
    Holds changing data in an aligned format for subject snapshot data generation.

    Class Vars:
        stacks: (OrderedDict) a dictionary of stack objects for reserving data
            keys: (str) item name
            values: (list) the most recent data entries
    """

    def __init__(self, encoder_collection, window_size=5, fill_value=0, source=True):
        """
        Initializes the object with lists of fill values.

        Args:
            encoder_collection: (OrdinalEncoderCollection) encoders for transforming string data into encoded ints
        Kwargs:
            window_size: (int) the number of previous values to lag
            fill_value: (str, int) the initial value of stacks
        Returns:
            None
        """
        self.stacks = OrderedDict()
        self.source = source
        self.encoder_collection = encoder_collection

        for item in encoder_collection.encoders:
            self.stacks[item] = [fill_value] * window_size

    def add_new_value(self, item, value):
        """
        Add a new value and remove the oldest value from a designated stack

        Args:
            item: (str) the ItemOID to update
            value: (str) the value to insert onto the stack
        Returns:
            a Boolean, True if a data change was made
        """
        if item in self.stacks:
            if self.source:
                encoded_value = self.encoder_collection.transform(item, value)
            else:
                encoded_value = value

            self.stacks[item].insert(0, encoded_value)
            self.stacks[item].pop()
            return True
        return False

    def get_snapshot(self):
        """
            Generate a numpy array of the current stacks.

            Args:
                None
        Returns:
            stack_snapshot: (numpy array) a 2-tensor of source data
                first index: ItemOID index
                second index: number of data lags
        """
        stack_lists = [stack for stack in self.stacks.values()]
        stack_snapshot = np.asarray(stack_lists)
        return stack_snapshot
