import tensorflow.keras.layers as layers

from tensorflow import split as tf_split
import tensorflow.keras.backend as K
from tensorflow.keras import Model
from tensorflow.keras.metrics import Recall, Precision
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from collections import defaultdict
import numpy as np
import dill

import sys
sys.path.append("../src/classes")

from datagenerator import DataGenerator

def f1_metric(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    recall = true_positives / (possible_positives + K.epsilon())
    f1_val = 2*(precision*recall)/(precision+recall+K.epsilon())
    return f1_val
    

class SubjectModel:
    """
    A class wrapper for the TensorFlow-based subject model
    """

    def __init__(self, path=""):
        """
        Initialize the model object.

        Args:
            None
        Kwargs:
            path: (str) if a path is specifed a saved model is loaded
        Returns:
            None
        """
        self.model = Model()
        self.trained = False
        self.histories = []
        self.parameters = {}

        if path:
            self.load_from_path(path)

    def build_model(
        self, feature_dict, window_size=20, embed_dim=5, kernel_size=3, base_filters=8
    ):
        """
        Build and compile the model.

        Args:
            feature_dict: (dict) a dictionary whose keys are ItemOID names and whose values are the number of categorical features,
                this is used to construct the embedding layers
        Kwargs:
            window_size: (int) the number of past values to save
            embed_dim: (int) the dimension of the embedding
            kernel_size: (int) the size of the convolutional kernel
            base_filters: (int) the number of convolutional filters in the first layer of the Unet
        Returns:
            None
        """

        self.parameters.update(
            {
                "window_size": window_size,
                "embed_dim": embed_dim,
                "kernel_size": kernel_size,
                "base_filters": base_filters,
                "idx_to_name": {idx: name for idx, name in enumerate(feature_dict.keys())}
            }
        )

        input_layer = layers.Input(shape=(len(feature_dict), window_size, 1))
        split = layers.Lambda(
            lambda x: tf_split(x, num_or_size_splits=len(feature_dict), axis=1)
        )(input_layer)

        embedding_layers = [
            layers.Embedding(
                input_dim=num_features,
                output_dim=embed_dim,
                input_length=window_size,
                name=f"embedding_{name}",
            )(split[idx])
            for idx, (name, num_features) in enumerate(feature_dict.items())
        ]

        concat_layer = layers.concatenate([layer for layer in embedding_layers])
        reshape_layer = layers.Reshape((window_size, embed_dim, len(feature_dict)))(
            concat_layer
        )
        permute_layer = layers.Permute((3, 1, 2))(reshape_layer)

        num_filters = base_filters
        conv0 = layers.Conv2D(num_filters, kernel_size, padding="same")(permute_layer)
        norm0 = layers.BatchNormalization()(conv0)
        actv0 = layers.Activation("relu")(norm0)

        # First backbone block
        conv1 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(actv0)
        norm1 = layers.BatchNormalization()(conv1)

        actv1 = layers.Activation("relu")(norm1)
        conv1 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(actv1)
        norm1 = layers.BatchNormalization()(conv1)

        pool1 = layers.MaxPooling2D(pool_size=(2, 1))(norm1)

        # Add the first backbone residual
        resd1 = layers.Conv2D(num_filters, 1, padding="same", strides=(2, 1))(actv0)
        plus1 = layers.add([pool1, resd1])

        # Second backbone block
        num_filters *= 2
        conv2 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(plus1)
        norm2 = layers.BatchNormalization()(conv2)

        actv2 = layers.Activation("relu")(norm2)
        conv2 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(actv2)
        norm2 = layers.BatchNormalization()(conv2)

        pool2 = layers.MaxPooling2D(pool_size=(2, 1))(norm2)

        # Add the second residual
        resd2 = layers.Conv2D(num_filters, 1, padding="same", strides=(2, 1))(plus1)
        plus2 = layers.add([pool2, resd2])

        # Third backbone block
        num_filters *= 2
        conv3 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(plus2)
        norm3 = layers.BatchNormalization()(conv3)

        actv3 = layers.Activation("relu")(norm3)
        conv3 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(actv3)
        norm3 = layers.BatchNormalization()(conv3)

        pool3 = layers.MaxPooling2D(pool_size=(2, 1))(norm3)

        # Add the third backbone residual
        resd3 = layers.Conv2D(num_filters, 1, padding="same", strides=(2, 1))(plus2)
        plus3 = layers.add([pool3, resd3])

        # Fourth backbone block
        num_filters *= 2
        conv4 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(plus3)
        norm4 = layers.BatchNormalization()(conv4)

        actv4 = layers.Activation("relu")(norm4)
        conv4 = layers.SeparableConv2D(num_filters, kernel_size, padding="same")(actv4)
        norm4 = layers.BatchNormalization()(conv4)

        pool4 = layers.MaxPooling2D(pool_size=(2, 1))(norm4)

        # Add the fourth backbone residual
        resd4 = layers.Conv2D(num_filters, 1, padding="same", strides=(2, 1))(plus3)
        plus4 = layers.add([pool4, resd4])

        # Fourth upsample block
        actv5 = layers.Activation("relu")(plus4)
        conv5 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv5)
        norm5 = layers.BatchNormalization()(conv5)

        actv5 = layers.Activation("relu")(norm5)
        conv5 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv5)
        norm5 = layers.BatchNormalization()(conv5)

        up5 = layers.UpSampling2D(size=(2, 1))(norm5)

        # Add the fourth upsampling residual connection
        resd5 = layers.UpSampling2D(size=(2, 1))(plus4)
        resd5 = layers.Conv2D(num_filters, 1, padding="same")(resd5)
        plus5 = layers.add([up5, resd5])

        # Third upsampling layer
        num_filters //= 2
        actv6 = layers.Activation("relu")(plus5)
        conv6 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv6)
        norm6 = layers.BatchNormalization()(conv6)

        actv6 = layers.Activation("relu")(norm6)
        conv6 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv6)
        norm6 = layers.BatchNormalization()(conv6)

        up6 = layers.UpSampling2D(size=(2, 1))(norm6)

        # Add the third upsampling residual connection
        resd6 = layers.UpSampling2D(size=(2, 1))(plus5)
        resd6 = layers.Conv2D(num_filters, 1, padding="same")(resd6)
        plus6 = layers.add([up6, resd6])

        # Second upsampling layer
        num_filters //= 2
        actv7 = layers.Activation("relu")(plus6)
        conv7 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv7)
        norm7 = layers.BatchNormalization()(conv7)

        actv7 = layers.Activation("relu")(norm7)
        conv7 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv7)
        norm7 = layers.BatchNormalization()(conv7)

        up7 = layers.UpSampling2D(size=(2, 1))(norm7)

        # Add the third upsampling residual and transverse connection
        resd7 = layers.UpSampling2D(size=(2, 1))(plus6)
        resd7 = layers.Conv2D(num_filters, 1, padding="same")(resd7)
        plus7 = layers.add([up7, resd7])

        # First upsampling layer
        num_filters //= 2
        actv8 = layers.Activation("relu")(plus7)
        conv8 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv8)
        norm8 = layers.BatchNormalization()(conv8)

        actv8 = layers.Activation("relu")(norm8)
        conv8 = layers.Conv2DTranspose(num_filters, kernel_size, padding="same")(actv8)
        norm8 = layers.BatchNormalization()(conv8)

        up8 = layers.UpSampling2D(size=(2, 1))(norm8)

        # Add the third upsampling residual connection
        resd8 = layers.UpSampling2D(size=(2, 1))(plus7)
        resd8 = layers.Conv2D(num_filters, 1, padding="same")(resd8)
        plus8 = layers.add([up8, resd8])

        # Output block
        conv9 = layers.Conv2D(1, 1, activation="sigmoid", padding="same")(plus8)

        self.model = Model(input_layer, conv9)
        self.model.compile(
            optimizer="rmsprop",
            loss="binary_crossentropy",
            metrics=["accuracy", Recall(), Precision(), f1_metric],
        )

    def train(self, train_generator, validation_generator=None, epochs=20):
        """
        Train the model, will warm start if a pretrained model is loaded, otherwise cold starts.

        Args:
            train_generator: (DataGenerator) a keras Sequence object for generating subject snapshots
        Kwargs:
            epochs: (int) number of training epochs
        Returns:
            None

        """
        model_checkpoint_callback = ModelCheckpoint(
                filepath="tmp",
                save_weights_only=True,
                monitor="val_f1_metric",
                mode='max',
                save_best_only=True)

        history = self.model.fit(
            train_generator,
            validation_data=validation_generator,
            epochs=epochs,
            verbose=2,
            callbacks=[model_checkpoint_callback]
        )
        self.model.load_weights("tmp")

        self.histories.append(history)
        self.trained = True

    def infer(self, source_data):
        """
        Use a pretrained model to infer data risk scores

        Args:
            source_data: (numpy array) source data on which to perform inference
        Returns:
            a numpy array of model inferences
        """
        if self.trained:
            out = self.model.predict(source_data)
            return out

    def save_to_path(self, path):
        """
        Save the model

        Args:
            path: (str) where to save the model
        Returns:
            None
        """
        self.model.save(path)

    def load_from_path(self, path):
        """
        Load a model

        Args:
            path: (str) where the model is saved
        Returns:
            None
        """
        loaded_model = load_model(path)
        self.model = loaded_model
        self.trained = True

    def assess_snapshot(self, target_snapshot, record_snapshot):
        """
        Score the data risk of a single snapshot.

        Args:
            target_snapshot: (numpy array) the model inference scores
            record_snapshot: (numpy array) the record IDs of the target snapshot
        Returns:
            a dictionary whose keys are record IDs and whose values are aggregated data risk scores
        """

        idx_to_item = self.parameters["idx_to_name"]
        record_dict = {item_idx: dict(enumerate(record_snapshot[item_idx,:])) for item_idx in idx_to_item}
        target_dict = {item_idx: dict(enumerate(target_snapshot[item_idx,:])) for item_idx in idx_to_item}

        record_item_scores = defaultdict(int)

        for item_idx, item_dict in record_dict.items():
            for window_idx, record_id in item_dict.items():
                item = idx_to_item[item_idx]
                target_value = target_dict[item_idx][window_idx]
                if record_id != 0:
                    record_item_scores[record_id, item] = target_value

        return record_item_scores

    def assess_batch(self, infer_generator, infer=True):
        """
        Score the data risk of a batch of snapshots.

        Args:
            target_snapshots: (numpy array) the model inference scores
            record_snapshots: (numpy array) the record IDs of the target snapshot
        Returns:
            a dictionary whose keys are record IDs and whose values are aggregated data risk scores
        """
        if infer:
            target_snapshots = self.infer(infer_generator)
        else:
            target_snapshots = infer_generator.target_snapshots
        record_snapshots = infer_generator.record_snapshots

        record_item_counts = defaultdict(int)
        record_item_scores = defaultdict(int)
        record_item_scores_sq = defaultdict(int)


        for idx in range(target_snapshots.shape[0]):
            target_snapshot = target_snapshots[idx, :, :, 0]
            record_snapshot = record_snapshots[idx, :, :, 0]

            temp_record_item_score = self.assess_snapshot(target_snapshot, record_snapshot)

            for record_tup, record_item_score in temp_record_item_score.items():
                record_item_counts[record_tup] += 1
                record_item_scores[record_tup] += record_item_score
                record_item_scores_sq[record_tup] += record_item_score **2


        record_item_output = []

        for record_tup in record_item_scores:
            temp_row = {}
            temp_row["ItemGroupRecordId"] = record_tup[0]
            temp_row["ItemOID"] = record_tup[1]
            temp_row["OccurenceCount"] = record_item_counts[record_tup]
            mean = record_item_scores[record_tup]/record_item_counts[record_tup]
            temp_row["RawOutput"] = mean

            second_moment = record_item_scores_sq[record_tup]/record_item_counts[record_tup]
            std_dev = np.sqrt(second_moment - mean**2)
            temp_row["Confidence"] = 1 / (1 + std_dev)
            
            if mean > 0.25:
                temp_row["Prediction"] = 1
            else:
                temp_row["Prediction"] = 0

            record_item_output.append(temp_row)

        return record_item_output

class ZeroKnowledgeModel:
    """
    A Binomial random variable model, used to provide a baseline performance estimate.
    """

    def __init__(self, num_itemoids=128, window_size=20):
        """
        Initialize a model object.

        Args:
            None
        Kwargs:
            num_itemoids: (int) the number of itemoids to mimic
            window_size: (int) the number of past values to mimic
        Returns:
            None
        """
        self.num_itemoids = num_itemoids
        self.window_size = window_size
        self.rates = None

    def fit(self, train_generator):
        """
        Extract the GT positive rate from training data

        Args:
            train_generator: (DataGenerator) a keras Sequence object for generating subject snapshots
        Returns:
            None
        """

        means = np.zeros((self.num_itemoids, self.window_size))
        for idx in train_generator.partition["train"]:
            means += train_generator.target_snapshots[idx, :, :, 0]
        means = means / len(train_generator.partition["train"])
        means = means.mean(axis=1)
        # self.rates = np.tile(means, (1, self.window_size))
        self.rates = np.tile(means, (self.window_size, 1)).T

    def infer(self):
        """
        Generate a random target snapshot.

        Args:
            None
        Returns:
            None
        """

        random_mat = np.random.rand(self.num_itemoids, self.window_size)
        pred_mat = 1 * (random_mat < self.rates)
        return pred_mat

    def save_to_path(self, path):
        """
        Save the model

        Args:
            path: (str) where to save the model
        Returns:
            None
        """
        with open(path, "wb") as file:
            dill.dump(self, file)

    def load_from_path(self, path):
        """
        Load a model

        Args:
            path: (str) where the model is saved
        Returns:
            None
        """
        with open(path, "rb") as file:
            load = dill.load(file)

        for attr, value in load.__dict__.items():
            setattr(self, attr, value)
