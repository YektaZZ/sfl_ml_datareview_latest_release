import numpy as np
import sys

sys.path.append("../src/classes")

from datagenerator import DataGenerator
from datawindow import DataWindow

class DataGeneratorFactory:
    """
    A Keras data generator object for creating subject snapshots and randomly
    augmenting training snapshots.
    """

    def __init__(
        self,
        record_collection,
        encoder_collection,
        itemoids,
        window_size=20,
        batch_size=256,
        shuffle=True,
        augmentation=0.1,
        subject_split=0.3,
        split_date=None,
    ):
        """
        Initialize the data generator object.

        Args:
            record_collection: (RecordCollection) preprocessed audit and query data
            encoder_collection: (EncoderCollection) translator from study data to categorical data
            itemoids: (list) the ItemOIDs to be modeled
        Kwargs:
            window_size: (int) the number of past values to save in snapshots
            batch_size: (int) the number of samples per backprop step
            shuffle: (bool) check for shuffling the training data
            augmentation: (float) the probability that training data is augmented
            subject_split: (float) the ratio of subject used for the subject holdout set
            split_date: (str) the date used for temporal split
        Class Args:
            subjects: (dict) a dictionary which tracks whether a subject is used for training/validation
            partition: (dict) a dictionary which tracks which snapshots are used for training/validation
            source_snapshots: (numpy array) a 4-tensor which houeses the source data snapshots
                0 axis: snapshot number
                1 axis: ItemOID number
                2 axis: window number (how many edits ago was this source data used)
                3 axis: trivial dimension for keras model
            target_snapshots: (numpy array) a 4-tensor which houeses the target data snapshots, follows same indexing scheme as source_snapshots
            record_snapshots: (numpy array) a 4-tensor which houeses the record numbers of the source snapshots snapshots, follows same indexing scheme as source_snapshots
            altval_snapshots: (numpy array) a 4-tensor which counts the number of alternative source values , follows same indexing scheme as source_snapshots
        Returns:
            None
        """

        self.record_collection = record_collection
        self.encoder_collection = encoder_collection
        self.itemoids = itemoids

        self.window_size = window_size
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.augmentation = augmentation
        self.subject_split = subject_split
        self.split_date = split_date

        self.subjects = {"train": set(), "validation": set()}
        self.partition = {"train": set(), "validation": set()}

        self.source_snapshots = None
        self.target_snapshots = None
        self.record_snapshots = None
        self.altval_snapshots = None

        self.identify_viable_subjects()
        self.make_subject_snapshots()

    def identify_viable_subjects(self, record_count=100):
        """
        Filter out subjects with too few records, also splits subjects into the train and validation set.

        Args:
            None
        Kwargs:
            record_count: (int) the threshold for removing a subject
        Returns:
            None


        """
        subject_record_count = self.record_collection.manifest.groupby("SubjectName")[
            "ItemGroupRecordId"
        ].count().sort_values()
        subject_bool = subject_record_count > record_count
        viable_subjects = subject_record_count[subject_bool]

        split_bool = viable_subjects.cumsum() < self.subject_split * viable_subjects.sum()

        self.subjects["validation"].update(viable_subjects[split_bool].index)
        self.subjects["train"].update(viable_subjects[~split_bool].index)

        assert (
            len(self.subjects["train"].intersection(self.subjects["validation"])) == 0
        ), "Subject used for training and validation"

    def make_subject_snapshots(self):
        """
        Generate the rectangularized training data for the subject-centric model.

        Args:
            None
        Kwargs:
            None
        Returns:
            None
        """

        manifest = self.record_collection.manifest
        subject_id_list = self.subjects["train"].union(self.subjects["validation"])

        num_snapshots = self.record_collection.count_snapshots(self.itemoids)
        num_itemoids = len(self.itemoids)
        tensor_shape = (num_snapshots, num_itemoids, self.window_size, 1)

        source_snapshots = np.full(tensor_shape, 0)
        target_snapshots = np.full(tensor_shape, 0)
        record_snapshots = np.full(tensor_shape, 0)
        altval_snapshots = np.full(tensor_shape, 0)

        snapshot_id = 0

        for subject_id in subject_id_list:
            subject_manifest_mask = manifest["SubjectName"] == subject_id
            subject_manifest = manifest[subject_manifest_mask]
            subject_manifest = subject_manifest.sort_values("CreationDate")
            subject_record_ids = subject_manifest["ItemGroupRecordId"]
            subject_record_iter = subject_record_ids.iteritems()

            source_window = DataWindow(
                self.encoder_collection, window_size=self.window_size
            )
            target_window = DataWindow(
                self.encoder_collection, window_size=self.window_size, source=False
            )
            record_window = DataWindow(
                self.encoder_collection, window_size=self.window_size, source=False
            )
            altval_window = DataWindow(
                self.encoder_collection,
                window_size=self.window_size,
                source=False,
                fill_value=1,
            )

            for row_id, subject_record_id in subject_record_iter:
                subject_record = self.record_collection.records[subject_record_id]
                subject_error = False
                index_error = False

                source_changes = 0
                target_changes = 0

                source_data_to_add = {
                    item: value_list[0]
                    for item, value_list in subject_record.candidate_values.items()
                }
                target_data_to_add = {
                    item: value_list[0]
                    for item, value_list in subject_record.candidate_labels.items()
                }
                record_data_to_add = {
                    item: subject_record_id for item in subject_record.get_items()
                }
                altval_data_to_add = {
                    item: len(value_list)
                    for item, value_list in subject_record.candidate_values.items()
                }

                for item, source_value in source_data_to_add.items():
                    source_catch = source_window.add_new_value(item, source_value)
                    source_changes += source_catch

                for item, target_value in target_data_to_add.items():
                    target_catch = target_window.add_new_value(
                        item, 1 * any(target_data_to_add.values())
                    )
                    target_changes += target_catch

                for item, record_value in record_data_to_add.items():
                    record_catch = record_window.add_new_value(item, record_value)

                for item, altval_count in altval_data_to_add.items():
                    record_catch = altval_window.add_new_value(item, altval_count)

                if source_changes and target_changes:
                    source_snapshot = source_window.get_snapshot()
                    source_snapshots[snapshot_id, :, :, 0] = source_snapshot

                    target_snapshot = target_window.get_snapshot()
                    target_snapshots[snapshot_id, :, :, 0] = target_snapshot

                    record_snapshot = record_window.get_snapshot()
                    record_snapshots[snapshot_id, :, :, 0] = record_snapshot

                    altval_snapshot = altval_window.get_snapshot()
                    altval_snapshots[snapshot_id, :, :, 0] = altval_snapshot

                    if self.split_date:
                        creation_date = subject_record.edit_history.events[0].start_time
                        date_bool = creation_date > self.split_date
                    else:
                        date_bool = True

                    if self.subject_split:
                        subject_bool = subject_id in self.subjects["validation"]
                    else:
                        subject_bool = True

                    if not (subject_bool and date_bool):
                        self.partition["train"].add(snapshot_id)
                    else:
                        self.partition["validation"].add(snapshot_id)

                    snapshot_id += 1

                elif source_changes != target_changes:
                    raise RuntimeWarning(
                        f" subject {subject_id} has data misalignment; breaking subject loop"
                    )
                    subject_error = True
                    break

                if snapshot_id == num_snapshots:
                    index_error = True
                    # raise RuntimeWarning("Exceeded number of anticipated snapshots")
                    break

            if subject_error or index_error:
                break

        source_snapshots = source_snapshots.astype(float)
        target_snapshots = target_snapshots.astype(float)

        self.source_snapshots = source_snapshots
        self.target_snapshots = target_snapshots
        self.record_snapshots = record_snapshots
        self.altval_snapshots = altval_snapshots

    def make_data_generators(self):
        """
        Create a training and validation Keras Sequence object.

        Args:
            None
        Returns:
            a training DataGenerator and a validation DataGenerator

        """
        train_bool = np.asarray(
            [
                idx in self.partition["train"]
                for idx in range(self.source_snapshots.shape[0])
            ]
        )
        valid_bool = np.asarray(
            [
                idx in self.partition["validation"]
                for idx in range(self.source_snapshots.shape[0])
            ]
        )

        train_source_snapshots = self.source_snapshots[train_bool, :, :, :]
        train_target_snapshots = self.target_snapshots[train_bool, :, :, :]
        train_record_snapshots = self.record_snapshots[train_bool, :, :, :]
        train_altval_snapshots = self.altval_snapshots[train_bool, :, :, :]

        valid_source_snapshots = self.source_snapshots[valid_bool, :, :, :]
        valid_target_snapshots = self.target_snapshots[valid_bool, :, :, :]
        valid_record_snapshots = self.record_snapshots[valid_bool, :, :, :]
        valid_altval_snapshots = self.altval_snapshots[valid_bool, :, :, :]

        train_generator = DataGenerator(
            train_source_snapshots,
            train_target_snapshots,
            train_record_snapshots,
            train_altval_snapshots,
            batch_size=self.batch_size,
            shuffle=self.shuffle,
            augmentation=self.augmentation,
        )

        valid_batch_size = max(1, int(.1* valid_source_snapshots.shape[0]))
        valid_generator = DataGenerator(
            valid_source_snapshots,
            valid_target_snapshots,
            valid_record_snapshots,
            valid_altval_snapshots,
            batch_size = valid_batch_size,
            shuffle = self.shuffle,
            augmentation = 0)

        return train_generator, valid_generator

    def count_feature_values(self):
        """
        Generate the number of categorical features seen for each ItemOID.

        Args:
            None
        Returns:
            a dict of ItemOID, number of value pairs
        """
        return {
            key: 1 + int(self.source_snapshots[:, idx, :, :].max())
            for idx, key in enumerate(self.itemoids)
        }
