from ordinalencoder import (
    CategoricalOrdinalEncoder,
    NumericOrdinalEncoder,
    DateTimeOrdinalEncoder,
)

import dill

import nltk

nltk.download("stopwords")


class OrdinalEncoderCollection:
    """
    A class for handling multiple encoder objects, all of which correspond to different target ItemOIDs.

    Class Args:
        items_to_dtype: (dict) a dictionary of ItemOID, datatype pairs
        encoders: (dict) a dictionary of ItemOID, encoder pairs

    """

    def __init__(self, items_to_dtype=dict(), path=""):
        """
        Initialize a new object

        Args:
            None
        Kwargs:
            items_to_dtype: (dict) a dictionary of ItemOID, datatype pairs
            path: (str) the path to a saved OrdinalEncoderCollection
        Returns:
            None
        """

        self.items_to_dtype = items_to_dtype
        self.encoders = dict()

        if path:
            self.load_from_path(path)
        else:
            for item, dtype in items_to_dtype.items():
                if dtype[0] == "C":
                    self.encoders[item] = CategoricalOrdinalEncoder()
                elif dtype[0] == "N":
                    self.encoders[item] = NumericOrdinalEncoder()
                elif dtype[0] == "D":
                    self.encoders[item] = DateTimeOrdinalEncoder()
                else:
                    raise ValueError("unexpected data type, accepted types are C, N, D")

    def fit(self, item_to_string_value_set):
        """
        Fit the encoders to given item values.

        Args:
            item_to_string_value_set: (dict) a dictionary of item, set pairs; the set objects contain all values which occur in the target ItemOID
        Returns:
            None
        """
        for item, string_value_set in item_to_string_value_set.items():
            if item in self.encoders:
                if (
                    isinstance(self.encoders[item], NumericOrdinalEncoder)
                    and len(string_value_set) < 5
                ):
                    self.encoders[item] = CategoricalOrdinalEncoder()
                self.encoders[item].fit(string_value_set)

    def transform(self, item, value):
        """
        Transform the given ItemOID, ItemValue to an encoded value

        Args:
            item: (str) the target ItemOID
            value: (str) the raw string value of the ItemValue
        Returns:
            an encoded integer value
        """

        if item in self.encoders:
            return self.encoders[item].transform(value)

    def save_to_path(self, path):
        with open(path, "wb") as file:
            dill.dump(self, file)

    def load_from_path(self, path):
        with open(path, "rb") as file:
            load = dill.load(file)

        for attr, value in load.__dict__.items():
            setattr(self, attr, value)
