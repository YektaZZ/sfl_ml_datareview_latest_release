import numpy as np
from tensorflow.keras.utils import Sequence

import sys

sys.path.append("../src/classes")
from datawindow import DataWindow

import numpy as np
import itertools


class DataGenerator(Sequence):
    """
    A Keras data generator object for creating subject snapshots and randomly
    augmenting training snapshots.
    """

    def __init__(
        self,
        source_snapshots,
        target_snapshots,
        record_snapshots,
        altval_snapshots,
        batch_size=256,
        shuffle=True,
        augmentation=0.1,
    ):
        """
        Initialize the data generator object.

        Args:
            record_collection: (RecordCollection) preprocessed audit and query data
            encoder_collection: (EncoderCollection) translator from study data to categorical data
            itemoids: (list) the ItemOIDs to be modeled
        Kwargs:
            window_size: (int) the number of past values to save in snapshots
            batch_size: (int) the number of samples per backprop step
            shuffle: (bool) check for shuffling the training data
            augmentation: (float) the probability that training data is augmented
            subject_split: (float) the ratio of subject used for the subject holdout set
            split_date: (str) the date used for temporal split
        Class Args:
            subjects: (dict) a dictionary which tracks whether a subject is used for training/validation
            partition: (dict) a dictionary which tracks which snapshots are used for training/validation
            source_snapshots: (numpy array) a 4-tensor which houeses the source data snapshots
                0 axis: snapshot number
                1 axis: ItemOID number
                2 axis: window number (how many edits ago was this source data used)
                3 axis: trivial dimension for keras model
            target_snapshots: (numpy array) a 4-tensor which houeses the target data snapshots, follows same indexing scheme as source_snapshots
            record_snapshots: (numpy array) a 4-tensor which houeses the record numbers of the source snapshots snapshots, follows same indexing scheme as source_snapshots
            altval_snapshots: (numpy array) a 4-tensor which counts the number of alternative source values , follows same indexing scheme as source_snapshots
        Returns:
            None
        """

        self.batch_size = batch_size
        self.shuffle = shuffle
        self.augmentation = augmentation

        self.source_snapshots = source_snapshots
        self.target_snapshots = target_snapshots
        self.record_snapshots = record_snapshots
        self.altval_snapshots = altval_snapshots

        self.num_snapshots = self.source_snapshots.shape[0]
        self.num_itemoids = self.source_snapshots.shape[1]
        self.window_size = self.source_snapshots.shape[2]

        self.on_epoch_end()

    def __len__(self):
        """
        Compute the number of batches per training epoch.

        Args:
            None
        Returns:
            int
        """
        num_train = self.num_snapshots
        return int(np.floor(num_train / self.batch_size))

    def on_epoch_end(self):
        """
        Refresh the index set, shuffling if necessary.

        Args:
            None
        Returns:
            None
        """
        self.indices = np.asarray(list(range(self.num_snapshots)))

        if self.shuffle:
            np.random.shuffle(self.indices)

    def __getitem__(self, index):
        """
        Get a batch of training data.

        Args:
            index: (int) the batch number for the epoch
        Returns:
            a tuple of training source data and target data
        """
        start_index = index * self.batch_size
        end_index = (index + 1) * self.batch_size
        temp_indices = self.indices[start_index:end_index]

        X, Y = self.__data_generation(temp_indices)

        return X, Y

    def __data_generation(self, temp_indices):
        """
        Extract the appropriate training snapshots, augmenting the training set if possible.

        Args:
            temp_indices: (list) the snapshot ids of the training data to extract
        Returns:
            a tuple of training source data and target data
        """

        X = np.zeros((self.batch_size, self.num_itemoids, self.window_size, 1))
        Y = np.zeros((self.batch_size, self.num_itemoids, self.window_size, 1))

        for counter, index in enumerate(temp_indices):
            X[counter,] = self.source_snapshots[
                index,
            ]
            Y[counter,] = self.target_snapshots[
                index,
            ]

        return X, Y

class DomainDataGeneratorFactory:
    """
    Generates training and testing data for the domain model from a record collection.
    
    Args:
        None
    Returns:
        None
    """
    def __init__(self, record_collection, path_to_metadata):
        """
        Initialize a new object.
        
        Args:
            record_collection: (RecordCollection) the preprocessed data from which to extract data
            path_to_metadata: (str) a relative path to the metadata
            
        Returns:
            None
        """
        self.record_collection = record_collection
        
        metadata_df = pd.read_csv(path_to_metadata)
        self.itemoid_to_type = metadata_df[["FieldName","DataType"]].set_index("FieldName").to_dict()["DataType"]
        
    def get_record(self, record_id):
        """
        Fetch a record from the record collection object.
        
        Args:
            record_id: (int) the ItemGroupRecordId of the record
        Returns:
            a Record object
        """
        return self.record_collection.records[record_id]
    
    @staticmethod
    def to_onehot(encoded_value, num_features):
        """
        Transform an encoded value into a one-hot vector.
        
        Args:
            encoded_value: (int) a value from an Encoder
            num_features: (int) the number of values, determines the length of the vector
        Returns:
            a NumPy array
        """
        out = np.zeros(top_val)
        out[val] = 1
        return out
        
    def identify_viable_itemoids(self, domain):
        """
        Filter out any ItemOIDs which are sparesly recorded.
        
        Args:
            domain: (str) the name of the target domain table
        Returns:
            a list of ItemOIDs which are consistently recorded
        """
        itemoid_counter = Counter()
        for record_id in self.record_collection.domain_record_iterable(domain):
            record = self.get_record(record_id)
            record_items = record.get_items()
            itemoid_counter.update(record_items)
        most_common_count = max(itemoid_counter.values())
        viable_itemoids = [item for item, count in itemoid_counter.items() if count / most_common_count > .95 ]
        return viable_itemoids
    
    def identify_itemoid_values(self, domain):
        """
        Catalog all the ItemValues in the record collection.
        
        Args:
            domain: (str) the name of the target domain table
        Returns:
            a dictionary of ItemOID, values pairs
        """
        viable_itemoids = self.identify_viable_itemoids(domain)
        if viable_itemoids:
            itemoid_values = {itemoid: set() for itemoid in viable_itemoids}
            for record_id in self.record_collection.domain_record_iterable(domain):
                record = self.get_record(record_id)
                candidate_values = record.candidate_values
                for itemoid in itemoid_values:
                    itemoid_values[itemoid].update(candidate_values[itemoid])
            return itemoid_values
        else:
            raise ValueError("no viable itemoids")
    
    def fit_encoder_collection(self, domain):
        """
        Fit and EncoderCollection using a specified domain.
        
        Args:
            domain: (str) the name of the target domain table
        Returns:
            a fitted OrdinalEncoderCollection 
        """
        itemoid_values = self.identify_itemoid_values(domain)
        viable_itemoid_to_type = {itemoid:self.itemoid_to_type[itemoid] for itemoid in itemoid_values}
        encoder_collection = OrdinalEncoderCollection(items_to_dtype=viable_itemoid_to_type)
        encoder_collection.fit(itemoid_values)
        return encoder_collection
    
    def generate_data(self, domain, split=.3):
        """
        Extract data from the record collection, randomly split into test/train sets and augment training data
        
        Args:
            domain: (str) the name of the target domain table
        Kwargs:
            split: (float) the ratio of data to split for testing
        Returns:
            a 5 tuple:
                0: (NumPy array) augmented source data for training
                1: (NumPy array) augmented target data for training
                2: (NumPy array) source test data
                3: (NumPy array) target test data
                4: a dictionary of index, ItemOID pairs
            
        """
        encoder_collection = self.fit_encoder_collection(domain)
        feature_dict = {itemoid : max(encoder.string_to_ordinal.values()) + 1 for itemoid, encoder in encoder_collection.encoders.items()}
        itemoid_idx_to_item = {idx: item for idx, item in enumerate(encoder_collection.encoders)}
        
        source_train_data = []
        target_train_data = []
        
        source_test_data = []
        target_test_data = []
        
        for record_id in self.record_collection.domain_record_iterable(domain):
            record = self.get_record(record_id)
            
            candidate_values = record.candidate_values
            candidate_labels = record.candidate_labels
            
            random_variable = np.random.rand()
            
            zipped_candidate_data = [zip(candidate_values[itemoid], candidate_labels[itemoid]) for itemoid in encoder_collection.encoders]
            for candidate_idx, candidate_tuple in enumerate(itertools.product(*zipped_candidate_data)):
                source_row = []
                target_row = []
                
                for tuple_idx, tuple_pair in enumerate(candidate_tuple):    
                    itemoid = itemoid_idx_to_item[tuple_idx]
                    source_value, target_value = tuple_pair
                    
                    encoded_value = encoder_collection.transform(itemoid, source_value)
                    one_hot = self.to_onehot(encoded_value, feature_dict[itemoid])
                    
                    source_row.append(one_hot)
                    target_row.append(target_value)

                source_row = np.hstack(source_row)
                target_row = np.asarray(target_row)
                
                if random_variable < split:
                    if candidate_idx == 0:
                        source_test_data.append(source_row)
                        target_test_data.append(target_row)
                else:
                    source_train_data.append(source_row)
                    target_train_data.append(target_row)
                
        source_train_data = np.vstack(source_train_data)
        target_train_data = np.vstack(target_train_data)
                
        source_test_data = np.vstack(source_test_data)
        target_test_data = np.vstack(target_test_data)
        
        return source_train_data, target_train_data, source_test_data, target_test_data, itemoid_idx_to_item
