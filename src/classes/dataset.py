import pandas as pd


class Dataset:
    """
    Base class for ingesting data

    Class Vars:
        dataset: (pd.DataFrame) the data
        raw: (Boolean) represents whether or not the data is in its raw state
    """

    def __init__(self, path=""):
        self.dataset = None
        self.raw = True

        if path:
            self.load_csv(path)
            self.process_dataset()

    def load_csv(self, path):
        """
        Load CSV data from a local path:

        Args:
            path: (str) a path to the data
        Returns:
            None
        """
        self.dataset = pd.read_csv(path)

    def process_dataset(self):
        """
        Abstract method for processing data

        Args:
            None
        Returns:
            None
        """
        pass

    def to_grouped_dict(self):
        """
        Abstract method for generating a grouped dict for faster iteration

        Args:
            None
        Returns:
            None
        """
        pass


class AuditDataset(Dataset):
    def __init__(self, path=""):
        super().__init__(path=path)

    def process_dataset(self):
        """
        Processes data

        Args:
            None
        Returns:
            None
        """
        audit_df = self.dataset.fillna("NULL")
        edit_mask = audit_df["AuditSubCategoryName"].apply(lambda x: x[:7] == "Entered")
        audit_edit_df = audit_df[edit_mask]
        audit_edit_df = audit_edit_df[
            [
                "ItemGroupRecordId",
                "SubjectName",
                "FormOID",
                "ItemOID",
                "ItemValue",
                "DateTimeStamp",
                "UserOID",
                "AuditSubCategoryName",
            ]
        ]
        record_mask = audit_df["ItemGroupRecordId"] != "NULL"
        audit_edit_df = audit_edit_df[record_mask]
        audit_edit_df["ItemGroupRecordId"] = audit_edit_df["ItemGroupRecordId"].apply(
            int
        )
        audit_edit_df = audit_edit_df.sort_values("DateTimeStamp")
        audit_edit_df = audit_edit_df.fillna({"ItemValue": "NULL"})
        self.dataset = audit_edit_df
        self.raw = False

    def filter_on_date(self, date):
        """
        Rollback the dataset to a previous state.

        Args:
            data: (str) the date to roll back to
        Returns:
            None
        """
        self.dataset = self.dataset[self.dataset.DateTimeStamp < date]

    def to_grouped_dict(self):
        """
        Generates a grouped dict for faster iteration

        Args:
            None
        Returns:
            None
        """
        if not self.raw:
            group_by_id = self.dataset.groupby("ItemGroupRecordId")
            group_by_id_iter = iter(group_by_id)
            audit_grouped_dict = {record_id: df for record_id, df in group_by_id_iter}
            return audit_grouped_dict
        else:
            self.process_dataset()
            self.to_grouped_dict()


class QueryDataset(Dataset):
    def __init__(self, path=""):
        super().__init__(path=path)

    def process_dataset(self):
        """
        Processes data

        Args:
            None
        Returns:
            None
        """
        query_df = self.dataset.fillna("NULL")
        query_df = query_df[
            [
                "RecordId",
                "SubjectID",
                "FormOID",
                "OpenDate",
                "QueryOpenUserOID",
                "QueryRecipient",
                "ItemOID",
            ]
        ]
        query_df = query_df.sort_values("OpenDate")
        self.dataset = query_df
        self.raw = False

    def filter_on_date(self, date):
        """
        Rollback the dataset to a previous state.

        Args:
            data: (str) the date to roll back to
        Returns:
            None
        """
        self.dataset = self.dataset[self.dataset.OpenDate < date]

    def to_grouped_dict(self):
        """
        Generates a grouped dict for faster iteration

        Args:
            None
        Returns:
            None
        """
        if not self.raw:
            group_by_id = self.dataset.groupby("RecordId")
            group_by_id_iter = iter(group_by_id)
            query_grouped_dict = {record_id: df for record_id, df in group_by_id_iter}
            return query_grouped_dict
        else:
            self.process_dataset()
            self.to_grouped_dict()
