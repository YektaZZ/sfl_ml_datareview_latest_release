import pandas as pd
import dill
from record import Record
from collections import defaultdict


class RecordCollection:
    """
    Class for constructing and storing all records from Audit and Query data

    Class Vars:
        records: (dict) all study records keyed by ItemGroupRecordId
        manifest: (pd.DataFrame) a table of all records in the study
        populated: (Boolean) True if the records in the RecordCollection have been filled out

    """

    def __init__(self, path=""):
        self.records = {}
        self.manifest = None
        self.update_manifest = None
        self.populated = False

        if path:
            self.load_from_path(path)

    def make_manifest(self, audit_dataset):
        """
        Generate a table of all ItemGroupRecordIds for a study, along with related information
            for accessing data.

        Args:
            audit_dataset: (AuditDataset) audit data for study
        Returns:
            None
        """
        audits_with_id = audit_dataset.dataset[
            audit_dataset.dataset["ItemGroupRecordId"].notna()
        ]
        creation_date_group = audits_with_id.groupby("ItemGroupRecordId")
        creation_date = creation_date_group["DateTimeStamp"].min()
        creation_date = creation_date.rename("CreationDate")
        manifest = audits_with_id[["ItemGroupRecordId", "SubjectName", "FormOID"]]
        manifest = manifest.drop_duplicates()
        manifest = manifest.merge(
            creation_date, left_on="ItemGroupRecordId", right_index=True
        )

        if isinstance(self.manifest, pd.DataFrame):
            self.update_manifest = manifest
        else:
            self.manifest = manifest

    def initialize_records(self):
        """
        Allocate memory for Record objects, speeds up iteration

        Args:
            None
        Returns:
            None
        """
        if isinstance(self.update_manifest, pd.DataFrame):
            existing_records = self.manifest.ItemGroupRecordId
            update_records = self.update_manifest.ItemGroupRecordId
            new_record_mask = ~update_records.isin(existing_records)
            new_records = self.update_manifest[new_record_mask]
            iterator = new_records.iterrows()

        elif isinstance(self.manifest, pd.DataFrame):
            iterator = self.manifest.iterrows()

        else:
            raise ValueError("record manifest not defined")

        for idx, record_row in iterator:
            record_id = record_row["ItemGroupRecordId"]
            subject_id = record_row["SubjectName"]
            form = record_row["FormOID"]

            new = Record(record_id=record_id, subject_id=subject_id, form=form)
            self.records[record_id] = new

    def make_records(self, audit_dataset, query_dataset):
        """
        Fill in relevant information in pre-allocated Record objects

        Args:
            audit_dataset: (AuditDataset) audit data for study
            query_dataset: (QueryDataset) query data for study
        Returns:
            None
        """
        if self.records:
            audit_grouped_dict = audit_dataset.to_grouped_dict()
            query_grouped_dict = query_dataset.to_grouped_dict()

            for record_id, record in self.records.items():
                if record_id in audit_grouped_dict:
                    edit_record = audit_grouped_dict[record_id]
                    record.add_edit_history(edit_record)

                if record_id in query_grouped_dict:
                    query_record = query_grouped_dict[record_id]
                    record.add_query_history(query_record)

                record.extract_ground_truth()
            self.populated = True
        else:
            pass

    def save_to_path(self, path):
        with open(path, "wb") as file:
            dill.dump(self, file)

    def load_from_path(self, path):
        with open(path, "rb") as file:
            load = dill.load(file)

        for attr, value in load.__dict__.items():
            setattr(self, attr, value)

    def get_items(self):
        """
        Get the exhaustive set of items contained in the record collection.

        Args:
            None
        Returns
            a set of items
        """

        itemoids = set()
        for record in self.records.values():
            itemoids.update(record.get_items())
        return itemoids

    def count_snapshots(self, itemoid_list):
        """
        Count the number of snapshots to expect from the subject-centric data generation routine,
            allows for a priori memory allocation.

        Args:
            itemoid_list: (list) all ItemOID values to include in the snapshots
        Returns:
            an integer count of snapshots
        """

        itemoid_set = set(itemoid_list)
        num_shapshots = 0

        for record_id, record in self.records.items():
            record_itemoids = record.get_items()

            if record_itemoids.intersection(itemoid_set):
                num_shapshots += 1

        return num_shapshots

    def get_queried_ids(self, date=None):
        """
        Populate a list of all records which have been queried.

        Args:
            None
        Kwargs:
            date: (str) if a date is specified the queries must be issued before date
        Returns:
            a list of record IDs corresponding to records queried before date
        """
        previously_queried_records = []
        for record_id, record in self.records.items():
            if record.query_history.is_manual(date=date):
                previously_queried_records.append(record_id)

        return previously_queried_records

    def filter_record_scores(self, record_scores, date=None):
        """
        Filters out records which have been previously queried from a record score dictionary

        Args:
            record_scores: (dict) a dictionary of record ID, model score pairs
        Kwargs:
            date: (str) if a date is specified the queries must be issued before date
        Returns:
            a dictionary of record ID, model score pairs whose record IDs have not been previously queried
        """
        previously_queried_records = self.get_queried_ids(date=date)
        filtered_record_scores = {
            record_id: score
            for record_id, score in record_scores.items()
            if record_id not in previously_queried_records
        }
        return filtered_record_scores

    def get_modeling_parameters(self, path_to_metadata):
        """
        Determine the number of values that each ItemOID has in the data, and determine the 128 most queried ItemOIDs.

        Args:
            path_to_metadata: (str) the path to the appropriate metadata csv
        Returns:
            a dictionary of all values seen for each ItemOID, and a sorted list of the 128 most queried ItemOIDs
        """
        GT_counts = defaultdict(int)
        itemoid_values = defaultdict(set)

        for record in self.records.values():
            if any(map(lambda x: any(x), record.candidate_labels.values())):
                for itemoid in record.get_items():
                    GT_counts[itemoid] += 1
            for itemoid, values in record.candidate_values.items():
                itemoid_values[itemoid].update(values)

        metadata_df = pd.read_csv(path_to_metadata)
        metadata_df = metadata_df[["FieldName", "DataType"]]
        metadata_df = metadata_df.set_index("FieldName")
        metadata_dict = metadata_df.to_dict()
        metadata_dict = metadata_dict["DataType"]

        sorted_itemoids = sorted(list(GT_counts.items()), key=lambda x: -x[1])
        sorted_itemoids = list(map(lambda x: x[0], sorted_itemoids))
        sorted_itemoids = [
            itemoid for itemoid in sorted_itemoids if itemoid in metadata_dict
        ]

        num_itemoids = min(128, 32 * (len(sorted_itemoids) // 32))

        idx = 0
        last_itemoid = ""
        selected_itemoids = []

        while len(selected_itemoids) < num_itemoids:
            last_itemoid_with_suffix = last_itemoid + "_UNIT"
            if last_itemoid_with_suffix in sorted_itemoids:
                new_itemoid = last_itemoid_with_suffix
            else:
                new_itemoid = sorted_itemoids[idx]
                idx += 1

            if new_itemoid not in selected_itemoids:
                selected_itemoids.append(new_itemoid)
                last_itemoid = new_itemoid

        sorted_itemoids_to_dtype = {
            itemoid: metadata_dict[itemoid] for itemoid in selected_itemoids
        }

        return itemoid_values, selected_itemoids, sorted_itemoids_to_dtype

    def domain_record_iterable(self, domain):
        domain_bool = (self.manifest.FormOID == domain)
        if any(domain_bool):
            domain_iter = self.manifest[domain_bool].ItemGroupRecordId
            return domain_iter
        else:
            raise ValueError(f"{domain} not in records")

