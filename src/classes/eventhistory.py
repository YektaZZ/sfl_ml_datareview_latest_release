import pandas as pd
from collections import defaultdict
from event import EditEvent, QueryEvent


class EventHistory:
    """
    Abstract class representing the history of events for a record.

    Class Vars:
        events: (list) the sequence of events in chronological order
    """

    def __init__(self):
        self.events = []

    def __len__(self):
        return len(self.events)

    def add_event(self, event_dict):
        """
        Abstract method for adding an event to the event list.

        Args:
            event_dict: (dict) relevant event information

        Returns:
            None
        """
        pass

    def get_event(self, event_index):
        """
        Get method for accessing events in the event list.

        Args:
            event_index: (int) the desired index
        Returns:
            Event object corresponding to the index
        """
        if isinstance(event_index, int) and 0 <= event_index < len(self):
            event = self.events[event_index]
            return event
        else:
            raise IndexError("edit_number out of range")

    def from_event_record(self, event_record):
        """
        Abstract method for generating an EventHistory from SQL data.

        Args:
            event_record: (pd.DataFrame) a dataframe containing all events which occur
                to the record in question
        Returns:
            None
        """
        pass


class EditEventHistory(EventHistory):
    """
    Class for tracking the edits made to a Record

    Class Vars:
        events: (list) the sequence of events in chronological order
    """

    def __init__(self):
        super().__init__()

    @staticmethod
    def to_edit_dict(edit_df):
        """
        Transforms an edit data frame into a dictionary.

        Args:
            edit_df: (pd.DataFrame) edit data frame
        Returns:
            a dictionary of critical edit information
        """
        edit_content_df = edit_df[
            ["ItemOID", "ItemValue", "DateTimeStamp", "UserOID", "AuditSubCategoryName"]
        ]
        edit_content_df_pivot = edit_content_df.T
        edit_dict = edit_content_df_pivot.to_dict()
        return edit_dict

    def is_edited(self):
        """
        Check if the initial and final ItemValues of the EditHistory are identical.

        Args:
            None
        Returns:
            Boolean, True if the ItemValues have been changed from their initial values
        """
        initial = self.state(0)
        final = self.state(-1)
        if initial == final:
            return False
        return True

    def add_event(self, event_dict):
        """
        Add an event to the event list.

        Args:
            event_dict: (dict) relevant event information

        Returns:
            None
        """
        event = EditEvent()
        event.from_dict(event_dict)
        self.events.append(event)

    def timestamp_to_edit_index(self, timestamp):
        """
        docstring
        """
        for index, edit_event in enumerate(self.events):
            if edit_event.start_time > timestamp:
                return max(index - 1, 0)
        return len(self) - 1

    def state(self, edit_index, item=""):
        """
        Rolls back data to the state immediately after the edit_index

        Args:
            edit_index: (int) last edit index to include, passing -1
                returns the final state of the record
        Kwargs:
            item: (str) a specific item to rollback
        Returns:
            a dictionary of ItemOID, ItemValue pairs if no item is passed, otherwise a string
        """
        if edit_index == -1:
            edit_index = len(self) - 1
        out = defaultdict(lambda: "NULL")
        for idx in range(edit_index + 1):
            edit = self.get_event(idx)
            edit_dict = dict(edit)
            out.update(edit_dict)

        if item:
            if item in out:
                return out[item]
            else:
                return "NULL"

        return out

    def from_event_record(self, event_record):
        """
        Generate an EventHistory from SQL data.

        Args:
            event_record: (pd.DataFrame) a dataframe containing all events which occur
                to the record in question
        Returns:
            None
        """
        event_dates = event_record["DateTimeStamp"].unique()

        for event_date in event_dates:
            event_mask = event_record["DateTimeStamp"] == event_date
            event_df = event_record[event_mask]
            event_dict = self.to_edit_dict(event_df)
            self.add_event(event_dict)

    def get_items(self):
        """
        Get the exhaustive set of items contained in the event history.

        Args:
            None
        Returns
            a set of items
        """
        itemoids = set()
        for event in self.events:
            itemoids.update(event.values.keys())
        return itemoids


class QueryEventHistory(EventHistory):
    """
    Class for tracking the queries issued for a Record

    Class Vars:
        events: (list) the sequence of events in chronological order
    """

    def __init__(self):
        super().__init__()

    def is_manual(self, date=None):
        """
        Checks if the history contains any manual queries.

        Args:
            None
        Kwargs:
            date: (str) if a date is specified the function checks for manual queries before the date
        Returns:
            a Boolean, True if any query was generated manually
        """
        for event in self.events:
            if date:
                if event.is_manual() and event.start_time < date:
                    return True
            else:
                if event.is_manual():
                    return True
        return False

    def add_event(self, event_dict):
        """
        Add an event to the event list.

        Args:
            event_dict: (dict) relevant event information

        Returns:
            None
        """
        event = QueryEvent()
        event.from_dict(event_dict)
        self.events.append(event)

    def from_event_record(self, event_record):
        """
        Generate an EventHistory from SQL data.

        Args:
            event_record: (pd.DataFrame) a dataframe containing all events which occur
                to the record in question
        Returns:
            None
        """
        for idx, event in event_record.iterrows():
            event_dict = event.to_dict()
            self.add_event(event_dict)

    def get_manual_queries(self):
        """
        Generate an ordered list of manual queries

        Args:
            None
        Returns:
            a chronologically sorted list of query start time, itemoid pairs
        """
        time_key_pairs = [
            (event.start_time, event.item) for event in self.events if event.is_manual()
        ]
        unique_time_key_pairs = set(time_key_pairs)
        sorted_time_key_pairs = sorted(unique_time_key_pairs, reverse=True)
        return sorted_time_key_pairs
