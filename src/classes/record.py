import pandas as pd
from collections import defaultdict
from eventhistory import EditEventHistory, QueryEventHistory


class Record:
    """
    A class representing the information from a single ItemGroupRecord

    Class Variables:
        record_id: (int) the ItemGroupRecordId for the record
        subject_id: (int) the SubjectName for the record
        form: (str) the FormOID for the record
        edit_history: (EditEventHistory) the log of edits made to the record
        query_history:(QueryEventHistory) the log of queries made to the record

        label: (bool) the ground truth label for the record
    """

    def __init__(self, record_id=0, subject_id=0, form=""):
        self.record_id = record_id
        self.subject_id = subject_id
        self.form = form

        self.edit_history = EditEventHistory()
        self.query_history = QueryEventHistory()

        self.candidate_labels = defaultdict(list)
        self.candidate_values = defaultdict(list)

    def add_edit_history(self, event_record):
        """
        Construct the edit event history of the record:

        Args:
            event_record: (pd.DataFrame) a data table of all edit events which
                have occured for the record ID
        Returns:
            None
        """
        self.edit_history.from_event_record(event_record)

    def add_query_history(self, event_record):
        """
        Construct the query event history of the record:

        Args:
            event_record: (pd.DataFrame) a data table of all query events which
                have occured for the record ID
        Returns:
            None
        """
        self.query_history.from_event_record(event_record)

    def extract_ground_truth(self):
        """
        Generates ground truth label for the record.

        Args:
            None
        Returns:
            a Boolean, True if the record has differing initial and final ItemValues
                AND has been manually queried
        """

        for item, lock_value in self.edit_history.state(-1).items():
            self.candidate_values[item].append(lock_value)
            self.candidate_labels[item].append(0)

        for time, item in self.query_history.get_manual_queries():
            candidate_index = self.edit_history.timestamp_to_edit_index(time)
            candidate_value = self.edit_history.state(candidate_index, item=item)

            if candidate_value not in self.candidate_values[item]:
                self.candidate_values[item].insert(0, candidate_value)
                lock_value = self.candidate_values[item][-1]

                if candidate_value != lock_value:
                    self.candidate_labels[item].insert(0, 1)
                else:
                    self.candidate_labels[item].insert(0, 0)

    def get_items(self):
        """
        Get the exhaustive set of items contained in the record.

        Args:
            None
        Returns
            a set of items
        """
        return self.edit_history.get_items()

    def state(self, edit_index):
        """
        Rolls back data to the state immediately after the edit_index

        Args:
            edit_index: (int) last edit index to include, passing -1
                returns the final state of the record
        Returns:
            a dictionary of ItemOID, ItemValue pairs
        """
        self.edit_history.state(edit_index)
