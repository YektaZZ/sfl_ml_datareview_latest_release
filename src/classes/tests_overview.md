# Tests Overview

Based on the first pass of unit tests, these were the fail criteria I found. This document is separated by scripts (e.g. Dataset) and test functions (e.g. test_dataset_query_required_fields).

## Dataset
#### test_dataset_query_required_fields
Code fails with the empty init params, need to load the dataset before processing for this test.

## Event
### test_event_empty_init
EditEvent fails because it does not inherit from Event class

### test_event_class_inheritance
The EditEvent class does not inherit from the Event class

## EventHistory
### test_eventhistory_edit_required_date
`to_edit_dict` is not defined

## OrdinalEncoder
### test_ordinalencoder_required_string_value_len
Currently generates a ZeroDivisionError, should generate ValueError (or QuantityError?)

### test_ordinalencoder_fit_input
The function does not raise a TypeError for these inputs

## OrdinalEncoderCollection
### test_ordinalencodercollection_items_to_dtype_errors
Currently generates no error, should generate ValueError

### test_ordinalencodercollection_init_input
The function does not raise a TypeError for non-dict inputs

### test_ordinalencodercollection_fit_input
The function does not raise a TypeError for non-dict inputs

## Record
### test_record_add_edit_history
`to_edit_dict`

### test_record_add_query_history
The event_record is never added to the events

## RecordCollection
### test_recordcollection_make_records
May need better dummy data?