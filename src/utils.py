import pandas as pd
import numpy as np
import sys

sys.path.append("../classes")
from datawindow import DataWindow


def to_edit_dict(edit_df):
    """
    Transforms an edit data frame into a dictionary.

    Args:
        edit_df: (pd.DataFrame) edit data frame
    Returns:
        a dictionary of critical edit information
    """
    edit_content_df = edit_df[
        ["ItemOID", "ItemValue", "DateTimeStamp", "UserOID", "AuditSubCategoryName"]
    ]
    edit_content_df_pivot = edit_content_df.T
    edit_dict = edit_content_df_pivot.to_dict()
    return


def make_subject_snapshots(
    record_collection,
    encoder_collection,
    subject_id_list,
    window_size=20,
    date_split="",
):
    """
    Generate the rectangularized training data for the subject-centric model.

    Args:
        record_collection: (RecordCollection) contains the records for a study
        encoder_collection: (OrdinalEncoderCollection) encoders for transforming string data into encoded ints
        subject_id_list: (list) the SubjectName values to include in the snapshots
    Kwargs:
        window_size: (int) the number of past values to include in the snapshots
    Returns:
        source_snapshots: (numpy array) a 3-tensor of source data
            first index: ItemOID index
            second index: number of data lags
            third index: snapshot number

        target_snapshots: (numpy array) a 3-tensor of ground truth data, same indexing
            as source_snapshots with corresponding target data
    """
    itemoid_list = list(encoder_collection.encoders.keys())
    manifest = record_collection.manifest
    num_snapshots = record_collection.count_snapshots(itemoid_list)
    num_itemoids = len(itemoid_list)
    tensor_shape = (num_snapshots, num_itemoids, window_size, 1)

    if date_split:
        date_mask = np.full(num_snapshots, False)

    source_snapshots = np.full(tensor_shape, -1)
    target_snapshots = np.full(tensor_shape, 0)
    record_snapshots = np.full(tensor_shape, 0)

    snapshot_id = 0

    # Iterate over all subjects
    for subject_id in subject_id_list:
        # Identify all subject records
        subject_manifest_mask = manifest["SubjectName"] == subject_id
        subject_manifest = manifest[subject_manifest_mask]
        subject_manifest = subject_manifest.sort_values("CreationDate")
        subject_record_ids = subject_manifest["ItemGroupRecordId"]
        subject_record_iter = subject_record_ids.iteritems()

        source_window = DataWindow(encoder_collection, window_size=window_size)
        target_window = DataWindow(
            encoder_collection, window_size=window_size, fill_value=0, source=False
        )
        record_window = DataWindow(
            encoder_collection, window_size=window_size, fill_value=0, source=False
        )

        # Iterate over all records for the subject
        for row_id, subject_record_id in subject_record_iter:
            subject_record = record_collection.records[subject_record_id]
            subject_error = False
            index_error = False

            if date_split:
                creation_date = subject_record.edit_history.events[0].start_time
                if creation_date > date_split:
                    date_mask[snapshot_id] = True

            source_changes = 0
            target_changes = 0
            record_changes = 0

            source_data_to_add = {
                item: value_list[0]
                for item, value_list in subject_record.candidate_values.items()
            }
            target_data_to_add = {
                item: value_list[0]
                for item, value_list in subject_record.candidate_labels.items()
            }
            record_data_to_add = {
                item: subject_record_id for item in subject_record.get_items()
            }

            # Add all data to the snapshot
            for item, source_value in source_data_to_add.items():
                source_catch = source_window.add_new_value(item, source_value)
                source_changes += source_catch

            if sum(target_data_to_add.values()) > 0:
                label = 1
            else:
                label = 0

            for item, target_value in target_data_to_add.items():
                target_catch = target_window.add_new_value(item, label)
                target_changes += target_catch

            for item, record_value in record_data_to_add.items():
                record_catch = record_window.add_new_value(item, record_value)
                record_changes += record_catch

            # Check that both the source and target windows have been updated
            if source_changes and target_changes:
                source_snapshot = source_window.get_snapshot()
                source_snapshots[snapshot_id, :, :, 0] = source_snapshot

                target_snapshot = target_window.get_snapshot()
                target_snapshots[snapshot_id, :, :, 0] = target_snapshot

                record_snapshot = record_window.get_snapshot()
                record_snapshots[snapshot_id, :, :, 0] = record_snapshot

                snapshot_id += 1

            # If this is reached an alignment error has occured
            elif source_changes != target_changes:
                raise RuntimeWarning(
                    f" subject {subject_id} has data misalignment; breaking subject loop"
                )
                subject_error = True
                break
            # If this is reached the wrong number of snapshots was initialized
            if snapshot_id == num_snapshots + 1:
                index_error = True
                print("exceeded num snapshots")
                break

        if subject_error or index_error:
            break

    source_snapshots = 1 + source_snapshots.astype(float)
    target_snapshots = target_snapshots.astype(float)

    if date_split:
        return source_snapshots, target_snapshots, record_snapshots, date_mask

    return source_snapshots, target_snapshots, record_snapshots
