import nltk
from collections import defaultdict
import numpy as np
import pandas as pd
import sys

sys.path.append("../src/classes")
from model import SubjectModel
from ordinalencodercollection import OrdinalEncoderCollection
from dataset import AuditDataset, QueryDataset
from recordcollection import RecordCollection
from datageneratorfactory import DataGeneratorFactory
from collections import OrderedDict, defaultdict


nltk.download("stopwords")

def train_model(
    path_to_record_collection, path_to_metadata, path_to_model, path_to_encoder
):
    """
    Load a preprocessed record colection, train a subject model and save the model to a specified path

    Args:
        path_to_record_collection: (str) path to a preprocessed record collection
        path_to_metadata: (str) path to the appropriate metadata
        path_to_encoder_collection: (str) path to a preprocessed encoder collection
        path_to_model: (str) path to a trained model
    Returns:
        None
    """
    record_collection = RecordCollection(path=path_to_record_collection)

    (
        itemoid_values,
        sorted_itemoids,
        sorted_itemoids_to_dtype,
    ) = record_collection.get_modeling_parameters(path_to_metadata)

    encoder_collection = OrdinalEncoderCollection(
        items_to_dtype=sorted_itemoids_to_dtype
    )
    encoder_collection.fit(itemoid_values)
    encoder_collection.save_to_path(path_to_encoder)

    generator_factory = DataGeneratorFactory(
        record_collection, encoder_collection, sorted_itemoids
    )
    feature_dict = generator_factory.count_feature_values()
    train_generator, validation_generator = generator_factory.make_data_generators()

    model = SubjectModel()
    model.build_model(feature_dict)

    model.train(train_generator, validation_generator=validation_generator, epochs=10)
    model.save_to_path(path_to_model)
