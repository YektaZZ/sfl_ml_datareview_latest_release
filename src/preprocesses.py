import sys

sys.path.append("./classes")
from dataset import AuditDataset, QueryDataset
from recordcollection import RecordCollection


def make_record_collection(path_to_audit_csv, path_to_query_csv, path_to_saved_records):
	"""
	TODO
	"""
    audit = AuditDataset(path=path_to_audit_csv)
    query = QueryDataset(path=path_to_query_csv)

    record_collection = RecordCollection()
    record_collection.make_manifest(audit)
    record_collection.initialize_records()
    record_collection.make_records(audit, query)

    record_collection.save_to_path(path_to_saved_records)
