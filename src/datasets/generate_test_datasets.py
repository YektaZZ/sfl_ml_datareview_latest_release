import pandas as pd
import numpy as np

import pickle
import random

import os
import sys

from tqdm.notebook import tqdm

###############
## VARIABLES ##
###############

ABS_PATH = os.path.dirname(os.path.abspath(__file__))

STUDIES = [
    "Clintek_010",
    "Clintek_011",
    "Clintek_012",
    "Clintek_013",
    "Clintek_015",
]

TOP_DOMAINS = ["EX", "CM", "SV", "AE"]

# Audit == Edit
AUDIT_DATA_COLS = [
    "ItemGroupRecordId",
    "SubjectName",
    "FormOID",
    "ItemOID",
    "ItemValue",
    "DateTimeStamp",
    "UserOID",
    "AuditSubCategoryName",
]

# Query
QUERY_DATA_COLS = [
    "RecordId",
    "SubjectID",
    "FormOID",
    "OpenDate",
    "CloseDate",
    "QueryOpenUserOID",
    "QueryRecipient",
    "ItemOID",
    "ItemValue",
]

AUDIT_SUB_CATEGORIES = {
    "query": "QueryOpen",
    "no query": "Entered",
}

START_TIMESTAMP = pd.Timestamp("2020-01-01")
END_TIMESTAMP = pd.Timestamp("2020-12-31")

n_records = 60
min_record_n, max_record_n = 10000, 100000

assert n_records <= (
    max_record_n - min_record_n
), "Insufficient amount of record IDs for given number of records."
unique_records = np.random.choice(
    range(min_record_n, max_record_n), n_records, replace=False
)
UNIQUE_RECORDS = [str(x) for x in unique_records]

# Number of rows to be generated for each record in query dataset
ROW_PER_RECORD = 2

###############
## FUNCTIONS ##
###############


def get_query_recipient():
    if random.random() < 0.5:
        return "DM to Site"
    else:
        return "Monitor / CRA to Site"


def get_audit_subcategory(audit_subcategory):
    return AUDIT_SUB_CATEGORIES[audit_subcategory]


""" Returns a timestamp within the range [start, end]. """


def random_timestamp(start=START_TIMESTAMP, end=END_TIMESTAMP):
    return pd.Timestamp(random.random() * (end - start) + start).round("s")


def generate_pseudo_data(
    df, itemoids, domain, record, audit_subcategory=None, time_data=None
):
    n_rows = len(itemoids) * ROW_PER_RECORD
    for col in df.columns:
        if col == "ItemOID":
            df[col] = itemoids * ROW_PER_RECORD
        elif col == "FormOID":
            df[col] = [domain] * n_rows
        elif col in ["RecordId", "ItemGroupRecordId"]:
            df[col] = [record] * n_rows
        elif col == "QueryRecipient":
            df[col] = [get_query_recipient() for y in range(n_rows)]
        elif col == "AuditSubCategoryName":
            df[col] = [get_audit_subcategory(audit_subcategory) for y in range(n_rows)]
        elif col == "OpenDate":
            df[col] = [random_timestamp() for y in range(n_rows)]
        elif col == "CloseDate":
            # The CloseDate must come after and 30 days within the OpenDate
            df[col] = df["OpenDate"].apply(
                lambda x: random_timestamp(x, x + pd.Timedelta(30, unit="D"))
            )
        elif col == "DateTimeStamp":
            # Use the time_data to match up with the query data if available
            if time_data is not None:
                df[col] = time_data
            # Otherwise randomize the timestamps
            else:
                df[col] = [random_timestamp() for y in range(n_rows)]
        elif "id" in col.lower():
            df[col] = ["x." + str(np.random.randint(0, 10)) for y in range(n_rows)]
        elif "name" in col.lower():
            df[col] = ["x_" + str(np.random.randint(0, 100)) for y in range(n_rows)]
        else:
            df[col] = [str(round(np.random.ranf() * 100, 2)) for y in range(n_rows)]
    return df


def extend_audit_query_data(audit_data, query_data, metadata, domain, record):
    # Add the domain prefix to the itemoids
    itemoids = metadata["FieldName"].values
    itemoids = [domain + "." + item for item in itemoids]

    # Generate the pseduo data based on the column names
    # NOTE: The query_data must be generated first because the OpenDate is used
    # to map queries to the audit_data
    query_data = generate_pseudo_data(
        query_data, itemoids, domain, record, audit_subcategory=None, time_data=None
    )

    # Create two audit dataframes: one that matches up with the query data,
    # and the other that does not
    audit_data_1 = generate_pseudo_data(
        audit_data.copy(),
        itemoids,
        domain,
        record,
        audit_subcategory="query",
        time_data=query_data["OpenDate"].values,
    )
    audit_data_2 = generate_pseudo_data(
        audit_data.copy(),
        itemoids,
        domain,
        record,
        audit_subcategory="no query",
        time_data=None,
    )

    audit_data = pd.concat([audit_data_1, audit_data_2], ignore_index=True)

    return audit_data, query_data


##############
## DATASETS ##
##############


def generate():
    # Metadata table must be loaded first
    metadata = pd.read_excel(
        os.path.join(ABS_PATH, "..", "..", "data", "raw", "SDTMMetadata_20200330.xlsx")
    )

    # Empty audit and query dataframes from which to extend the data
    audit_data = pd.DataFrame(columns=AUDIT_DATA_COLS)
    query_data = pd.DataFrame(columns=QUERY_DATA_COLS)

    # Query and audit dataset created for each study + domain + record
    query_data_dict, audit_data_dict = {}, {}
    for study in tqdm(STUDIES, "Dataset Generation"):
        for domain in TOP_DOMAINS:
            audit_records, query_records = [], []
            for record in UNIQUE_RECORDS:
                ds_metadata = metadata[
                    (metadata["StudyName"] == study)
                    & (metadata["DomainName"] == domain)
                ]

                audit_ext, query_ext = extend_audit_query_data(
                    audit_data.copy(), query_data.copy(), ds_metadata, domain, record
                )
                audit_records += [audit_ext]
                query_records += [query_ext]

            audit_data_dict[study + "+" + domain] = pd.concat(
                audit_records, ignore_index=True
            )
            query_data_dict[study + "+" + domain] = pd.concat(
                query_records, ignore_index=True
            )

    pickle.dump(
        audit_data_dict,
        open(
            os.path.join(ABS_PATH, "..", "..", "data", "raw", "test_dict_audit_df.p"),
            "wb",
        ),
    )
    pickle.dump(
        query_data_dict,
        open(
            os.path.join(ABS_PATH, "..", "..", "data", "raw", "test_dict_query_df.p"),
            "wb",
        ),
    )

    # Finally expand the datasets to be used in the pipeline tests

    audit_df = pd.concat(audit_data_dict.values(), ignore_index=True)
    query_df = pd.concat(query_data_dict.values(), ignore_index=True)

    audit_df = audit_df.sort_values(["ItemGroupRecordId", "DateTimeStamp"])
    query_df = query_df.sort_values(["RecordId", "OpenDate"])

    audit_df.to_csv(
        os.path.join(ABS_PATH, "..", "..", "data", "raw", "test_dataset_audit_df.csv"),
        index=False,
    )
    query_df.to_csv(
        os.path.join(ABS_PATH, "..", "..", "data", "raw", "test_dataset_query_df.csv"),
        index=False,
    )


if __name__ == "__main__":
    generate()
