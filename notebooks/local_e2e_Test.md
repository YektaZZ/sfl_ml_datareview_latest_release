

```python
import sys
import pandas as pd
sys.path.append('../src')

from preprocess import make_record_collection
```


```python
make_record_collection("../data/raw/fake_audit.csv", 
                       "../data/raw/fake_query.csv", 
                       "../data/processed/test_records.pkl")
```


```python
from train import train_model
```


```python
train_model("../data/processed/test_records.pkl", 
            "../data/raw/fake_metadata.csv", 
            "../data/processed/fake_model.h5", 
            "../data/processed/test_encoder.pkl")
```


```python
from infer import identify_queries
```


```python
identify_queries("../data/processed/test_records.pkl", 
            "../data/raw/fake_metadata.csv", 
            "../data/processed/fake_model.h5", 
            "../data/processed/test_encoder.pkl")
```
