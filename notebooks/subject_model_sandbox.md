
# Subject Model Sandbox

This notebook provides an example for how to use the SubjectModel outside of the AI Architecture.  The saved cell outputs use Clintek 21 (a muscular dystrophy trial) as an example. First, we need to import the source code into our notebook:


```python
import sys
import pandas as pd
sys.path.append('../src/classes')

from recordcollection import RecordCollection
from dataset import AuditDataset, QueryDataset
from ordinalencodercollection import OrdinalEncoderCollection
from model import SubjectModel
from datageneratorfactory import DataGeneratorFactory
from datawindow import DataWindow
```

    [nltk_data] Downloading package stopwords to
    [nltk_data]     /home/ec2-user/nltk_data...
    [nltk_data]   Package stopwords is already up-to-date!


Next, paths to the Audit, Query, and Metadata are specified.  The recommended location to save data is the `data/raw` directory of the repository.


```python
path_to_audit = "../data/raw/c21_audit.csv"
path_to_query = "../data/raw/c21_query.csv"
path_to_metadata = "../data/raw/c21_metadata.csv"
```

With the relative paths defined, the Audit and Query data can be loaded into a `Dataset` object.  The `Dataset` is a class wrapper around a `pandas DataFrame`, the additional methods filter out irrelevant audit data (such as "Verify" AuditSubCategoryType events, etc.). The `Dataset` object is designed to interface with the `RecordCollection` object.


```python
audit = AuditDataset(path = path_to_audit) 
dates = audit.dataset.groupby("ItemGroupRecordId").DateTimeStamp.min()
```

The above code computes the creation date of all the Audit data, and the data input timeline is shown below


```python
import matplotlib.pyplot as plt

def sfl_defaults():
	plt.style.use('classic')
	plt.rcParams['figure.figsize'] = [8.0, 5.0]
	plt.rcParams['figure.facecolor']='w'

	# text size
	plt.rcParams['xtick.labelsize']=14
	plt.rcParams['ytick.labelsize']=14
	plt.rcParams['axes.labelsize']=15
	plt.rcParams['axes.titlesize']=16
	plt.rcParams['legend.fontsize']=12

	# grids
	plt.rcParams['grid.color'] = 'k'
	plt.rcParams['grid.linestyle'] = ':'
	plt.rcParams['grid.linewidth'] = 0.5

	# 
	print('SFL style loaded...')
    
sfl_defaults()
dates = pd.to_datetime(dates)
dates.hist(bins=20)
plt.xticks(rotation=45)
plt.xlabel("Date")
plt.ylabel("New Records")
plt.title("Clintek 21: DMD")
```

    SFL style loaded...





    Text(0.5, 1.0, 'Clintek 21: DMD')




![png](subject_model_sandbox_files/subject_model_sandbox_8_2.png)


Hyperparameters are loaded from the `configs.pkl` file:


```python
import pickle as dill
with open("configs.pkl", "rb") as file:
    load = dill.load(file)

study_name = "Clintek_21"
config = load[study_name]

num_layers = config["num_layers"]
num_epochs = config["num_epochs"]

first_train = pd.to_datetime(config["first_train"])
last_train = pd.to_datetime(config["last_train"])
num_trains = config["num_trains"]

# The num_runs variable is used to train the model multiple times per step, set to 1 to avoid repeated training
num_runs = 1

step = (last_train - first_train) / (num_trains - 1)
step
```




    Timedelta('48 days 06:51:25.714285')



The following code runs sequential model trainings, the time elapsed between model trainings is controlled by the `step` variable


```python
# Initialize empty lists for saving the RecordCollection, EncoderCollection, and Model objects
record_collections = []
encoder_collections = []
train_generators = []
validation_generators = []
models = []

# Initialize the first training date
train_date = first_train

for train_idx in range(num_trains):
    # Fetch the Audit and Query data
    train_audit = AuditDataset(path = path_to_audit)
    train_query = QueryDataset(path = path_to_query)
    
    # The filter_on_date method filters out rows of the Query and Audit tables that occur after the passed date
    train_audit.filter_on_date(str(train_date))
    train_query.filter_on_date(str(train_date))
    
    # Initialize a new RecordCollection
    train_record_collection = RecordCollection()
    
    # Generate the manifest (a table of record information)
    train_record_collection.make_manifest(train_audit)
    
    # Initialize empty records
    train_record_collection.initialize_records()
    
    # Populate the empty records with all available information
    train_record_collection.make_records(train_audit, train_query)
    
    # Add the train_record_collection to the record collection list
    record_collections.append(train_record_collection)
    
    print(train_date)
    
    # The get_modeling_paramters method returns a 3-tuple of information:
    #     0: a dictionary whose keys are ItemOIDs and whose values are sets of all values seen for that ItemOID
    #     1: a sorted list of the 128 most queried ItemOIDs
    #     2: a dictionary whose keys are the 128 most queried ItemOIDs and whose values are the datatype of the ItemOIDs (taken from the metadata)
    (itemoid_values, sorted_itemoids, sorted_itemoids_to_dtype) = train_record_collection.get_modeling_parameters(path_to_metadata)
    
    # Generate an encoder collection
    encoder_collection = OrdinalEncoderCollection(items_to_dtype=sorted_itemoids_to_dtype)
    
    # Fit the encoder, this assigns integer values to the data by tokenizing (text data) or binning (datetime and numerical data)
    encoder_collection.fit(itemoid_values)
    
    # Add the new encoder collection to the list
    encoder_collections.append(encoder_collection)
    
    # Define the model parameters
    window_size = 20
    split_date = train_date - pd.Timedelta("30d")
    split_date = str(split_date)

    # The DataGenerator object creates the subject snapshots and splits the data using the split date and randomly selects 30% of subjects for a temporal & subject holdout
    generator_factory = DataGeneratorFactory(train_record_collection, encoder_collection, sorted_itemoids, split_date=split_date,subject_split=.3)
    
    # Create a dictionary whose keys are ItemOIDs and whose values are the number of tokens/bins, this is used to create the embedding vocabularies
    feature_dict = generator_factory.count_feature_values()
    train_generator, validation_generator = generator_factory.make_data_generators()
    
    train_generators.append(train_generator)
    validation_generators.append(validation_generator)
    
    temp_model_list = []
    
    for run_idx in range(num_runs):
        print(f"Executing {train_idx+1, run_idx+1}")
        # Instantiate a model object and build the model
        model = SubjectModel()
        model.build_model(feature_dict, base_filters=num_layers)

        # Train the model
        model.train(train_generator, validation_generator=validation_generator, epochs=num_epochs)
        temp_model_list.append(model)
        
    # Add the model to the list
    models.append(temp_model_list)

    # Advance the train date
    train_date += step
```

    2019-06-05 15:07:29
    10
    Executing (1, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 28 steps, validate for 10 steps
    Epoch 1/20
    28/28 - 28s - loss: 0.3402 - accuracy: 0.8812 - recall: 0.4397 - precision: 0.2228 - f1_metric: 0.4075 - val_loss: 0.3454 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 2/20
    28/28 - 4s - loss: 0.0930 - accuracy: 0.9682 - recall: 0.5823 - precision: 0.8022 - f1_metric: 0.6674 - val_loss: 0.2452 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 3/20
    28/28 - 4s - loss: 0.0714 - accuracy: 0.9765 - recall: 0.6907 - precision: 0.8690 - f1_metric: 0.7674 - val_loss: 0.2030 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 4/20
    28/28 - 4s - loss: 0.0579 - accuracy: 0.9815 - recall: 0.7462 - precision: 0.9111 - f1_metric: 0.8180 - val_loss: 0.1961 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 5/20
    28/28 - 4s - loss: 0.0473 - accuracy: 0.9851 - recall: 0.7942 - precision: 0.9339 - f1_metric: 0.8573 - val_loss: 0.1901 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 6/20
    28/28 - 4s - loss: 0.0403 - accuracy: 0.9873 - recall: 0.8255 - precision: 0.9431 - f1_metric: 0.8801 - val_loss: 0.1862 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 7/20
    28/28 - 4s - loss: 0.0351 - accuracy: 0.9888 - recall: 0.8457 - precision: 0.9517 - f1_metric: 0.8954 - val_loss: 0.1857 - val_accuracy: 0.9522 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 8/20
    28/28 - 4s - loss: 0.0323 - accuracy: 0.9894 - recall: 0.8562 - precision: 0.9512 - f1_metric: 0.9010 - val_loss: 0.2046 - val_accuracy: 0.9515 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 9/20
    28/28 - 4s - loss: 0.0288 - accuracy: 0.9905 - recall: 0.8723 - precision: 0.9556 - f1_metric: 0.9118 - val_loss: 0.1936 - val_accuracy: 0.9519 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 10/20
    28/28 - 4s - loss: 0.0273 - accuracy: 0.9909 - recall: 0.8819 - precision: 0.9544 - f1_metric: 0.9165 - val_loss: 0.2180 - val_accuracy: 0.9160 - val_recall: 0.0000e+00 - val_precision: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 11/20
    28/28 - 4s - loss: 0.0243 - accuracy: 0.9916 - recall: 0.8909 - precision: 0.9579 - f1_metric: 0.9229 - val_loss: 0.2254 - val_accuracy: 0.9092 - val_recall: 0.0298 - val_precision: 0.0311 - val_f1_metric: 0.0305
    Epoch 12/20
    28/28 - 4s - loss: 0.0238 - accuracy: 0.9916 - recall: 0.8935 - precision: 0.9555 - f1_metric: 0.9234 - val_loss: 0.3357 - val_accuracy: 0.8753 - val_recall: 0.1827 - val_precision: 0.0926 - val_f1_metric: 0.1229
    Epoch 13/20
    28/28 - 4s - loss: 0.0208 - accuracy: 0.9926 - recall: 0.9078 - precision: 0.9598 - f1_metric: 0.9329 - val_loss: 0.3092 - val_accuracy: 0.8795 - val_recall: 0.2178 - val_precision: 0.1115 - val_f1_metric: 0.1474
    Epoch 14/20
    28/28 - 4s - loss: 0.0197 - accuracy: 0.9929 - recall: 0.9116 - precision: 0.9607 - f1_metric: 0.9354 - val_loss: 0.3510 - val_accuracy: 0.8593 - val_recall: 0.2731 - val_precision: 0.1098 - val_f1_metric: 0.1564
    Epoch 15/20
    28/28 - 4s - loss: 0.0199 - accuracy: 0.9927 - recall: 0.9140 - precision: 0.9553 - f1_metric: 0.9343 - val_loss: 0.4063 - val_accuracy: 0.8259 - val_recall: 0.4548 - val_precision: 0.1282 - val_f1_metric: 0.1997
    Epoch 16/20
    28/28 - 4s - loss: 0.0175 - accuracy: 0.9934 - recall: 0.9194 - precision: 0.9624 - f1_metric: 0.9403 - val_loss: 0.3532 - val_accuracy: 0.8529 - val_recall: 0.5246 - val_precision: 0.1679 - val_f1_metric: 0.2541
    Epoch 17/20
    28/28 - 4s - loss: 0.0168 - accuracy: 0.9936 - recall: 0.9259 - precision: 0.9607 - f1_metric: 0.9429 - val_loss: 0.2327 - val_accuracy: 0.9151 - val_recall: 0.4479 - val_precision: 0.2681 - val_f1_metric: 0.3350
    Epoch 18/20
    28/28 - 4s - loss: 0.0160 - accuracy: 0.9939 - recall: 0.9294 - precision: 0.9615 - f1_metric: 0.9452 - val_loss: 0.2090 - val_accuracy: 0.9274 - val_recall: 0.5029 - val_precision: 0.3303 - val_f1_metric: 0.3977
    Epoch 19/20
    28/28 - 4s - loss: 0.0152 - accuracy: 0.9941 - recall: 0.9324 - precision: 0.9618 - f1_metric: 0.9468 - val_loss: 0.1686 - val_accuracy: 0.9534 - val_recall: 0.3747 - val_precision: 0.5179 - val_f1_metric: 0.4331
    Epoch 20/20
    28/28 - 4s - loss: 0.0150 - accuracy: 0.9941 - recall: 0.9330 - precision: 0.9613 - f1_metric: 0.9467 - val_loss: 0.1551 - val_accuracy: 0.9570 - val_recall: 0.5067 - val_precision: 0.5551 - val_f1_metric: 0.5289
    2019-07-23 21:37:41.285714285
    13
    Executing (2, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 34 steps, validate for 10 steps
    Epoch 1/20
    34/34 - 26s - loss: 0.1720 - accuracy: 0.9368 - recall_1: 0.4538 - precision_1: 0.5044 - f1_metric: 0.5043 - val_loss: 0.5300 - val_accuracy: 0.9264 - val_recall_1: 0.0000e+00 - val_precision_1: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 2/20
    34/34 - 5s - loss: 0.0901 - accuracy: 0.9688 - recall_1: 0.6389 - precision_1: 0.8333 - f1_metric: 0.7187 - val_loss: 0.5949 - val_accuracy: 0.9090 - val_recall_1: 0.1801 - val_precision_1: 0.3019 - val_f1_metric: 0.2257
    Epoch 3/20
    34/34 - 5s - loss: 0.0701 - accuracy: 0.9765 - recall_1: 0.7270 - precision_1: 0.8844 - f1_metric: 0.7964 - val_loss: 0.5455 - val_accuracy: 0.9047 - val_recall_1: 0.2434 - val_precision_1: 0.3115 - val_f1_metric: 0.2732
    Epoch 4/20
    34/34 - 5s - loss: 0.0550 - accuracy: 0.9818 - recall_1: 0.7844 - precision_1: 0.9180 - f1_metric: 0.8452 - val_loss: 0.5878 - val_accuracy: 0.8462 - val_recall_1: 0.3645 - val_precision_1: 0.2003 - val_f1_metric: 0.2584
    Epoch 5/20
    34/34 - 5s - loss: 0.0463 - accuracy: 0.9847 - recall_1: 0.8196 - precision_1: 0.9320 - f1_metric: 0.8719 - val_loss: 0.7858 - val_accuracy: 0.3068 - val_recall_1: 0.7245 - val_precision_1: 0.0734 - val_f1_metric: 0.1333
    Epoch 6/20
    34/34 - 5s - loss: 0.0418 - accuracy: 0.9861 - recall_1: 0.8386 - precision_1: 0.9369 - f1_metric: 0.8849 - val_loss: 0.6104 - val_accuracy: 0.5878 - val_recall_1: 0.5932 - val_precision_1: 0.1025 - val_f1_metric: 0.1747
    Epoch 7/20
    34/34 - 5s - loss: 0.0352 - accuracy: 0.9882 - recall_1: 0.8610 - precision_1: 0.9485 - f1_metric: 0.9024 - val_loss: 1.0245 - val_accuracy: 0.3057 - val_recall_1: 0.9179 - val_precision_1: 0.0894 - val_f1_metric: 0.1628
    Epoch 8/20
    34/34 - 5s - loss: 0.0328 - accuracy: 0.9886 - recall_1: 0.8688 - precision_1: 0.9484 - f1_metric: 0.9065 - val_loss: 1.1350 - val_accuracy: 0.3102 - val_recall_1: 0.8650 - val_precision_1: 0.0856 - val_f1_metric: 0.1557
    Epoch 9/20
    34/34 - 5s - loss: 0.0300 - accuracy: 0.9893 - recall_1: 0.8792 - precision_1: 0.9495 - f1_metric: 0.9129 - val_loss: 0.9875 - val_accuracy: 0.4282 - val_recall_1: 0.8810 - val_precision_1: 0.1033 - val_f1_metric: 0.1848
    Epoch 10/20
    34/34 - 5s - loss: 0.0275 - accuracy: 0.9900 - recall_1: 0.8897 - precision_1: 0.9508 - f1_metric: 0.9189 - val_loss: 0.6221 - val_accuracy: 0.6666 - val_recall_1: 0.6726 - val_precision_1: 0.1379 - val_f1_metric: 0.2288
    Epoch 11/20
    34/34 - 5s - loss: 0.0264 - accuracy: 0.9903 - recall_1: 0.8921 - precision_1: 0.9523 - f1_metric: 0.9209 - val_loss: 0.7573 - val_accuracy: 0.5855 - val_recall_1: 0.6891 - val_precision_1: 0.1147 - val_f1_metric: 0.1965
    Epoch 12/20
    34/34 - 5s - loss: 0.0244 - accuracy: 0.9908 - recall_1: 0.9004 - precision_1: 0.9532 - f1_metric: 0.9256 - val_loss: 0.6152 - val_accuracy: 0.6956 - val_recall_1: 0.6516 - val_precision_1: 0.1468 - val_f1_metric: 0.2394
    Epoch 13/20
    34/34 - 5s - loss: 0.0227 - accuracy: 0.9914 - recall_1: 0.9071 - precision_1: 0.9552 - f1_metric: 0.9304 - val_loss: 0.7760 - val_accuracy: 0.6672 - val_recall_1: 0.6971 - val_precision_1: 0.1418 - val_f1_metric: 0.2355
    Epoch 14/20
    34/34 - 5s - loss: 0.0216 - accuracy: 0.9917 - recall_1: 0.9110 - precision_1: 0.9568 - f1_metric: 0.9332 - val_loss: 0.8489 - val_accuracy: 0.6805 - val_recall_1: 0.7842 - val_precision_1: 0.1597 - val_f1_metric: 0.2651
    Epoch 15/20
    34/34 - 5s - loss: 0.0209 - accuracy: 0.9919 - recall_1: 0.9149 - precision_1: 0.9555 - f1_metric: 0.9346 - val_loss: 0.3540 - val_accuracy: 0.8703 - val_recall_1: 0.7117 - val_precision_1: 0.3256 - val_f1_metric: 0.4463
    Epoch 16/20
    34/34 - 5s - loss: 0.0191 - accuracy: 0.9925 - recall_1: 0.9210 - precision_1: 0.9591 - f1_metric: 0.9395 - val_loss: 0.3149 - val_accuracy: 0.8925 - val_recall_1: 0.7169 - val_precision_1: 0.3783 - val_f1_metric: 0.4948
    Epoch 17/20
    34/34 - 5s - loss: 0.0188 - accuracy: 0.9925 - recall_1: 0.9228 - precision_1: 0.9576 - f1_metric: 0.9399 - val_loss: 0.1979 - val_accuracy: 0.9341 - val_recall_1: 0.6621 - val_precision_1: 0.5428 - val_f1_metric: 0.5964
    Epoch 18/20
    34/34 - 5s - loss: 0.0187 - accuracy: 0.9926 - recall_1: 0.9245 - precision_1: 0.9573 - f1_metric: 0.9404 - val_loss: 0.2088 - val_accuracy: 0.9350 - val_recall_1: 0.7676 - val_precision_1: 0.5410 - val_f1_metric: 0.6347
    Epoch 19/20
    34/34 - 5s - loss: 0.0172 - accuracy: 0.9931 - recall_1: 0.9306 - precision_1: 0.9600 - f1_metric: 0.9449 - val_loss: 0.1479 - val_accuracy: 0.9603 - val_recall_1: 0.6735 - val_precision_1: 0.7597 - val_f1_metric: 0.7139
    Epoch 20/20
    34/34 - 5s - loss: 0.0168 - accuracy: 0.9931 - recall_1: 0.9307 - precision_1: 0.9605 - f1_metric: 0.9452 - val_loss: 0.1833 - val_accuracy: 0.9518 - val_recall_1: 0.6072 - val_precision_1: 0.6980 - val_f1_metric: 0.6489
    2019-09-10 04:07:53.571428570
    14
    Executing (3, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 41 steps, validate for 10 steps
    Epoch 1/20
    41/41 - 27s - loss: 0.2651 - accuracy: 0.9176 - recall_2: 0.4770 - precision_2: 0.4899 - f1_metric: 0.5066 - val_loss: 0.3544 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 2/20
    41/41 - 6s - loss: 0.1016 - accuracy: 0.9645 - recall_2: 0.6971 - precision_2: 0.8367 - f1_metric: 0.7569 - val_loss: 0.3311 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 3/20
    41/41 - 6s - loss: 0.0751 - accuracy: 0.9747 - recall_2: 0.7767 - precision_2: 0.8965 - f1_metric: 0.8313 - val_loss: 0.3811 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 4/20
    41/41 - 6s - loss: 0.0604 - accuracy: 0.9801 - recall_2: 0.8245 - precision_2: 0.9210 - f1_metric: 0.8698 - val_loss: 0.4337 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 5/20
    41/41 - 6s - loss: 0.0532 - accuracy: 0.9821 - recall_2: 0.8419 - precision_2: 0.9303 - f1_metric: 0.8836 - val_loss: 0.4741 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 6/20
    41/41 - 6s - loss: 0.0451 - accuracy: 0.9848 - recall_2: 0.8666 - precision_2: 0.9405 - f1_metric: 0.9018 - val_loss: 0.5277 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 7/20
    41/41 - 6s - loss: 0.0416 - accuracy: 0.9855 - recall_2: 0.8720 - precision_2: 0.9441 - f1_metric: 0.9064 - val_loss: 0.5690 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 8/20
    41/41 - 6s - loss: 0.0360 - accuracy: 0.9874 - recall_2: 0.8898 - precision_2: 0.9509 - f1_metric: 0.9193 - val_loss: 0.5910 - val_accuracy: 0.9066 - val_recall_2: 0.0000e+00 - val_precision_2: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 9/20
    41/41 - 6s - loss: 0.0343 - accuracy: 0.9876 - recall_2: 0.8933 - precision_2: 0.9500 - f1_metric: 0.9207 - val_loss: 0.6197 - val_accuracy: 0.9063 - val_recall_2: 9.1225e-04 - val_precision_2: 0.2057 - val_f1_metric: 0.0018
    Epoch 10/20
    41/41 - 6s - loss: 0.0323 - accuracy: 0.9882 - recall_2: 0.8991 - precision_2: 0.9516 - f1_metric: 0.9245 - val_loss: 0.5630 - val_accuracy: 0.9083 - val_recall_2: 0.0262 - val_precision_2: 0.7633 - val_f1_metric: 0.0500
    Epoch 11/20
    41/41 - 6s - loss: 0.0298 - accuracy: 0.9889 - recall_2: 0.9070 - precision_2: 0.9530 - f1_metric: 0.9294 - val_loss: 0.5993 - val_accuracy: 0.9089 - val_recall_2: 0.0361 - val_precision_2: 0.7581 - val_f1_metric: 0.0683
    Epoch 12/20
    41/41 - 6s - loss: 0.0276 - accuracy: 0.9894 - recall_2: 0.9122 - precision_2: 0.9541 - f1_metric: 0.9326 - val_loss: 0.4139 - val_accuracy: 0.9134 - val_recall_2: 0.0902 - val_precision_2: 0.8387 - val_f1_metric: 0.1620
    Epoch 13/20
    41/41 - 6s - loss: 0.0261 - accuracy: 0.9898 - recall_2: 0.9168 - precision_2: 0.9549 - f1_metric: 0.9353 - val_loss: 0.3612 - val_accuracy: 0.9212 - val_recall_2: 0.1894 - val_precision_2: 0.8514 - val_f1_metric: 0.3099
    Epoch 14/20
    41/41 - 6s - loss: 0.0253 - accuracy: 0.9900 - recall_2: 0.9203 - precision_2: 0.9541 - f1_metric: 0.9369 - val_loss: 0.2179 - val_accuracy: 0.9432 - val_recall_2: 0.4180 - val_precision_2: 0.9414 - val_f1_metric: 0.5791
    Epoch 15/20
    41/41 - 6s - loss: 0.0235 - accuracy: 0.9904 - recall_2: 0.9233 - precision_2: 0.9563 - f1_metric: 0.9394 - val_loss: 0.1891 - val_accuracy: 0.9487 - val_recall_2: 0.5088 - val_precision_2: 0.8983 - val_f1_metric: 0.6503
    Epoch 16/20
    41/41 - 6s - loss: 0.0225 - accuracy: 0.9908 - recall_2: 0.9277 - precision_2: 0.9568 - f1_metric: 0.9420 - val_loss: 0.1347 - val_accuracy: 0.9631 - val_recall_2: 0.6931 - val_precision_2: 0.8875 - val_f1_metric: 0.7788
    Epoch 17/20
    41/41 - 6s - loss: 0.0215 - accuracy: 0.9911 - recall_2: 0.9303 - precision_2: 0.9581 - f1_metric: 0.9439 - val_loss: 0.1206 - val_accuracy: 0.9678 - val_recall_2: 0.7703 - val_precision_2: 0.8704 - val_f1_metric: 0.8178
    Epoch 18/20
    41/41 - 6s - loss: 0.0209 - accuracy: 0.9912 - recall_2: 0.9319 - precision_2: 0.9576 - f1_metric: 0.9446 - val_loss: 0.1206 - val_accuracy: 0.9683 - val_recall_2: 0.8007 - val_precision_2: 0.8516 - val_f1_metric: 0.8258
    Epoch 19/20
    41/41 - 6s - loss: 0.0200 - accuracy: 0.9914 - recall_2: 0.9336 - precision_2: 0.9591 - f1_metric: 0.9461 - val_loss: 0.1378 - val_accuracy: 0.9672 - val_recall_2: 0.8468 - val_precision_2: 0.8107 - val_f1_metric: 0.8289
    Epoch 20/20
    41/41 - 6s - loss: 0.0201 - accuracy: 0.9914 - recall_2: 0.9341 - precision_2: 0.9579 - f1_metric: 0.9458 - val_loss: 0.1361 - val_accuracy: 0.9658 - val_recall_2: 0.8323 - val_precision_2: 0.8074 - val_f1_metric: 0.8201
    2019-10-28 10:38:05.857142855
    18
    Executing (4, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 48 steps, validate for 10 steps
    Epoch 1/20
    48/48 - 29s - loss: 0.2347 - accuracy: 0.9077 - recall_3: 0.5338 - precision_3: 0.4523 - f1_metric: 0.5440 - val_loss: 0.2814 - val_accuracy: 0.9415 - val_recall_3: 0.0000e+00 - val_precision_3: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 2/20
    48/48 - 7s - loss: 0.0930 - accuracy: 0.9673 - recall_3: 0.7176 - precision_3: 0.8658 - f1_metric: 0.7836 - val_loss: 0.2226 - val_accuracy: 0.9415 - val_recall_3: 0.0000e+00 - val_precision_3: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 3/20
    48/48 - 7s - loss: 0.0698 - accuracy: 0.9764 - recall_3: 0.7972 - precision_3: 0.9067 - f1_metric: 0.8482 - val_loss: 0.2439 - val_accuracy: 0.9415 - val_recall_3: 0.0000e+00 - val_precision_3: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 4/20
    48/48 - 7s - loss: 0.0570 - accuracy: 0.9808 - recall_3: 0.8362 - precision_3: 0.9256 - f1_metric: 0.8783 - val_loss: 0.2811 - val_accuracy: 0.9415 - val_recall_3: 0.0000e+00 - val_precision_3: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 5/20
    48/48 - 7s - loss: 0.0485 - accuracy: 0.9833 - recall_3: 0.8552 - precision_3: 0.9385 - f1_metric: 0.8949 - val_loss: 0.3308 - val_accuracy: 0.9415 - val_recall_3: 0.0000e+00 - val_precision_3: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 6/20
    48/48 - 7s - loss: 0.0435 - accuracy: 0.9846 - recall_3: 0.8680 - precision_3: 0.9422 - f1_metric: 0.9034 - val_loss: 0.3540 - val_accuracy: 0.9415 - val_recall_3: 0.0000e+00 - val_precision_3: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 7/20
    48/48 - 7s - loss: 0.0389 - accuracy: 0.9860 - recall_3: 0.8821 - precision_3: 0.9460 - f1_metric: 0.9129 - val_loss: 0.3563 - val_accuracy: 0.9414 - val_recall_3: 0.0010 - val_precision_3: 0.2895 - val_f1_metric: 0.0020
    Epoch 8/20
    48/48 - 7s - loss: 0.0347 - accuracy: 0.9872 - recall_3: 0.8937 - precision_3: 0.9497 - f1_metric: 0.9207 - val_loss: 0.3388 - val_accuracy: 0.9414 - val_recall_3: 6.6764e-04 - val_precision_3: 0.2656 - val_f1_metric: 0.0014
    Epoch 9/20
    48/48 - 7s - loss: 0.0317 - accuracy: 0.9881 - recall_3: 0.9023 - precision_3: 0.9518 - f1_metric: 0.9263 - val_loss: 0.3274 - val_accuracy: 0.9428 - val_recall_3: 0.0846 - val_precision_3: 0.5744 - val_f1_metric: 0.1479
    Epoch 10/20
    48/48 - 7s - loss: 0.0306 - accuracy: 0.9883 - recall_3: 0.9065 - precision_3: 0.9502 - f1_metric: 0.9279 - val_loss: 0.2473 - val_accuracy: 0.9419 - val_recall_3: 0.1544 - val_precision_3: 0.5118 - val_f1_metric: 0.2375
    Epoch 11/20
    48/48 - 7s - loss: 0.0270 - accuracy: 0.9894 - recall_3: 0.9163 - precision_3: 0.9542 - f1_metric: 0.9348 - val_loss: 0.2395 - val_accuracy: 0.9422 - val_recall_3: 0.2546 - val_precision_3: 0.5127 - val_f1_metric: 0.3401
    Epoch 12/20
    48/48 - 7s - loss: 0.0260 - accuracy: 0.9896 - recall_3: 0.9190 - precision_3: 0.9544 - f1_metric: 0.9363 - val_loss: 0.1506 - val_accuracy: 0.9562 - val_recall_3: 0.5665 - val_precision_3: 0.6425 - val_f1_metric: 0.6018
    Epoch 13/20
    48/48 - 7s - loss: 0.0245 - accuracy: 0.9900 - recall_3: 0.9229 - precision_3: 0.9552 - f1_metric: 0.9387 - val_loss: 0.1415 - val_accuracy: 0.9644 - val_recall_3: 0.6863 - val_precision_3: 0.6993 - val_f1_metric: 0.6926
    Epoch 14/20
    48/48 - 7s - loss: 0.0241 - accuracy: 0.9901 - recall_3: 0.9241 - precision_3: 0.9552 - f1_metric: 0.9393 - val_loss: 0.4126 - val_accuracy: 0.9304 - val_recall_3: 0.8170 - val_precision_3: 0.4479 - val_f1_metric: 0.5776
    Epoch 15/20
    48/48 - 7s - loss: 0.0222 - accuracy: 0.9907 - recall_3: 0.9294 - precision_3: 0.9574 - f1_metric: 0.9432 - val_loss: 0.1841 - val_accuracy: 0.9622 - val_recall_3: 0.7885 - val_precision_3: 0.6446 - val_f1_metric: 0.7088
    Epoch 16/20
    48/48 - 7s - loss: 0.0217 - accuracy: 0.9908 - recall_3: 0.9304 - precision_3: 0.9578 - f1_metric: 0.9439 - val_loss: 0.2082 - val_accuracy: 0.9614 - val_recall_3: 0.8164 - val_precision_3: 0.6316 - val_f1_metric: 0.7115
    Epoch 17/20
    48/48 - 7s - loss: 0.0203 - accuracy: 0.9912 - recall_3: 0.9338 - precision_3: 0.9592 - f1_metric: 0.9464 - val_loss: 0.1337 - val_accuracy: 0.9692 - val_recall_3: 0.7655 - val_precision_3: 0.7245 - val_f1_metric: 0.7442
    Epoch 18/20
    48/48 - 7s - loss: 0.0202 - accuracy: 0.9912 - recall_3: 0.9341 - precision_3: 0.9589 - f1_metric: 0.9463 - val_loss: 0.1372 - val_accuracy: 0.9714 - val_recall_3: 0.7976 - val_precision_3: 0.7357 - val_f1_metric: 0.7650
    Epoch 19/20
    48/48 - 7s - loss: 0.0200 - accuracy: 0.9913 - recall_3: 0.9352 - precision_3: 0.9584 - f1_metric: 0.9467 - val_loss: 0.1242 - val_accuracy: 0.9749 - val_recall_3: 0.7727 - val_precision_3: 0.7937 - val_f1_metric: 0.7830
    Epoch 20/20
    48/48 - 7s - loss: 0.0190 - accuracy: 0.9916 - recall_3: 0.9377 - precision_3: 0.9602 - f1_metric: 0.9488 - val_loss: 0.1389 - val_accuracy: 0.9730 - val_recall_3: 0.7987 - val_precision_3: 0.7542 - val_f1_metric: 0.7754
    2019-12-15 17:08:18.142857140
    23
    Executing (5, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 56 steps, validate for 10 steps
    Epoch 1/20
    56/56 - 29s - loss: 0.2475 - accuracy: 0.9102 - recall_4: 0.5110 - precision_4: 0.4688 - f1_metric: 0.5229 - val_loss: 0.4628 - val_accuracy: 0.9012 - val_recall_4: 0.0072 - val_precision_4: 0.0098 - val_f1_metric: 0.0083
    Epoch 2/20
    56/56 - 8s - loss: 0.0988 - accuracy: 0.9642 - recall_4: 0.7028 - precision_4: 0.8451 - f1_metric: 0.7641 - val_loss: 0.3458 - val_accuracy: 0.9012 - val_recall_4: 0.0072 - val_precision_4: 0.0098 - val_f1_metric: 0.0083
    Epoch 3/20
    56/56 - 8s - loss: 0.0692 - accuracy: 0.9761 - recall_4: 0.8010 - precision_4: 0.9039 - f1_metric: 0.8488 - val_loss: 0.2638 - val_accuracy: 0.9407 - val_recall_4: 6.3378e-04 - val_precision_4: 0.0189 - val_f1_metric: 0.0012
    Epoch 4/20
    56/56 - 8s - loss: 0.0553 - accuracy: 0.9808 - recall_4: 0.8403 - precision_4: 0.9247 - f1_metric: 0.8802 - val_loss: 0.2538 - val_accuracy: 0.9421 - val_recall_4: 1.2859e-04 - val_precision_4: 0.0185 - val_f1_metric: 2.5878e-04
    Epoch 5/20
    56/56 - 8s - loss: 0.0480 - accuracy: 0.9832 - recall_4: 0.8599 - precision_4: 0.9354 - f1_metric: 0.8960 - val_loss: 0.3097 - val_accuracy: 0.9424 - val_recall_4: 0.0000e+00 - val_precision_4: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 6/20
    56/56 - 8s - loss: 0.0422 - accuracy: 0.9851 - recall_4: 0.8765 - precision_4: 0.9422 - f1_metric: 0.9080 - val_loss: 0.3921 - val_accuracy: 0.9425 - val_recall_4: 0.0000e+00 - val_precision_4: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 7/20
    56/56 - 8s - loss: 0.0387 - accuracy: 0.9859 - recall_4: 0.8841 - precision_4: 0.9449 - f1_metric: 0.9134 - val_loss: 0.4608 - val_accuracy: 0.9425 - val_recall_4: 0.0000e+00 - val_precision_4: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 8/20
    56/56 - 8s - loss: 0.0350 - accuracy: 0.9870 - recall_4: 0.8937 - precision_4: 0.9493 - f1_metric: 0.9205 - val_loss: 0.4056 - val_accuracy: 0.9425 - val_recall_4: 0.0032 - val_precision_4: 0.4444 - val_f1_metric: 0.0065
    Epoch 9/20
    56/56 - 8s - loss: 0.0327 - accuracy: 0.9875 - recall_4: 0.8988 - precision_4: 0.9496 - f1_metric: 0.9235 - val_loss: 0.3164 - val_accuracy: 0.9450 - val_recall_4: 0.0572 - val_precision_4: 0.8064 - val_f1_metric: 0.1073
    Epoch 10/20
    56/56 - 8s - loss: 0.0309 - accuracy: 0.9880 - recall_4: 0.9042 - precision_4: 0.9506 - f1_metric: 0.9269 - val_loss: 0.2058 - val_accuracy: 0.9537 - val_recall_4: 0.2204 - val_precision_4: 0.8945 - val_f1_metric: 0.3539
    Epoch 11/20
    56/56 - 8s - loss: 0.0293 - accuracy: 0.9884 - recall_4: 0.9083 - precision_4: 0.9518 - f1_metric: 0.9294 - val_loss: 0.1248 - val_accuracy: 0.9675 - val_recall_4: 0.5105 - val_precision_4: 0.8716 - val_f1_metric: 0.6441
    Epoch 12/20
    56/56 - 8s - loss: 0.0268 - accuracy: 0.9892 - recall_4: 0.9150 - precision_4: 0.9542 - f1_metric: 0.9341 - val_loss: 0.1098 - val_accuracy: 0.9725 - val_recall_4: 0.6178 - val_precision_4: 0.8660 - val_f1_metric: 0.7209
    Epoch 13/20
    56/56 - 8s - loss: 0.0265 - accuracy: 0.9891 - recall_4: 0.9159 - precision_4: 0.9531 - f1_metric: 0.9341 - val_loss: 0.1178 - val_accuracy: 0.9699 - val_recall_4: 0.6487 - val_precision_4: 0.7908 - val_f1_metric: 0.7126
    Epoch 14/20
    56/56 - 8s - loss: 0.0253 - accuracy: 0.9895 - recall_4: 0.9195 - precision_4: 0.9534 - f1_metric: 0.9361 - val_loss: 0.1151 - val_accuracy: 0.9710 - val_recall_4: 0.7639 - val_precision_4: 0.7405 - val_f1_metric: 0.7522
    Epoch 15/20
    56/56 - 8s - loss: 0.0233 - accuracy: 0.9901 - recall_4: 0.9245 - precision_4: 0.9559 - f1_metric: 0.9398 - val_loss: 0.1129 - val_accuracy: 0.9749 - val_recall_4: 0.7295 - val_precision_4: 0.8138 - val_f1_metric: 0.7697
    Epoch 16/20
    56/56 - 8s - loss: 0.0231 - accuracy: 0.9902 - recall_4: 0.9254 - precision_4: 0.9562 - f1_metric: 0.9405 - val_loss: 0.1196 - val_accuracy: 0.9709 - val_recall_4: 0.7169 - val_precision_4: 0.7629 - val_f1_metric: 0.7396
    Epoch 17/20
    56/56 - 8s - loss: 0.0224 - accuracy: 0.9903 - recall_4: 0.9275 - precision_4: 0.9561 - f1_metric: 0.9415 - val_loss: 0.1161 - val_accuracy: 0.9719 - val_recall_4: 0.7716 - val_precision_4: 0.7482 - val_f1_metric: 0.7600
    Epoch 18/20
    56/56 - 8s - loss: 0.0220 - accuracy: 0.9904 - recall_4: 0.9288 - precision_4: 0.9559 - f1_metric: 0.9420 - val_loss: 0.1195 - val_accuracy: 0.9715 - val_recall_4: 0.7776 - val_precision_4: 0.7399 - val_f1_metric: 0.7585
    Epoch 19/20
    56/56 - 8s - loss: 0.0211 - accuracy: 0.9907 - recall_4: 0.9310 - precision_4: 0.9570 - f1_metric: 0.9438 - val_loss: 0.1206 - val_accuracy: 0.9710 - val_recall_4: 0.7892 - val_precision_4: 0.7287 - val_f1_metric: 0.7578
    Epoch 20/20
    56/56 - 8s - loss: 0.0203 - accuracy: 0.9909 - recall_4: 0.9340 - precision_4: 0.9571 - f1_metric: 0.9454 - val_loss: 0.1108 - val_accuracy: 0.9745 - val_recall_4: 0.7709 - val_precision_4: 0.7820 - val_f1_metric: 0.7765
    2020-02-01 23:38:30.428571425
    26
    Executing (6, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 65 steps, validate for 10 steps
    Epoch 1/20
    65/65 - 30s - loss: 0.1565 - accuracy: 0.9363 - recall_5: 0.5261 - precision_5: 0.6510 - f1_metric: 0.5909 - val_loss: 0.3146 - val_accuracy: 0.9439 - val_recall_5: 0.0000e+00 - val_precision_5: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 2/20
    65/65 - 9s - loss: 0.0835 - accuracy: 0.9701 - recall_5: 0.7515 - precision_5: 0.8763 - f1_metric: 0.8077 - val_loss: 0.2551 - val_accuracy: 0.9439 - val_recall_5: 0.0000e+00 - val_precision_5: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 3/20
    65/65 - 9s - loss: 0.0604 - accuracy: 0.9793 - recall_5: 0.8261 - precision_5: 0.9201 - f1_metric: 0.8702 - val_loss: 0.2654 - val_accuracy: 0.9439 - val_recall_5: 0.0000e+00 - val_precision_5: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 4/20
    65/65 - 9s - loss: 0.0493 - accuracy: 0.9831 - recall_5: 0.8568 - precision_5: 0.9379 - f1_metric: 0.8953 - val_loss: 0.3333 - val_accuracy: 0.9192 - val_recall_5: 0.0169 - val_precision_5: 0.0356 - val_f1_metric: 0.0230
    Epoch 5/20
    65/65 - 9s - loss: 0.0408 - accuracy: 0.9857 - recall_5: 0.8779 - precision_5: 0.9486 - f1_metric: 0.9118 - val_loss: 0.3692 - val_accuracy: 0.8572 - val_recall_5: 0.1221 - val_precision_5: 0.0683 - val_f1_metric: 0.0873
    Epoch 6/20
    65/65 - 9s - loss: 0.0366 - accuracy: 0.9868 - recall_5: 0.8885 - precision_5: 0.9514 - f1_metric: 0.9188 - val_loss: 0.3309 - val_accuracy: 0.8768 - val_recall_5: 0.2058 - val_precision_5: 0.1281 - val_f1_metric: 0.1575
    Epoch 7/20
    65/65 - 9s - loss: 0.0326 - accuracy: 0.9879 - recall_5: 0.8992 - precision_5: 0.9541 - f1_metric: 0.9259 - val_loss: 0.2188 - val_accuracy: 0.9390 - val_recall_5: 0.2206 - val_precision_5: 0.4168 - val_f1_metric: 0.2887
    Epoch 8/20
    65/65 - 9s - loss: 0.0299 - accuracy: 0.9886 - recall_5: 0.9070 - precision_5: 0.9551 - f1_metric: 0.9304 - val_loss: 0.1925 - val_accuracy: 0.9546 - val_recall_5: 0.3495 - val_precision_5: 0.6886 - val_f1_metric: 0.4642
    Epoch 9/20
    65/65 - 9s - loss: 0.0273 - accuracy: 0.9893 - recall_5: 0.9138 - precision_5: 0.9569 - f1_metric: 0.9348 - val_loss: 0.1568 - val_accuracy: 0.9650 - val_recall_5: 0.4626 - val_precision_5: 0.8437 - val_f1_metric: 0.5973
    Epoch 10/20
    65/65 - 9s - loss: 0.0265 - accuracy: 0.9894 - recall_5: 0.9170 - precision_5: 0.9556 - f1_metric: 0.9358 - val_loss: 0.1273 - val_accuracy: 0.9721 - val_recall_5: 0.6177 - val_precision_5: 0.8424 - val_f1_metric: 0.7120
    Epoch 11/20
    65/65 - 9s - loss: 0.0239 - accuracy: 0.9902 - recall_5: 0.9239 - precision_5: 0.9580 - f1_metric: 0.9406 - val_loss: 0.1220 - val_accuracy: 0.9746 - val_recall_5: 0.6668 - val_precision_5: 0.8471 - val_f1_metric: 0.7457
    Epoch 12/20
    65/65 - 9s - loss: 0.0235 - accuracy: 0.9902 - recall_5: 0.9252 - precision_5: 0.9573 - f1_metric: 0.9409 - val_loss: 0.1265 - val_accuracy: 0.9718 - val_recall_5: 0.7492 - val_precision_5: 0.7482 - val_f1_metric: 0.7476
    Epoch 13/20
    65/65 - 9s - loss: 0.0212 - accuracy: 0.9909 - recall_5: 0.9308 - precision_5: 0.9597 - f1_metric: 0.9450 - val_loss: 0.1266 - val_accuracy: 0.9731 - val_recall_5: 0.7198 - val_precision_5: 0.7829 - val_f1_metric: 0.7490
    Epoch 14/20
    65/65 - 9s - loss: 0.0209 - accuracy: 0.9909 - recall_5: 0.9323 - precision_5: 0.9589 - f1_metric: 0.9453 - val_loss: 0.1264 - val_accuracy: 0.9747 - val_recall_5: 0.7042 - val_precision_5: 0.8198 - val_f1_metric: 0.7565
    Epoch 15/20
    65/65 - 9s - loss: 0.0203 - accuracy: 0.9911 - recall_5: 0.9341 - precision_5: 0.9592 - f1_metric: 0.9464 - val_loss: 0.1280 - val_accuracy: 0.9744 - val_recall_5: 0.7306 - val_precision_5: 0.7956 - val_f1_metric: 0.7609
    Epoch 16/20
    65/65 - 9s - loss: 0.0194 - accuracy: 0.9913 - recall_5: 0.9362 - precision_5: 0.9601 - f1_metric: 0.9479 - val_loss: 0.1250 - val_accuracy: 0.9740 - val_recall_5: 0.7335 - val_precision_5: 0.7877 - val_f1_metric: 0.7587
    Epoch 17/20
    65/65 - 9s - loss: 0.0190 - accuracy: 0.9915 - recall_5: 0.9376 - precision_5: 0.9602 - f1_metric: 0.9487 - val_loss: 0.1306 - val_accuracy: 0.9753 - val_recall_5: 0.7439 - val_precision_5: 0.8015 - val_f1_metric: 0.7706
    Epoch 18/20
    65/65 - 9s - loss: 0.0183 - accuracy: 0.9916 - recall_5: 0.9387 - precision_5: 0.9607 - f1_metric: 0.9496 - val_loss: 0.1292 - val_accuracy: 0.9740 - val_recall_5: 0.7424 - val_precision_5: 0.7833 - val_f1_metric: 0.7610
    Epoch 19/20
    65/65 - 9s - loss: 0.0181 - accuracy: 0.9917 - recall_5: 0.9400 - precision_5: 0.9606 - f1_metric: 0.9502 - val_loss: 0.1292 - val_accuracy: 0.9756 - val_recall_5: 0.7435 - val_precision_5: 0.8061 - val_f1_metric: 0.7727
    Epoch 20/20
    65/65 - 9s - loss: 0.0171 - accuracy: 0.9920 - recall_5: 0.9427 - precision_5: 0.9616 - f1_metric: 0.9521 - val_loss: 0.1425 - val_accuracy: 0.9732 - val_recall_5: 0.7418 - val_precision_5: 0.7713 - val_f1_metric: 0.7553
    2020-03-21 06:08:42.714285710
    30
    Executing (7, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 76 steps, validate for 10 steps
    Epoch 1/20
    76/76 - 31s - loss: 0.1653 - accuracy: 0.9357 - recall_6: 0.4947 - precision_6: 0.6613 - f1_metric: 0.5582 - val_loss: 0.3135 - val_accuracy: 0.9476 - val_recall_6: 0.0000e+00 - val_precision_6: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 2/20
    76/76 - 10s - loss: 0.0924 - accuracy: 0.9662 - recall_6: 0.7234 - precision_6: 0.8555 - f1_metric: 0.7815 - val_loss: 0.2895 - val_accuracy: 0.9476 - val_recall_6: 0.0000e+00 - val_precision_6: 0.0000e+00 - val_f1_metric: 0.0000e+00
    Epoch 3/20
    76/76 - 10s - loss: 0.0656 - accuracy: 0.9772 - recall_6: 0.8130 - precision_6: 0.9076 - f1_metric: 0.8571 - val_loss: 0.3314 - val_accuracy: 0.9450 - val_recall_6: 0.0084 - val_precision_6: 0.1271 - val_f1_metric: 0.0156
    Epoch 4/20
    76/76 - 10s - loss: 0.0516 - accuracy: 0.9822 - recall_6: 0.8522 - precision_6: 0.9315 - f1_metric: 0.8898 - val_loss: 0.2529 - val_accuracy: 0.9458 - val_recall_6: 0.0068 - val_precision_6: 0.1438 - val_f1_metric: 0.0128
    Epoch 5/20
    76/76 - 11s - loss: 0.0449 - accuracy: 0.9844 - recall_6: 0.8704 - precision_6: 0.9404 - f1_metric: 0.9040 - val_loss: 0.2309 - val_accuracy: 0.9448 - val_recall_6: 0.0363 - val_precision_6: 0.2878 - val_f1_metric: 0.0642
    Epoch 6/20
    76/76 - 11s - loss: 0.0391 - accuracy: 0.9860 - recall_6: 0.8849 - precision_6: 0.9462 - f1_metric: 0.9144 - val_loss: 0.1846 - val_accuracy: 0.9503 - val_recall_6: 0.1453 - val_precision_6: 0.6110 - val_f1_metric: 0.2330
    Epoch 7/20
    76/76 - 11s - loss: 0.0351 - accuracy: 0.9871 - recall_6: 0.8965 - precision_6: 0.9488 - f1_metric: 0.9217 - val_loss: 0.1526 - val_accuracy: 0.9579 - val_recall_6: 0.3109 - val_precision_6: 0.7312 - val_f1_metric: 0.4347
    Epoch 8/20
    76/76 - 11s - loss: 0.0319 - accuracy: 0.9880 - recall_6: 0.9043 - precision_6: 0.9517 - f1_metric: 0.9273 - val_loss: 0.1151 - val_accuracy: 0.9654 - val_recall_6: 0.5134 - val_precision_6: 0.7485 - val_f1_metric: 0.6081
    Epoch 9/20
    76/76 - 11s - loss: 0.0293 - accuracy: 0.9886 - recall_6: 0.9113 - precision_6: 0.9527 - f1_metric: 0.9314 - val_loss: 0.1161 - val_accuracy: 0.9667 - val_recall_6: 0.6444 - val_precision_6: 0.6974 - val_f1_metric: 0.6692
    Epoch 10/20
    76/76 - 11s - loss: 0.0276 - accuracy: 0.9891 - recall_6: 0.9160 - precision_6: 0.9534 - f1_metric: 0.9342 - val_loss: 0.1207 - val_accuracy: 0.9702 - val_recall_6: 0.6358 - val_precision_6: 0.7571 - val_f1_metric: 0.6907
    Epoch 11/20
    76/76 - 11s - loss: 0.0257 - accuracy: 0.9896 - recall_6: 0.9212 - precision_6: 0.9543 - f1_metric: 0.9374 - val_loss: 0.1171 - val_accuracy: 0.9708 - val_recall_6: 0.7031 - val_precision_6: 0.7306 - val_f1_metric: 0.7161
    Epoch 12/20
    76/76 - 10s - loss: 0.0244 - accuracy: 0.9900 - recall_6: 0.9247 - precision_6: 0.9558 - f1_metric: 0.9400 - val_loss: 0.1175 - val_accuracy: 0.9710 - val_recall_6: 0.6906 - val_precision_6: 0.7397 - val_f1_metric: 0.7139
    Epoch 13/20
    76/76 - 10s - loss: 0.0231 - accuracy: 0.9903 - recall_6: 0.9281 - precision_6: 0.9566 - f1_metric: 0.9421 - val_loss: 0.1245 - val_accuracy: 0.9692 - val_recall_6: 0.6698 - val_precision_6: 0.7229 - val_f1_metric: 0.6949
    Epoch 14/20
    76/76 - 11s - loss: 0.0223 - accuracy: 0.9906 - recall_6: 0.9305 - precision_6: 0.9571 - f1_metric: 0.9436 - val_loss: 0.1185 - val_accuracy: 0.9712 - val_recall_6: 0.7300 - val_precision_6: 0.7231 - val_f1_metric: 0.7261
    Epoch 15/20
    76/76 - 11s - loss: 0.0212 - accuracy: 0.9909 - recall_6: 0.9334 - precision_6: 0.9579 - f1_metric: 0.9455 - val_loss: 0.1166 - val_accuracy: 0.9730 - val_recall_6: 0.6966 - val_precision_6: 0.7672 - val_f1_metric: 0.7299
    Epoch 16/20
    76/76 - 11s - loss: 0.0203 - accuracy: 0.9911 - recall_6: 0.9359 - precision_6: 0.9585 - f1_metric: 0.9470 - val_loss: 0.1253 - val_accuracy: 0.9721 - val_recall_6: 0.7416 - val_precision_6: 0.7304 - val_f1_metric: 0.7357
    Epoch 17/20
    76/76 - 11s - loss: 0.0200 - accuracy: 0.9912 - recall_6: 0.9369 - precision_6: 0.9584 - f1_metric: 0.9475 - val_loss: 0.1204 - val_accuracy: 0.9733 - val_recall_6: 0.7495 - val_precision_6: 0.7440 - val_f1_metric: 0.7465
    Epoch 18/20
    76/76 - 10s - loss: 0.0188 - accuracy: 0.9916 - recall_6: 0.9396 - precision_6: 0.9602 - f1_metric: 0.9498 - val_loss: 0.1254 - val_accuracy: 0.9742 - val_recall_6: 0.7121 - val_precision_6: 0.7764 - val_f1_metric: 0.7425
    Epoch 19/20
    76/76 - 11s - loss: 0.0186 - accuracy: 0.9916 - recall_6: 0.9405 - precision_6: 0.9595 - f1_metric: 0.9498 - val_loss: 0.1243 - val_accuracy: 0.9748 - val_recall_6: 0.7220 - val_precision_6: 0.7811 - val_f1_metric: 0.7502
    Epoch 20/20
    76/76 - 10s - loss: 0.0181 - accuracy: 0.9917 - recall_6: 0.9415 - precision_6: 0.9603 - f1_metric: 0.9507 - val_loss: 0.1300 - val_accuracy: 0.9737 - val_recall_6: 0.7282 - val_precision_6: 0.7607 - val_f1_metric: 0.7439
    2020-05-08 12:38:54.999999995
    31
    Executing (8, 1)
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    WARNING:tensorflow:sample_weight modes were coerced from
      ...
        to  
      ['...']
    Train for 85 steps, validate for 10 steps
    Epoch 1/20
    85/85 - 33s - loss: 0.1908 - accuracy: 0.9261 - recall_7: 0.4581 - precision_7: 0.5715 - f1_metric: 0.5113 - val_loss: 0.8559 - val_accuracy: 0.1282 - val_recall_7: 0.8923 - val_precision_7: 0.0511 - val_f1_metric: 0.0967
    Epoch 2/20
    85/85 - 12s - loss: 0.0998 - accuracy: 0.9633 - recall_7: 0.6922 - precision_7: 0.8396 - f1_metric: 0.7563 - val_loss: 0.8782 - val_accuracy: 0.2735 - val_recall_7: 0.9497 - val_precision_7: 0.0642 - val_f1_metric: 0.1203
    Epoch 3/20
    85/85 - 12s - loss: 0.0709 - accuracy: 0.9750 - recall_7: 0.7937 - precision_7: 0.8947 - f1_metric: 0.8405 - val_loss: 0.2957 - val_accuracy: 0.8824 - val_recall_7: 0.2369 - val_precision_7: 0.1377 - val_f1_metric: 0.1740
    Epoch 4/20
    85/85 - 12s - loss: 0.0562 - accuracy: 0.9804 - recall_7: 0.8388 - precision_7: 0.9197 - f1_metric: 0.8771 - val_loss: 0.2186 - val_accuracy: 0.9366 - val_recall_7: 0.0688 - val_precision_7: 0.1976 - val_f1_metric: 0.1021
    Epoch 5/20
    85/85 - 12s - loss: 0.0474 - accuracy: 0.9834 - recall_7: 0.8625 - precision_7: 0.9330 - f1_metric: 0.8963 - val_loss: 0.2749 - val_accuracy: 0.9467 - val_recall_7: 0.0141 - val_precision_7: 0.3066 - val_f1_metric: 0.0268
    Epoch 6/20
    85/85 - 12s - loss: 0.0415 - accuracy: 0.9851 - recall_7: 0.8779 - precision_7: 0.9399 - f1_metric: 0.9077 - val_loss: 0.2258 - val_accuracy: 0.9460 - val_recall_7: 0.1378 - val_precision_7: 0.4471 - val_f1_metric: 0.2100
    Epoch 7/20
    85/85 - 12s - loss: 0.0372 - accuracy: 0.9864 - recall_7: 0.8896 - precision_7: 0.9443 - f1_metric: 0.9161 - val_loss: 0.1950 - val_accuracy: 0.9456 - val_recall_7: 0.5165 - val_precision_7: 0.4817 - val_f1_metric: 0.4975
    Epoch 8/20
    85/85 - 12s - loss: 0.0341 - accuracy: 0.9873 - recall_7: 0.8984 - precision_7: 0.9469 - f1_metric: 0.9219 - val_loss: 0.1470 - val_accuracy: 0.9619 - val_recall_7: 0.7015 - val_precision_7: 0.6199 - val_f1_metric: 0.6576
    Epoch 9/20
    85/85 - 12s - loss: 0.0313 - accuracy: 0.9881 - recall_7: 0.9053 - precision_7: 0.9495 - f1_metric: 0.9268 - val_loss: 0.1381 - val_accuracy: 0.9697 - val_recall_7: 0.6965 - val_precision_7: 0.7160 - val_f1_metric: 0.7057
    Epoch 10/20
    85/85 - 12s - loss: 0.0299 - accuracy: 0.9885 - recall_7: 0.9092 - precision_7: 0.9504 - f1_metric: 0.9292 - val_loss: 0.1430 - val_accuracy: 0.9679 - val_recall_7: 0.6452 - val_precision_7: 0.7138 - val_f1_metric: 0.6773
    Epoch 11/20
    85/85 - 12s - loss: 0.0272 - accuracy: 0.9893 - recall_7: 0.9160 - precision_7: 0.9537 - f1_metric: 0.9343 - val_loss: 0.1355 - val_accuracy: 0.9716 - val_recall_7: 0.6629 - val_precision_7: 0.7634 - val_f1_metric: 0.7090
    Epoch 12/20
    85/85 - 12s - loss: 0.0262 - accuracy: 0.9895 - recall_7: 0.9187 - precision_7: 0.9539 - f1_metric: 0.9359 - val_loss: 0.1489 - val_accuracy: 0.9702 - val_recall_7: 0.6558 - val_precision_7: 0.7438 - val_f1_metric: 0.6964
    Epoch 13/20
    85/85 - 12s - loss: 0.0250 - accuracy: 0.9898 - recall_7: 0.9220 - precision_7: 0.9546 - f1_metric: 0.9379 - val_loss: 0.1354 - val_accuracy: 0.9741 - val_recall_7: 0.6836 - val_precision_7: 0.7921 - val_f1_metric: 0.7333
    Epoch 14/20
    85/85 - 12s - loss: 0.0239 - accuracy: 0.9901 - recall_7: 0.9248 - precision_7: 0.9555 - f1_metric: 0.9399 - val_loss: 0.1484 - val_accuracy: 0.9727 - val_recall_7: 0.6783 - val_precision_7: 0.7728 - val_f1_metric: 0.7221
    Epoch 15/20
    85/85 - 12s - loss: 0.0228 - accuracy: 0.9905 - recall_7: 0.9275 - precision_7: 0.9568 - f1_metric: 0.9419 - val_loss: 0.1451 - val_accuracy: 0.9718 - val_recall_7: 0.6600 - val_precision_7: 0.7681 - val_f1_metric: 0.7095
    Epoch 16/20
    85/85 - 12s - loss: 0.0222 - accuracy: 0.9906 - recall_7: 0.9298 - precision_7: 0.9563 - f1_metric: 0.9428 - val_loss: 0.1385 - val_accuracy: 0.9735 - val_recall_7: 0.6768 - val_precision_7: 0.7873 - val_f1_metric: 0.7275
    Epoch 17/20
    85/85 - 12s - loss: 0.0212 - accuracy: 0.9909 - recall_7: 0.9315 - precision_7: 0.9579 - f1_metric: 0.9444 - val_loss: 0.1409 - val_accuracy: 0.9727 - val_recall_7: 0.7122 - val_precision_7: 0.7521 - val_f1_metric: 0.7312
    Epoch 18/20
    85/85 - 12s - loss: 0.0208 - accuracy: 0.9909 - recall_7: 0.9329 - precision_7: 0.9575 - f1_metric: 0.9449 - val_loss: 0.1488 - val_accuracy: 0.9717 - val_recall_7: 0.6785 - val_precision_7: 0.7563 - val_f1_metric: 0.7147
    Epoch 19/20
    85/85 - 12s - loss: 0.0200 - accuracy: 0.9912 - recall_7: 0.9351 - precision_7: 0.9583 - f1_metric: 0.9465 - val_loss: 0.1460 - val_accuracy: 0.9722 - val_recall_7: 0.7279 - val_precision_7: 0.7375 - val_f1_metric: 0.7324
    Epoch 20/20
    85/85 - 12s - loss: 0.0196 - accuracy: 0.9913 - recall_7: 0.9357 - precision_7: 0.9587 - f1_metric: 0.9470 - val_loss: 0.1450 - val_accuracy: 0.9716 - val_recall_7: 0.7159 - val_precision_7: 0.7349 - val_f1_metric: 0.7248


The next cell computes the accuracy, precision, recall and F1 scores for the model across the expanding window training session. A 95% confidence interval is computed for each metric using a bootstrap approximation.


```python
import numpy as np
model_performance = []

def confidence_intervals(matrix, col_id, power=.05):
    mean = matrix[:, col_id].mean()
    lower = np.quantile(matrix[:, col_id], 1-power/2)
    upper = np.quantile(matrix[:, col_id], power/2)
    return 2*mean - lower, mean, 2*mean - upper

for idx in range(num_trains):
    temp_records = record_collections[idx]
    _ , items , _ = temp_records.get_modeling_parameters(path_to_metadata)
    
    num_records = len(temp_records.records)
    num_positive = sum([any(sum(record.candidate_labels.values(),[])) for record in temp_records.records.values() if record.get_items().intersection(set(items))])
    
    temp_generator = validation_generators[idx]
    
    for model in models[idx]:
        temp_model = model

        scores = []

        for _ in range(100):
            X_batch, Y_batch = temp_generator.__getitem__(0)

            score = temp_model.model.evaluate(X_batch, Y_batch, verbose=0)
            scores.append(score)

            temp_generator.on_epoch_end()

        scores = np.asarray(scores)
        acc_low, acc_mean, acc_high = confidence_intervals(scores, 1)
        rec_low, rec_mean, rec_high = confidence_intervals(scores, 2)
        prec_low, prec_mean, prec_high = confidence_intervals(scores, 3)
        f1_low, f1_mean, f1_high = confidence_intervals(scores, 4)

        temp_dict = {"acc_low":acc_low,"acc_mean":acc_mean,"acc_high":acc_high,
                     "rec_low":rec_low,"rec_mean":rec_mean,"rec_high":rec_high,
                     "prec_low":prec_low,"prec_mean":prec_mean,"prec_high":prec_high,
                     "f1_low":f1_low,"f1_mean":f1_mean,"f1_high":f1_high,
                     "records":num_records, "positive":num_positive, "train_step":idx}
        model_performance.append(temp_dict)
        
model_performance = pd.DataFrame(model_performance)
```

Model performance is saved in a Pandas dataframe:


```python
model_performance
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>acc_low</th>
      <th>acc_mean</th>
      <th>acc_high</th>
      <th>rec_low</th>
      <th>rec_mean</th>
      <th>rec_high</th>
      <th>prec_low</th>
      <th>prec_mean</th>
      <th>prec_high</th>
      <th>f1_low</th>
      <th>f1_mean</th>
      <th>f1_high</th>
      <th>records</th>
      <th>positive</th>
      <th>train_step</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.950760</td>
      <td>0.956663</td>
      <td>0.964188</td>
      <td>0.422332</td>
      <td>0.504070</td>
      <td>0.579871</td>
      <td>0.505405</td>
      <td>0.549666</td>
      <td>0.607335</td>
      <td>0.465475</td>
      <td>0.524281</td>
      <td>0.604078</td>
      <td>10524</td>
      <td>655</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.953194</td>
      <td>0.960127</td>
      <td>0.966616</td>
      <td>0.632825</td>
      <td>0.676338</td>
      <td>0.728810</td>
      <td>0.714731</td>
      <td>0.758675</td>
      <td>0.803794</td>
      <td>0.678097</td>
      <td>0.715926</td>
      <td>0.753395</td>
      <td>12788</td>
      <td>889</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.961244</td>
      <td>0.967235</td>
      <td>0.975868</td>
      <td>0.803048</td>
      <td>0.847734</td>
      <td>0.903610</td>
      <td>0.776582</td>
      <td>0.811525</td>
      <td>0.852745</td>
      <td>0.734976</td>
      <td>0.818121</td>
      <td>1.001792</td>
      <td>15406</td>
      <td>1122</td>
      <td>2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.969491</td>
      <td>0.975519</td>
      <td>0.981910</td>
      <td>0.731409</td>
      <td>0.779886</td>
      <td>0.833591</td>
      <td>0.740635</td>
      <td>0.796924</td>
      <td>0.866001</td>
      <td>0.733393</td>
      <td>0.786289</td>
      <td>0.839670</td>
      <td>18420</td>
      <td>1291</td>
      <td>3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.969499</td>
      <td>0.974678</td>
      <td>0.978968</td>
      <td>0.742584</td>
      <td>0.773067</td>
      <td>0.806852</td>
      <td>0.751559</td>
      <td>0.783957</td>
      <td>0.827626</td>
      <td>0.740164</td>
      <td>0.776504</td>
      <td>0.826268</td>
      <td>21759</td>
      <td>1496</td>
      <td>4</td>
    </tr>
    <tr>
      <th>5</th>
      <td>0.971140</td>
      <td>0.975466</td>
      <td>0.978345</td>
      <td>0.691835</td>
      <td>0.742811</td>
      <td>0.791174</td>
      <td>0.759007</td>
      <td>0.804509</td>
      <td>0.849582</td>
      <td>0.718244</td>
      <td>0.766798</td>
      <td>0.818518</td>
      <td>25038</td>
      <td>1709</td>
      <td>5</td>
    </tr>
    <tr>
      <th>6</th>
      <td>0.971115</td>
      <td>0.974921</td>
      <td>0.978986</td>
      <td>0.686841</td>
      <td>0.725315</td>
      <td>0.769706</td>
      <td>0.745564</td>
      <td>0.783904</td>
      <td>0.822712</td>
      <td>0.721014</td>
      <td>0.753178</td>
      <td>0.790027</td>
      <td>29253</td>
      <td>2002</td>
      <td>6</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0.970807</td>
      <td>0.973740</td>
      <td>0.977218</td>
      <td>0.654247</td>
      <td>0.685189</td>
      <td>0.722294</td>
      <td>0.748825</td>
      <td>0.789913</td>
      <td>0.831216</td>
      <td>0.706033</td>
      <td>0.732976</td>
      <td>0.767249</td>
      <td>32512</td>
      <td>2185</td>
      <td>7</td>
    </tr>
  </tbody>
</table>
</div>



The following cells generate the plots shown in the generalizability analysis:


```python
model_performance.set_index("records")["f1_mean"].plot()
model_performance.set_index("records")["f1_low"].plot(style="k--")
model_performance.set_index("records")["f1_high"].plot(style="k--")

plt.xlabel("Number of records")
plt.ylabel("Validation F1 Score")
plt.ylim([0,1])
```




    (0, 1)




![png](subject_model_sandbox_files/subject_model_sandbox_18_1.png)


Plot an example source snapshot:


```python
model_performance.set_index("records")["rec_mean"].plot()
model_performance.set_index("records")["rec_low"].plot(style="k--")
model_performance.set_index("records")["rec_high"].plot(style="k--")

plt.xlabel("Number of records")
plt.ylabel("Validation Recall Score")
plt.ylim([0,1])
```




    (0, 1)




![png](subject_model_sandbox_files/subject_model_sandbox_20_1.png)



```python
model_performance.set_index("records")["prec_mean"].plot()
model_performance.set_index("records")["prec_low"].plot(style="k--")
model_performance.set_index("records")["prec_high"].plot(style="k--")

plt.xlabel("Number of records")
plt.ylabel("Validation Precision Score")
plt.ylim([0,1])
```




    (0, 1)




![png](subject_model_sandbox_files/subject_model_sandbox_21_1.png)



```python
model_performance.set_index("positive")["f1_mean"].plot()
model_performance.set_index("positive")["f1_low"].plot(style="k--")
model_performance.set_index("positive")["f1_high"].plot(style="k--")

plt.xlabel("Number of GT Positive")
plt.ylabel("Validation F1 Score")
plt.ylim([0,1])
```




    (0, 1)




![png](subject_model_sandbox_files/subject_model_sandbox_22_1.png)



```python
model_performance.set_index("positive")["rec_mean"].plot()
model_performance.set_index("positive")["rec_low"].plot(style="k--")
model_performance.set_index("positive")["rec_high"].plot(style="k--")
plt.xlabel("Number of GT Positive")
plt.ylabel("Validation Recall Score")
plt.ylim([0,1])
```




    (0, 1)




![png](subject_model_sandbox_files/subject_model_sandbox_23_1.png)



```python
model_performance.set_index("positive")["prec_mean"].plot()
model_performance.set_index("positive")["prec_low"].plot(style="k--")
model_performance.set_index("positive")["prec_high"].plot(style="k--")
plt.xlabel("Number of GT Positive")
plt.ylabel("Validation Precision Score")
```




    Text(0, 0.5, 'Validation Precision Score')




![png](subject_model_sandbox_files/subject_model_sandbox_24_1.png)



```python
source_test_data = validation_generator.source_snapshots
target_test_data = validation_generator.target_snapshots
record_test_data = validation_generator.record_snapshots
```


```python
plt.imshow(source_test_data[0,:,:,0],aspect=20/128)
plt.xlabel("Window ID")
plt.ylabel("ItemOID ID")
```




    Text(0, 0.5, 'ItemOID ID')




![png](subject_model_sandbox_files/subject_model_sandbox_26_1.png)


Plot an exmaple target snapshot:


```python
plt.imshow(target_test_data[0,:,:,0],aspect=20/128)
plt.xlabel("Window ID")
plt.ylabel("ItemOID ID")
plt.colorbar()
```




    <matplotlib.colorbar.Colorbar at 0x7fc948f7b160>




![png](subject_model_sandbox_files/subject_model_sandbox_28_1.png)


The `infer.py` file in the `src/` directory contains the code used to generate the data risk score for each record. First, the model is used to predict the labels of the tetsing data:


```python
model_predictions = model.infer(source_test_data)
```

Plot an example of the model prediction (should resemble the above target snapshot)


```python
plt.imshow(model_predictions[0,:,:,0],aspect=20/128)
plt.xlabel("Window ID")
plt.ylabel("ItemOID ID")
plt.colorbar()
```




    <matplotlib.colorbar.Colorbar at 0x7fc948cea358>




![png](subject_model_sandbox_files/subject_model_sandbox_32_1.png)


To generate the aggregated risk score of the records (at the ItemOID level) in the snapshot, use the `assess_batch` method of the model:


```python
predicted_record_scores = pd.DataFrame(model.assess_batch(validation_generator))
```


```python
predicted_record_scores.head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ItemGroupRecordId</th>
      <th>ItemOID</th>
      <th>OccurenceCount</th>
      <th>RawOutput</th>
      <th>Confidence</th>
      <th>Prediction</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>17077932</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.030716</td>
      <td>0.905362</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>17033773</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.152569</td>
      <td>0.738805</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>17033759</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.152582</td>
      <td>0.744869</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>16996782</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.032378</td>
      <td>0.854293</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>16991663</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.080186</td>
      <td>0.797730</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>16983349</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.156080</td>
      <td>0.738689</td>
      <td>0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>16975792</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.085891</td>
      <td>0.790808</td>
      <td>0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>16950791</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.207835</td>
      <td>0.718107</td>
      <td>0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>16949015</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.192829</td>
      <td>0.723759</td>
      <td>0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>16949001</td>
      <td>EXINREAS</td>
      <td>32</td>
      <td>0.179165</td>
      <td>0.727818</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>



To compate the model results in aggregate, we can perform the data risk aggregation on the actual ground truth data as well:


```python
true_record_scores = pd.DataFrame(model.assess_batch(validation_generator, infer=False))
```

Compare the distributions of the true and predicted risk scores using a histogram:


```python
plt.hist([predicted_record_scores.Prediction, true_record_scores.Prediction] , label=["Predicted Risk Scores", "Actual Risk Scores"])
plt.legend()
plt.xlabel("Risk Score")
plt.ylabel("Count")
plt.show()
```


![png](subject_model_sandbox_files/subject_model_sandbox_39_0.png)


We can also judge the model confidence (using the standard deviation of the predictions):


```python
predicted_record_scores.Confidence.hist()
plt.xlabel("Model Confidence")
plt.ylabel("Count")
```




    Text(0, 0.5, 'Count')




![png](subject_model_sandbox_files/subject_model_sandbox_41_1.png)


And the number of frames in which each record appears:


```python
predicted_record_scores.OccurenceCount.hist()
plt.xlabel("Number of Frames")
plt.ylabel("Count")
```




    Text(0, 0.5, 'Count')




![png](subject_model_sandbox_files/subject_model_sandbox_43_1.png)



```python

```
