# SFL Scientific Project Template

<p align="center"><img width=50% src="imgs/logo.png"></p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

![Python](https://img.shields.io/badge/python-v3.6+-blue.svg)
[![Build Status](https://travis-ci.org/anfederico/Clairvoyant.svg?branch=master)](https://travis-ci.org/anfederico/Clairvoyant)
![Dependencies](https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg)

## Purpose 

The purpose of this project is to explore AI solutions to data review. This repository contains the preprocessing pipeline and model code for a domain-centric and subject-centric data review models.

## Project Description

Given Audit and Query Rave ODM-style data, this code extracts QEC events and tracks data changes over time for all records in a study. Once that data is parsed it is assigned ground truth labels using query information; if a record is manually queried and changed as a result of that query, the record is given a ground truth positive label.  This ground truth label is predicted by the domain and subject models.

## Table of Contents
0. [AWS Deployment](docker/README.md)
1. [Local Deployment](#1-Deployment)\
	1.1 [Environment Dependencies](#11-Environment-Dependencies)\
    1.2 [Docker Registry Dependencies](#12-Docker-Registry-Dependencies)\
    1.3 [Installation](#13-Installation)
2. [Usage](#2-Usage)\
	2.1 [Training Usage](#21-Training-Usage)\
    2.2 [Inference Usage](#22-Inference-Usage)\
    2.3 [Notebook Usage](#25-Notebook-Usage)
3. [Project Contents](#3-Project-Contents)
4. [Contacts](#4-Contacts)
5. [License](#5-License)


## 1. Deployment
([Return to Table of Contents](#Table-of-Contents))
### 1.1 Environment Dependencies

Python 3.8 is required to run this repository; for information about installing Python on a windows environment visit [Python](https://www.python.org/downloads/).

### 1.2 Data Dependencies

In order to use this repo outside of the AI Architecture data will have to be pulled from the MSSQL environment manually. The Query, Audit and schema information will all need to be saved to a local directory as CSV files. The `data\raw` directory is recommended.

### 1.3 Installation

The installation process simply requires that the Python packages used by the repository be installed. This can be done automatically using the following CLI command:
```
pip install -r requirements.txt
```
this command will use the Python package manager (pip) to install the correct versions of all the Python packages listed in the requirements text file.


## 2. Usage
([Return to Table of Contents](#Table-of-Contents))

This repository is designed for use inside the AI Architecture, however it can be used like a standard Python package in Python code or Jupyter notebooks. There is a notebook (`notebooks\subject_model_sandbox.ipynb`) which contains all of the code for running the subject model locally.

### 2.1 Training Usage

Model training begins with the creation of a RecordCollection object, the class code for which is contained in `src/classes/recordcollection.py`.  The Audit and Query tables for a study are required to create a record collection. Once the `RecordCollection, AuditDataset, QueryDataset` objects have been imported, running the following Python commands will create and populate a record collection (for reference, this code is included in the `src/preprocess.py` code):
```
# Load in the data
audit = AuditDataset(path = path_to_audit_csv)
query = QueryDataset(path = path_to_query_csv)

# Initialize an empty object
record_collection = RecordCollection()

# Populate a list of all records using the Audit table
record_collection.make_manifest(audit)

# Create empty records
record_collection.initialize_records()

# Fill in initilized records with Audit and Query information
record_collection.make_records(audit, query)

```
Once a record collection is populated, the most queried 128 ItemOIDs can be identified and the values taken for each ItemOID can be tallied, this is performed using the `get_modeling_parameters` method of the record collection:
```
itemoid_values, sorted_itemoids, sorted_itemoids_to_dtype = record_collection.get_modeling_parameters(path_to_metadata)
```
where the `path_to_metadata` argumement points to the CDR metadata (which must be saved locally as a CSV). Next, an `EncoderCollection` is trained and a `DataGenerator` is created. The encoder collection takes the datetime, text, and numerical information from the record collection and converts it to categorical (integer valued) data for modeling.  The text information is scrubbed and tokenized, and the datetime/numerical values are binned.  The following code creates and fits an EncoderCollection
```
from ordinalencodercollection import OrdinalEncoderCollection
encoder_collection = OrdinalEncoderCollection(items_to_dtype = sorted_itemoids_to_dtype)
encoder_collection.fit(itemoid_values)
```
The final step in model preparation is creating a DataGenerator object.  The data generator creates the subject snapshots and performs the train/validation split:
```
from datagenerator import DataGenerator
fit_generator = DataGenerator(record_collection, encoder_collection, sorted_itemoids, split_date = date_to_split)
feature_dict = fit_generator.count_feature_values()
test_source_snapshots, test_target_snapshots, _ = fit_generator.get_nontraining_data()
```
where the `date_to_split` is the point in time to perform the temporal holdout split. By defualt, the data generator holds 30% of subject information out of the training data, if a date is passed the model performs a temporal-subject split. The test data is removed from the the data generator using the `get_nontraining_data` method.

At this point, the data is ready to train a SubjectModel:
```
from model import SubjectModel
model = SubjectModel()
model.build_model(feature_dict)

model.model.fit(fit_generator, validation_data=(test_source_snapshots, test_target_snapshots), epochs=20)
model.save_to_path(path_to_model)
```
the `feature_dict` that was created above is used as a vocabulary to construct the embedding layers of the model. The data generator is used to randomly sample the training data in batches, the test data is used for evaluating model performance.

As this point the model is trained and can be used to identify data risk scores.

### 2.2 Inference Usage

Model inference is performed using the `infer` method of the `SubjectModel` class. To provide an example, the test data from the model training procedure will be used (any of the subject snapshots can be used in the inference pipeline):
```
predicted_target_snapshots = model.infer(test_source_snapshots)
```
the `predicted_target_snapshots` will be a 4-tensor numpy array, the first index tracks the snapshot ID of the data. For example the fifth entry of the `test_source_snapshot` array (`test_source_snapshots[5,]`) will be a 128x20x1 numpy array, and the fifth entry of the `predicted_target_snapshots` object will correspondingly be a 128x20x1 array of model predictions. 

To generate the aggregated data risk scores for all the records contained in the test data, perform the following commands:
```
record_scores = model.assess_batch(pred_target_snapshots, record_snapshots)
filtered_record_scores = record_collection.filter_record_scores(record_scores)
```
the `filtered_record_scores` object will be a dictionary of ItemGroupRecordId, ItemOID values and corresponding data risk scores; data risk scores are between 0 and 1, 0 scores correspond to no risk, higher values denote more risk.

### 2.3 Notebook Usage

The `subject_model_sandbox` file in the `notebook` directory contains example code for using the codebase in a notebook.  To use the notebook, first generate CSV files for the desired study; the Audit and Query tables are required to process the data into a record collection, the CDR.Field metadata is required to generate the encoder collections.  For reference, only a small number of columns in the Audit and Query tables are used in the modeling process, namely:(Audit) ItemGroupRecordId, SubjectName, FormOID, ItemOID, ItemValue, DateTimeStamp, UserOID, AuditSubCategoryName; (Query) RecordId, SubjectID, FormOID, OpenDate, QueryOpenUserOID, QueryRecipient, ItemOID.  Excluding the remaining columns will reduce the RAM used by Pandas while loading data.

Next, login to the AWS Console as an IAM user. Navigate to the ECS-SFL-Notebook on the shared Sagemaker, and launch a Jupyter Lab instance by clicking Open JupyterLab. If the notebook is not visible on the Notebooks page, check that the region is set to `us-east-1`.  The toolbar on the left can be used to naviagte to the ECS_DataReview folder, which contains the latest code release.  Navigate to the `notebook` subdirectory and open the `subject_model_sandbox.ipynb` notebook.  If prompted, select the `conda_tensorflow_2_3.6` as the environment.  

Once the notebook is running, use the upload button (top of the left toolbar) to upload the CSV files to the `data/raw` directory. The names of the Audit, Query and metadata files should be inserted into the FILL cell. To recreate the generalizability results, a configuration file is loaded.  The config file has the hyperparameters used for Clintek 10, 21, 44, 56, and 63. To load the parameters, change the `study_name` variable to `Clintek ##`, where `##` is the desired study number.  

When configuring new studies, the `first_train` parameter should be chosen so that there are at least 200 sucessful queries in the audit table; before this point there may not be enough manual queries to correctly identify 128 itemoids for the model. The `last_train` variable can be chosen at any time in the study; to perform a temporal-subject split the `last_train` variable must be chosen so that there is data in the preceding month to generate a validation set. The `num_trains` variable controls the number of steps taken to progress fromt the `first_train` to the `last_train` dates.

The `num_layers` variable controls the base number of filters in the U-net model. Each time the synthetic image is compressed (4 times), the number of filters used doubles. Small changes to the `num_layers` variable has a large effect on the number of parameters in the CNN layers.  The model default value is 4 and `num_layers` should not be chosen smaller than 4.  Choosing values larger than 8 creates fairly large models, most studies should have values between 4 and 8.  To calibrate the correct number of layers, start with `num_layers=4` and grow the value until the validation performance plateaus.  It may be beneficial to increase the `num_runs` variable; this controls the number of times each timestep is trained. The data splits for each timestep are fixed, hence running the model several times will measure the training sensitivity of the model.  Smaller models are usually more brittle and prone to underfitting; larger models are more robust in the training process but are more likely to overfit.

The `num_epochs` variable controls the number of epochs in each training session; the default value is 20 which works well across most studies.  If the validation performance readouts level off before 20 epochs, reducing the `num_epochs` variable will reduce the training time.


### 3. Project Contents
([Return to Table of Contents](#Table-of-Contents))

    ├── README.md                                <- The top-level README for developers using this project.
    ├── src
         ├── classes                             <- The class code of the Data Review pipeline
               ├── datagenerator.py
               ├── dataset.py
               ├── datawindow.py
               ├── event.py
               ├── eventhistory.py
               ├── model.py
               ├── ordinalencoder.py
               ├── ordinalencodercollection.py
               ├── record.py
               ├── record_collection.py
         ├── preprocess.py                       <- AI Arch. code for data preprocessing
         ├── train.py                            <- AI Arch. code for model training
         ├── infer.py                            <- AI Arch. code for model inference
    ├── notebooks
    ├── test
    ├── models
    ├── logs
    ├── imgs
    ├── docker                                  <- AI Arch. code for containerization
    ├── data


### 4. Contacts
([Return to Table of Contents](#Table-of-Contents))

    - Benjamin Letson (bletson@sflscientific.com)
    - Daniel Ferrante (dferrante@sflscientific.com)


### 5. License
([Return to Table of Contents](#Table-of-Contents))

[![CC-BY-ND](https://licensebuttons.net/l/by-nd/4.0/88x31.png)](https://creativecommons.org/licenses/by-nd/4.0/)

To the extent possible under the law, and under our agreements, [SFL Scientific](http://sflscientific.com/) retains all copyright and related or neighboring rights to this work.
